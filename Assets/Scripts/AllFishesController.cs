﻿using UnityEngine;
using System.Collections;

public class AllFishesController : MonoBehaviour {

	 
	public Transform LimitTopLeft;
	public Transform LimitBottomRight;

	public Transform InstPosLeft;
	public Transform InstPosRight;

	public GameObject[] prefabs;
	public int prefGroupIndex; //index prefaba koji kreira jato tuna
	bool bKreiranoJato = false;

	
	// Use this for initialization
	void Start () {
	 

	//	Debug.Log ( "UnlockedFish: "+GameData.UnlockedFish + "  " +  " ,Level" + GameData.Level);

		InstantiateFishPrefab_FishingArea();
	}


	void InstantiateFishPrefab_FishingArea()
	{
		for(int i = 0;i<3;i++)
		{
			int prefInd = Random.Range(0,prefabs.Length );

			if(prefGroupIndex == prefInd &&  !bKreiranoJato) bKreiranoJato = true;
			else 	if(prefGroupIndex == prefInd && bKreiranoJato) 
			{
				while (prefGroupIndex == prefInd)
				{
					prefInd = Random.Range(0,prefabs.Length );
				}
			}

			Vector3 position = new Vector3(Random.Range(LimitTopLeft.position.x+1, LimitBottomRight.position.x-1), Random.Range(LimitTopLeft.position.y-1, LimitBottomRight.position.y+1), 0);
		 
			GameObject go = (GameObject) GameObject.Instantiate( prefabs[prefInd], position, Quaternion.identity );
			go.transform.parent = transform;
			go.transform.localScale = prefabs[prefInd].transform.localScale;
 

			if(i == 1 )
			{
				Tutorial.copyPositionTransform = go.transform.GetComponentInChildren<FishController>().transform;
			}

		}
	}

//	public void InstantiateFishPrefab_Outside()
//	{
//		int prefInd = Random.Range(0,prefabs.Length);
//		float x=  InstPosLeft.position.x;
//		if(Random.Range(0,100) > 50)  x =  InstPosRight.position.x;
//		Vector3 position = new Vector3( x , Random.Range(LimitTopLeft.position.y, LimitBottomRight.position.y), 0);
//		
//		GameObject go = (GameObject) GameObject.Instantiate( prefabs[prefInd], position, Quaternion.identity );
//		go.transform.parent = transform;
//		go.transform.localScale = prefabs[prefInd].transform.localScale;
// 
//	}
	 
}
