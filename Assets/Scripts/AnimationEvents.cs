﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class AnimationEvents : MonoBehaviour {

	 
	 

	public void StartParticles()
	{
		//transform.GetComponentInChildren<ParticleSystem>().Play();
	}
	public void CleaningAnimationFinished()
	{
		transform.parent.SendMessage("CleaningAnimationFinished",SendMessageOptions.DontRequireReceiver);
	}
 
	 

	public void PickaxHitGroundAnimation( )
	{
		transform.GetChild(1).GetChild(0).GetComponent<ParticleSystem>().Play();
	}

	public void AnimEventHideSceneAnimStarted()
	{
		LevelTransition.Instance.AnimEventHideSceneAnimStarted();
	}
	
	public void AnimEventShowSceneAnimFinished()
	{
		LevelTransition.Instance.AnimEventShowSceneAnimFinished();
	}

	public void UnlockLevelAnimationFinished( )
	{
		transform.GetChild(2).GetComponent<Image>().enabled = false; 
		transform.GetChild(4).GetComponent<Image>().enabled = true;
	}
}
