﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class DragToLocation : MonoBehaviour , IBeginDragHandler, IDragHandler, IEndDragHandler
{
	public static int ToClean= 0;

	float SnapDistance = 3f;


	Vector3 StartPosition ;
	bool bDrag = false;
	public static bool bEnableDrag = true;
	float x;
	float y;
	Vector3 diffPos = new Vector3(0,0,0);
	bool bMovingBack = false;
	 
	 
	Transform EndLocationParent;
	float EndLocationScale = 2.3f;//3
	public Vector3 StartScale;
	Vector3 target ;
	 
	bool bMoveBackAfterAnimation = false;
 
	bool bFinished;
  
	Transform ParentOld;
	Transform DragItemParent;
	int mouseFollowSpeed = 10; 

	void Awake()
	{
		 
	}

	IEnumerator Start () {
		yield return new WaitForSeconds(.1f);
		if(Application.loadedLevelName == "LevelGame3")
		{
			 
			DragItemParent = GameObject.Find("DragItemParent").transform;
			EndLocationParent = GameObject.Find("CanvasBG/Plate").transform;
			//StartScale = transform.localScale;
			target = EndLocationParent.position;
			mouseFollowSpeed = 5; 
		}
		StartPosition = transform.position;
		ParentOld = transform.parent;
	}


	IEnumerator MoveBack(  )
	{

		bEnableDrag = false;
		yield return new WaitForEndOfFrame( );
		if(!bMovingBack)
		{
			bMovingBack = true;
			
			
			yield return new WaitForEndOfFrame( );
			float pom = 0;
			while(pom<1 )
			{ 
				
				pom+=Time.deltaTime;
				transform.position = Vector3.Lerp(transform.position, StartPosition,pom);
				yield return new WaitForEndOfFrame( );
			}
			
			transform.position = StartPosition;

			bMovingBack = false;
		}
		
		transform.SetParent(ParentOld); 
		transform.localScale = StartScale;
		bEnableDrag = true;
		Tutorial.bPause = false;	 
	}






	void TestDistance()
	{
		if(Vector2.Distance(transform.position,target)<SnapDistance)
		{
 
			if(!bMoveBackAfterAnimation)  StartCoroutine("SnapToParent");
 	
			bDrag = false;
			bFinished = true;
			transform.GetComponent<CircleCollider2D>().enabled =  false;
		}
	}




	public void OnBeginDrag (PointerEventData eventData)
	{
		if(!bEnableDrag) return;
		if(!bDrag && !bFinished )
		{
			bDrag = true;
			diffPos =transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition)   ;
			diffPos = new Vector3(diffPos.x,diffPos.y,0);
			InvokeRepeating("TestDistance",0, .2f);
			if( Tutorial.Instance!=null) 	Tutorial.Instance.StopTutorial();
			transform.SetParent(DragItemParent);
		}
	}
	
	void Update()
	{
		if( bDrag  && bEnableDrag)  
		{
			
			x = Input.mousePosition.x;
			y = Input.mousePosition.y;

			Vector3 posM = Camera.main.ScreenToWorldPoint(new Vector3(x ,y,10.0f) );
			if(posM.x<-1.9f && posM.y<-2.0f) posM = new Vector3(posM.x,-2.0f,posM.z);
			else if(posM.y>2.2f) posM = new Vector3(posM.x,2.2f,posM.z);
			transform.position =  Vector3.Lerp (transform.position, posM  , mouseFollowSpeed * Time.deltaTime)  ;
			 
			
		}
	}
	
	
	public void OnEndDrag (PointerEventData eventData)
	{
		if(bDrag  &&  !bFinished)
		{
			 
			//transform.localScale = StartScale;
			bDrag = false;
			
			CancelInvoke("TestDistance");
			StartCoroutine("MoveBack" );
		}
	}
	
	
	public void OnDrag (PointerEventData eventData)
	{
		 
	}
	

	IEnumerator SnapToParent()
	{
	 
		bEnableDrag = false;

		if(Application.loadedLevelName == "LevelGame3")
		{
			gameObject.layer =      LayerMask.NameToLayer("Tool1Interact");// "Tool1Interact";
			for (int i = 1; i < 3; i++)
			{
				transform.GetChild(0).GetChild(i).GetComponent<PolygonCollider2D>().enabled = true;
			}
		}
		else
		{
			transform.GetComponent<PolygonCollider2D>().enabled = false;
		}

		CancelInvoke("TestDistance");
		float timeMove = 0;
		bool animationStarted = false;
		//SoundManager.Instance.StopAndPlay_Sound(SoundManager.Instance.ElementCompleted);
		
		while  (timeMove  <1 )
		{
			yield return new WaitForFixedUpdate();
			transform.position = Vector3.Lerp (transform.position, target , timeMove)  ;
			transform.localScale  = Vector3.Lerp (transform.localScale,  EndLocationScale * StartScale,  timeMove );
			timeMove += Time.fixedDeltaTime*2;
 
		}
		
		transform.parent = EndLocationParent; 

		yield return new WaitForFixedUpdate();
	 
 
//		Gameplay.Instance.ChangeProgressBar();
//		
//		Tutorial.bPause = true;
//		Tutorial.timeLeftToShowHelp = 10000;
//		Tutorial.ShowHelpPeriod = 10000;
//		
		if( Application.loadedLevelName == "LevelGame3" ) Camera.main.SendMessage("NextPhase", SendMessageOptions.DontRequireReceiver);
		else 	bEnableDrag = true;
		 
		this.enabled = false;
		
		
	}

	 
	bool appFoucs = true;
	void OnApplicationFocus( bool hasFocus )
	{
		if(  !appFoucs && hasFocus )
		{
			if(  bDrag )
			{
				bDrag = false;
				
				CancelInvoke("TestDistance");
				StartCoroutine("MoveBack" );
			}
		}
		appFoucs = hasFocus;
		
	}




}
