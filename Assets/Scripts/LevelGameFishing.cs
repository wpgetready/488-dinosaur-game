﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems; 

public class LevelGameFishing : MonoBehaviour {

	 
	Vector2 FishingStringStartSize;
	public RectTransform FishingString;
	public Animator animTongs;

	public bool bFishing = false;
	public bool bItemCaught = false;
	 
	Vector3 rotateClockwise = new Vector3(0,0,-50);  
	bool bClockwise = true; 
	bool bMoveForward = false; 
	bool bMoveBack = false; 
	
	float moveBackTime = 2f;
	
	
	Vector2 TargetPos  = Vector2.zero;
	float TargetDist  = 0;
	Vector2 FishingStringEndSize;
	float FishingStringStartLength = 0f;

	public Transform LimitBottomRight;
	public Transform LimitTopLeft;
	public Transform TongsTestPoint;
	Vector3 TongsTestPointDiffPos;

	public float stringSpeed = 500;
 
	Vector3 itemCauhtPos;

	public bool bPause = false;
	float catchTestSize = 0.2f;
	int nameToLayer = 0;

	Transform CaughtItem = null;

	public VerticalLayoutGroup CollectedBonesHolder;
	public GameObject HolderFillTop;

	int BonesCollected = 0;
	public Transform[] BoneHolders;
	public Transform[] BonePositions;

	public Transform  DisposeDestination;
	public ParticleSystem psCollected;
	public static bool bEnableFishing = true;

	public CanvasGroup NextButton;
	public Sprite NextButtonSprite;

	int obstaclesCount = 7;
	void Awake()
	{
		//-25 4:3
		//0 16:9
		int offset  = Mathf.FloorToInt((Screen.width/(float)Screen.height  - 1.7777f)* 57);
		CollectedBonesHolder.padding.top=offset;

		if(Shop.RemoveAds == 2) HolderFillTop.SetActive(false);

		Transform posBonePom;
		int a =0;
		int b =0;
		for(int i =0; i<BonePositions.Length*2; i++)
		{
			a = Random.Range(0,BonePositions.Length);
			posBonePom = BonePositions[a];
			b = Random.Range(0,BonePositions.Length);
			BonePositions[a] = BonePositions[b];
			BonePositions[b] = posBonePom;
		}
		CreateBones();

	}


	void CreateBones()
	{
		for(int i=1;i<=6;i++)
		{
			GameObject b = GameObject.Instantiate(  Resources.Load<GameObject>( "Dinos/"+ GameData.SelectedLevel.ToString()+"/Bone"+i.ToString()));
			b.transform.SetParent(BonePositions[i-1]);

			Vector2 size = b.GetComponent<RectTransform>().sizeDelta;
			float scale = 140f/size.x;
			float scale2 = 100/size.y;
			if(scale2<scale) scale = scale2;
			
			b.transform.localScale = scale*	Vector3.one;
			//b.transform.localScale = 0.6f*Vector3.one;
			b.transform.localPosition = Vector3.zero;
			Destroy( b.GetComponent<Mask>() );
		}
	}

	void Start () {
//		bPause = true;
//		Instance = this;
		Input.multiTouchEnabled = false;
	 
		FishingStringStartSize = FishingString.sizeDelta;
		FishingStringStartLength = FishingStringStartSize.y;
	 
		nameToLayer = 1 << LayerMask.NameToLayer("Item");
		
		BlockClicks.Instance.SetBlockAll(true);
		BlockClicks.Instance.SetBlockAllDelay(1.5f,false);

	 
		LevelTransition.Instance.ShowScene();

	}

	private bool IsPointerOverUIObject() {
		PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
		eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
		List<RaycastResult> results = new List<RaycastResult>();
		EventSystem.current. RaycastAll(eventDataCurrentPosition, results);

		return results.Count > 0;
	}

	void Update () 
	{
		if(bPause || !bEnableFishing ) 
		{
			return;
		}

		if(!bFishing)
		{
			if( Input.GetMouseButtonDown(0))
			{
				if( IsPointerOverUIObject () ) return;
				bFishing = true;
				bMoveForward = true;
			 
				if( SoundManager.Instance!=null ) 
				{
					SoundManager.Instance.FishingRope.Stop ();
					SoundManager.Instance.Play_Sound( SoundManager.Instance.FishingRope );
				}
			}

			if(bClockwise )
			{
				if(FishingString.rotation.eulerAngles.z > 290   ||  FishingString.rotation.eulerAngles.z < 95 )
				{
					FishingString.Rotate(rotateClockwise*Time.deltaTime);
				}
				else 
				{
					bClockwise = false;
				}
			}
			else if(!bClockwise )
			{
				if(FishingString.rotation.eulerAngles.z > 255  ||  FishingString.rotation.eulerAngles.z < 70 )
				{
					FishingString.Rotate(-rotateClockwise*Time.deltaTime);
				}
				else
				{
					bClockwise = true;
				}
			}
		}

		if(bFishing )
		{
			if(bMoveForward ) 
			{
				if( TongsTestPoint.position.x > LimitTopLeft.position.x 
				   && TongsTestPoint.position.x < LimitBottomRight.position.x
				  // && TongsTestPoint.position.y < LimitTopLeft.position.y 
				   && TongsTestPoint.position.y > LimitBottomRight.position.y)
				{
					//razvlacenje strune
					FishingString.sizeDelta += new Vector2( 0, stringSpeed*Time.deltaTime);
					TestCatchItem();
				}
				else
				{
					animTongs.SetTrigger("tCatch");

					bMoveForward = false;
				}
			}
		 
			else if(bMoveBack ) 
			{
				if(FishingString.sizeDelta.y > FishingStringStartLength  )
				{
					// privlacenje strune
					FishingString.sizeDelta -= new Vector2( 0, stringSpeed*Time.deltaTime);
					if(CaughtItem!=null) CaughtItem.position = TongsTestPoint.position - TongsTestPointDiffPos;
				}
				else
				{
					bMoveForward = false;
					bMoveBack = false;
					bFishing = false;
					animTongs.SetTrigger("tOpen");
					if(bItemCaught)
					{
						if(CaughtItem.GetComponent<PolygonCollider2D>() != null) CaughtItem.GetComponent<PolygonCollider2D>().enabled = false;
						if(CaughtItem.GetComponent<CircleCollider2D>() != null) CaughtItem.GetComponent<CircleCollider2D>().enabled = false;
						 

						MoveCatchedItem m =CaughtItem.gameObject.AddComponent<MoveCatchedItem>();
						m.StartPosition = CaughtItem.position;

						if(CaughtItem.tag == "Bone")
						{
							m.Destiantion = BoneHolders[BonesCollected];
							BonesCollected++;
							m.StartCoroutine("SetGlowAndMove");

							psCollected.Play();
							psCollected.transform.position =  CaughtItem.position;
							if( SoundManager.Instance!=null ) SoundManager.Instance.Play_Sound( SoundManager.Instance.MoveBoneLeft );

							if(BonesCollected == 6) 
							{
								//Debug.Log ("KRAJ");
								NextButton.alpha = 1;
								NextButton.GetComponent<Button>().enabled = true;
								NextButton.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = NextButtonSprite;
								NextButton.transform.parent.GetComponent<Animator>().SetTrigger("tPulse");
								if( SoundManager.Instance!=null ) SoundManager.Instance.Play_Sound( SoundManager.Instance.LevelFinished );
							}
						}
						else
						{
							obstaclesCount--;
						 
							m.Destiantion = DisposeDestination;
							m.StartCoroutine("DisposeJunk");
							if( SoundManager.Instance!=null ) SoundManager.Instance.Play_Sound( SoundManager.Instance.DisgardItem );

						}

						CaughtItem = null;
						bItemCaught = false;

						if(BonesCollected == 6 && obstaclesCount == 0)
						{
							Invoke("LoadNextLevel",2);
						}
					}
				}
			}

			else
			{
				StartCoroutine("TongsClose");
			}
		}
	}

	void LoadNextLevel()
	{
		LevelTransition.Instance.HideSceneAndLoadNext("MainGamePuzzle");
		BlockClicks.Instance.SetBlockAll(true);
	}

	IEnumerator TongsClose()
	{
		if( SoundManager.Instance!=null ) 
		{
			SoundManager.Instance.FishingRope.Stop ();
			SoundManager.Instance.Play_Sound( SoundManager.Instance.ItemCaught );
		}
		yield return new WaitForSeconds(0.3f);
		bMoveBack = true;
	}

	void TestCatchItem()
	{
		//TODO: probati sa raycast da se odredi
		CaughtItem =null;

		Collider2D[] hitColliders = Physics2D.OverlapCircleAll( TongsTestPoint.position, catchTestSize  , nameToLayer); //layermask to filter the varius colliders
		 
		foreach(Collider2D coll in hitColliders)
		{
		 
			if(coll.tag == "Obstacle" || coll.tag == "Fish" ) //prepreka ima prednost
			{
				TongsTestPointDiffPos = TongsTestPoint.position - coll.transform.position;
				CaughtItem = coll.transform;

				animTongs.SetTrigger("tCatch");
				StartCoroutine("TongsClose");
				bMoveForward = false;
				bItemCaught = true;

				if(coll.tag == "Fish")
				{
					FishController fc = CaughtItem.GetComponent<FishController>();
					if(fc!=null)  fc.FishCaught();
				}
				return;
			}
			else
			{
				CaughtItem = coll.transform;
			}

			animTongs.SetTrigger("tCatch");
			StartCoroutine("TongsClose");
			bMoveForward = false;
			bItemCaught = true;
			TongsTestPointDiffPos = TongsTestPoint.position - CaughtItem.transform.position;
		}
	}

	public void ButtonBackClicked()
	{
		LevelTransition.Instance.HideSceneAndLoadNext("Map");
		SoundManager.Instance.Play_ButtonClick();
		BlockClicks.Instance.SetBlockAll(true);
		GameData.IncrementBackButtonClickedCount();
	}
 
	public void ButtonNextClicked()
	{
		LevelTransition.Instance.HideSceneAndLoadNext("MainGamePuzzle");
		SoundManager.Instance.Play_ButtonClick();
		BlockClicks.Instance.SetBlockAll(true);
		GameData.IncrementForwardButtonClickedCount();
	}

}
