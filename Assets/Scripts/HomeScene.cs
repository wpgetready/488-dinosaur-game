﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
 


public class HomeScene : MonoBehaviour {

	public Image SoundOn;
	public Image SoundOff;
 
 
 
	public Image SettingsOpen;
	public Image SettingsClose;
	public Animator animSettings;
 
	public MenuManager menuManager;
	public GameObject PopUpResetMap; 
	public GameObject PopUpRate; 
   
		
	void Awake()
	{
		Input.multiTouchEnabled = false;

	}

	void Start () {
	 
		if(SoundManager.soundOn == 1)
		{
			SoundOff.enabled = false;
			SoundOn.enabled = true;
		}
		else
		{
			SoundOff.enabled = true;
			SoundOn.enabled = false;
		}

		LevelTransition.Instance.ShowScene();
	}

	
	public void ExitGame () {
 
		Debug.Log("EXIT");

		if (Shop.RemoveAds !=2 )
		{
			AdsManager.Instance.ShowInterstitial();
			//Debug.Log("EXIT INTERSTITAL, ID=" +WebelinxCMS.INTERSTITIAL_APPEXIT_ID);
		}
		else
		{
			Application.Quit();
		}
	}
 
 
	public void btnSoundClicked()
	{
 
		if(SoundManager.soundOn == 1)
		{
			SoundOff.enabled = true;
			SoundOn.enabled = false;
			SoundManager.soundOn = 0;
			 

		 	SoundManager.Instance.MuteAllSounds();

		}
		else
		{
			SoundOff.enabled = false;
			SoundOn.enabled = true;
			SoundManager.soundOn = 1;
			 

			SoundManager.Instance.UnmuteAllSounds();
			SoundManager.Instance.Play_ButtonClick();
 
		}



		if(SoundManager.musicOn == 1)
		{
			SoundManager.Instance.Stop_Music();
			SoundManager.musicOn = 0;
		}
		else
		{
			SoundManager.musicOn = 1;
			SoundManager.Instance.Play_Music();
		}

		PlayerPrefs.SetInt("SoundOn",SoundManager.soundOn);
		PlayerPrefs.SetInt("MusicOn",SoundManager.musicOn);
		PlayerPrefs.Save();

	 
		BlockClicks.Instance.SetBlockAll(true);
		BlockClicks.Instance.SetBlockAllDelay(.3f,false);
 

	}

	public void btnSettingsClicked()
	{

		SettingsOpen.enabled = !SettingsOpen.enabled;
		SettingsClose.enabled = !SettingsOpen.enabled;

		animSettings.SetBool("bOpen",SettingsClose.enabled);
		SoundManager.Instance.Play_ButtonClick();

		BlockClicks.Instance.SetBlockAll(true);
		BlockClicks.Instance.SetBlockAllDelay(.3f,false);
	}
 

	public void btnResetMapClicked()
	{
		menuManager.ShowPopUpMenu(PopUpResetMap);
		BlockClicks.Instance.SetBlockAll(true);
		BlockClicks.Instance.SetBlockAllDelay(.8f,false);
		SoundManager.Instance.Play_ButtonClick();
	}

	public void btnResetYesClicked()
	{
		menuManager.ClosePopUpMenu(PopUpResetMap);
		BlockClicks.Instance.SetBlockAll(true);
		BlockClicks.Instance.SetBlockAllDelay(.8f,false);
		SoundManager.Instance.Play_ButtonClick();

		GameData.ResetGameProgress();
	}

	public void btnResetNoClicked()
	{
		menuManager.ClosePopUpMenu(PopUpResetMap);
		BlockClicks.Instance.SetBlockAll(true);
		BlockClicks.Instance.SetBlockAllDelay(.8f,false);
		SoundManager.Instance.Play_ButtonClick();
	}



	public void btnRateUsClicked()
	{
		SoundManager.Instance.Play_ButtonClick();
		//if(Rate.alreadyRated !=1)
		//	menuManager.ShowPopUpMenu(PopUpRate);
		Application.OpenURL(Rate.rateURL);
	}


	public void btnPlayClicked()
	{
		LevelTransition.Instance.HideSceneAndLoadNext("Map");
		SoundManager.Instance.Play_ButtonClick();
		BlockClicks.Instance.SetBlockAll(true);
		BlockClicks.Instance.SetBlockAllDelay(.3f,false);
	}

 

}
