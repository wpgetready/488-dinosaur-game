﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.EventSystems;

public class LevelGame_Digger : MonoBehaviour {



	public VerticalLayoutGroup BonesHolder;
	public GameObject HolderFillTop;
	
	int bonesCollected = 0;
	public Transform[] BoneHolders;
	public Transform[] BonePositions;
	
	public ParticleSystem psCollected;
	 
	GameObject CollectedBoneObject = null;
	Vector2 TargetPos  = Vector2.zero;

	bool bEnableDigg = true;
	int ActiveBoneHolderIndex;

	public Sprite[] CrackMasks;
	public Sprite[] Cracks;

	public Transform Pickax;
	public Animator animPickax;

	public static float timeTutorial = 3f;
	float periodTutorial = 5;

	bool bEnabled  = true;
	public CanvasGroup NextButton;
	public Sprite NextButtonSprite;

	public Transform BoneDragHolder;
	public bool bDiggingStarted = false;
	 
	public BoxCollider2D GroundCollider;
	void Awake()
	{

		timeTutorial = 4f;
		int offset  = Mathf.FloorToInt((Screen.width/(float)Screen.height  - 1.7777f)* 57);

		int GroundCollSizeX  = Mathf.FloorToInt((Screen.width/(float)Screen.height  - 1.333f)* 789);
		GroundCollider.size = new Vector2(750 + GroundCollSizeX ,570   );


		BonesHolder.padding.top=offset;
		
		if(Shop.RemoveAds == 2) 
		{
			HolderFillTop.SetActive(false);
		}
		
		
		Transform posBonePom;
		int a =0;
		int b =0;
		for(int i =0; i<BonePositions.Length*2; i++)
		{
			a = Random.Range(0,BonePositions.Length);
			posBonePom = BonePositions[a];
			b = Random.Range(0,BonePositions.Length);
			BonePositions[a] = BonePositions[b];
			BonePositions[b] = posBonePom;
		}
		CreateBones();
		
		InvokeRepeating("TestTutorial",1,1);

	}

	void Start()
	{
		LevelTransition.Instance.ShowScene();
	}

	// Update is called once per frame
	void Update () {
		
		
		if(    Input.GetMouseButtonDown(0)   )
		{
			if(MenuManager.activeMenu != "" || !bEnableDigg) return;
			
			PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
			eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
			List<RaycastResult> results = new List<RaycastResult>();
			EventSystem.current. RaycastAll(eventDataCurrentPosition, results);
			
			
			if(results.Count  >= 1) 
			{
				
			 
				bool bGround = false;
				 
				foreach(RaycastResult r in results)
				{ 
					if(r.gameObject.tag == "Bone" && r.gameObject.transform.childCount>0) 
					{
						bEnableDigg = false;
						StartCoroutine(DiggForBone(r));
						return;
					}
					if(r.gameObject.tag == "Ground") bGround = true;
					if(r.gameObject.tag == "Button") return;
				}

				if( bGround)
				{
					bEnableDigg = false;
					//Debug.Log("Zemlja");
					StartCoroutine(DiggEmptyPlace());
				}
				 
			}
			
		}
		
	}
	


	
	void CreateBones()
	{
		for(int i=1;i<=6;i++)
		{
			GameObject b = GameObject.Instantiate(  Resources.Load<GameObject>( "Dinos/" + GameData.SelectedLevel.ToString() +"/Bone"+i.ToString()));
			b.transform.SetParent(BonePositions[i-1].GetChild(0));

			Vector2 size = b.GetComponent<RectTransform>().sizeDelta;
			float scale = 140f/size.x;
			float scale2 = 100/size.y;
			if(scale2<scale) scale = scale2;
			
			b.transform.localScale = scale*	Vector3.one;
			//b.transform.localScale = 0.6f*Vector3.one;

			b.transform.localPosition = Vector3.zero;
			b.GetComponent<Mask>().enabled = false;
			b.GetComponent<PolygonCollider2D>() .enabled = false;
			Destroy( b.GetComponent<Mask>() );
			Destroy( b.GetComponent<PolygonCollider2D>() );

		}

		for(int i=6;i<BonePositions.Length;i++)
		{
			GameObject.Destroy(BonePositions[i].gameObject);
		}

	}


	public IEnumerator DiggEmptyPlace( )
	{
		timeTutorial = 0;
		if(!bDiggingStarted) 
		{
			animPickax.Play("default");
			animPickax.SetBool("digging",true);
			yield return new  WaitForSeconds(.5f);
			bDiggingStarted = true;
		}



		Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);



		if(Vector2.Distance(Pickax.transform.position ,mousePosition) >.1f)
		{
			Vector3 startPos1 = Pickax.transform.position ;
			for (float t = 0;t <1; t+=(Time.deltaTime*4))
			{
				Pickax.transform.position = Vector3.Lerp(startPos1, mousePosition, t);
				yield return new  WaitForEndOfFrame();
			}
			Pickax.transform.position = mousePosition;
		}
		animPickax.SetTrigger("tDigg");
		yield return new  WaitForSeconds(.3f);

		//Debug.Log("HIT Ground");
		if( SoundManager.Instance!=null ) SoundManager.Instance.Play_Sound( SoundManager.Instance.DiggEmpty );
		yield return new  WaitForSeconds(.2f);
		bEnableDigg = true;
		timeTutorial = 0;

	}



	public IEnumerator DiggForBone(RaycastResult r )
	{
		GameObject _bone =	r.gameObject.transform.GetChild(0).GetChild(0).gameObject;

		timeTutorial = 0;
		if(!bDiggingStarted) 
		{
			animPickax.Play("default");
			//Debug.Log("default");
			animPickax.SetBool("digging",true);
			yield return new  WaitForSeconds(.5f);
			bDiggingStarted = true;
		}

		if(Vector3.Distance(Pickax.transform.position ,_bone.transform.position) >.1f)
		{
			Vector3 startPos1 = Pickax.transform.position ;
			for (float t = 0;t <1; t+=(Time.deltaTime*4))
			{
				Pickax.transform.position = Vector3.Lerp(startPos1, _bone.transform.position, t);
				yield return new  WaitForEndOfFrame();
			}
			Pickax.transform.position = _bone.transform.position;
		}
		animPickax.SetTrigger("tDigg");

		yield return new  WaitForSeconds(.3f);
		if( SoundManager.Instance!=null ) SoundManager.Instance.Play_Sound( SoundManager.Instance.DiggBone );
 
		r.gameObject.transform.GetComponent<DiggLocation>().StartDigging();
		//Debug.Log("Bone");
		yield return new  WaitForSeconds(.2f);
		if(CollectedBoneObject ==null) bEnableDigg = true;
		timeTutorial = 0;
	}
	
	public void FoundBone( GameObject _bone )
	{

		CollectedBoneObject = _bone;

		psCollected.transform.position = CollectedBoneObject.transform.position;
		
		psCollected.Play();
		//ANIMATE BONE
		bonesCollected++;
		 
		StartCoroutine("MoveBone");
 
 		GameObject parentOld = CollectedBoneObject.transform.parent.gameObject;
		CollectedBoneObject.transform.SetParent(BoneDragHolder);
//		CollectedBoneObject.transform.SetParent(BoneHolders[bonesCollected-1]);
 		GameObject.Destroy(parentOld);
		//bDiggingStarted = false;
	}



 

	IEnumerator MoveBone()
	{
		if( SoundManager.Instance!=null ) SoundManager.Instance.Play_Sound( SoundManager.Instance.MoveBoneLeft );
		bEnableDigg = false;
		yield return new WaitForSeconds(1.5f);
		Vector3 StartPosition = CollectedBoneObject.transform.position;
		yield return new WaitForEndOfFrame();
		float pom =0;
		while(pom<1f)
		{
			pom+=Time.deltaTime*3f;
			CollectedBoneObject.transform.position = Vector3.Lerp(StartPosition, BoneHolders[bonesCollected-1].position, pom);
			yield return new WaitForEndOfFrame();
		}
		CollectedBoneObject.transform.position = BoneHolders[bonesCollected-1].position;
		CollectedBoneObject.transform.SetParent(BoneHolders[bonesCollected-1]);
		CollectedBoneObject = null;
		bEnableDigg = true;
		
 

		if(bonesCollected == 6) 
		{
			//Debug.Log ("KRAJ");
			NextButton.alpha = 1;
			NextButton.GetComponent<Button>().enabled = true;
			NextButton.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = NextButtonSprite;
			NextButton.transform.parent.GetComponent<Animator>().SetTrigger("tPulse");
			CancelInvoke("TestTutorial");
			GameObject.Destroy(animPickax.gameObject);
			if( SoundManager.Instance!=null ) SoundManager.Instance.Play_Sound( SoundManager.Instance.LevelFinished );
		}
	}

	void TestTutorial()
	{
		if(timeTutorial <0) CancelInvoke("TestTutorial");
		if(bEnabled)
			timeTutorial ++;
		else return;

		if(timeTutorial == (periodTutorial-2))
		{
			animPickax.SetBool("digging",false);
			bDiggingStarted = false;
		}
			
		if(timeTutorial == periodTutorial)
		{
			if(!bDiggingStarted)
			{
				List<Vector3> sakrivene = new List<Vector3>();
				for(int i = 0;i<6;i++)
				{
					if(BonePositions[i].childCount>0)
					{
						if(BonePositions[i].GetComponent<DiggLocation>().phase > 0)
						{
							Pickax.transform.position = BonePositions[i].position;
							timeTutorial = 0;
							animPickax.Play("tutorial");
							return;
						}
						else
							sakrivene.Add(BonePositions[i].position);
					}
				}
				if(sakrivene.Count>0)
				{
					int bone = Random.Range(0,sakrivene.Count);
					Pickax.transform.position =  sakrivene[bone];
					timeTutorial = 0;	 
					animPickax.Play("tutorial");
				}
//				for(int i = 0;i<6;i++)
//				{
//					if(BonePositions[i].childCount>0)
//					{
//						Pickax.transform.position = BonePositions[i].position;
//					}
//				}

			}
//			timeTutorial = 0;
//			//animPickax.SetTrigger("tTutorial");
//			animPickax.Play("tutorial");
		}


	}








	public void ButtonBackClicked()
	{
		LevelTransition.Instance.HideSceneAndLoadNext("Map");
		SoundManager.Instance.Play_ButtonClick();
		BlockClicks.Instance.SetBlockAll(true);
		GameData.IncrementBackButtonClickedCount();
	}

	public void ButtonNextClicked()
	{
		LevelTransition.Instance.HideSceneAndLoadNext("MainGamePuzzle");
		SoundManager.Instance.Play_ButtonClick();
		BlockClicks.Instance.SetBlockAll(true);
		GameData.IncrementForwardButtonClickedCount();
	}
}
