﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class DragBonePuzzle : MonoBehaviour , IBeginDragHandler, IDragHandler, IEndDragHandler
{
	public static bool bEnableDrag = true;
	bool bDrag = false;
	bool bFinished;
	bool bMovingBack = false;

	public Vector3 StartPosition;
	public Vector3 StartScale;
	 
	public Quaternion startRotation;

	public Vector2 EndPosition;

	Vector3 EndLocationScale = Vector3.one;
	float endRotationZ = 0;

	float SnapDistance = 1f;

	float x;
	float y;
	Vector3 diffPos = new Vector3(0,0,0);
 
	public RectTransform recTr;

	Transform ParentOld;
	Transform DragItemParent; 

 
	IEnumerator Start () {
		yield return new WaitForSeconds(.1f);
		ParentOld = transform.parent;
		DragItemParent = GameObject.Find("DragItemParent").transform;
	}
	
	 
	IEnumerator MoveBack(  )
	{
		Tutorial.bTutorial = false;	
		bEnableDrag = false;
		yield return new WaitForEndOfFrame( );
		if( SoundManager.Instance!=null ) 
		{
			SoundManager.Instance.BonePuzzleMoveBack.Stop();
			SoundManager.Instance.Play_Sound( SoundManager.Instance.BonePuzzleMoveBack );
		}
		if(!bMovingBack)
		{
			bMovingBack = true;
			
			
			yield return new WaitForEndOfFrame( );
			float pom = 0;
			while(pom<1 )
			{ 
				
				pom+=2*Time.deltaTime;
				transform.position = Vector3.Lerp(transform.position, StartPosition,pom);
				transform.localScale = Vector3.Lerp(transform.localScale, StartScale,pom);
				transform.localRotation = Quaternion.Lerp(transform.localRotation, startRotation,pom);
				yield return new WaitForEndOfFrame( );
			}
			
			transform.position = StartPosition;
			transform.localScale = StartScale;
			transform.localRotation = startRotation;
			bMovingBack = false;
			 
			transform.SetParent(ParentOld); 
		}
		

		bEnableDrag = true;
		Tutorial.bTutorial = true;	 
	}
	
	
	
	
	
	
	void TestDistance()
	{
		if(Vector2.Distance(( Vector2) transform.position  ,EndPosition) < SnapDistance)
		{
			StartCoroutine("SnapToParent");
			
			bDrag = false;
			bFinished = true;

		}
	}
	
	
	
	
	public void OnBeginDrag (PointerEventData eventData)
	{
		if(!bEnableDrag) return;
		if(!bDrag && !bFinished )
		{

			bDrag = true;
			InvokeRepeating("TestDistance",0, .2f);
			diffPos =transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition)   ;
			diffPos = new Vector3(diffPos.x,diffPos.y,0);
 
			transform.localRotation = Quaternion.identity;
			transform.localScale = EndLocationScale;

			Tutorial.bTutorial = false;	
			Tutorial.instance.HideTutorial();
//			if(Tutorial.bTutorial)
//			{
//				 Tutorial.instance.StopTutorial();	

//			}

			transform.SetParent(DragItemParent);
		}
	}
	 
	void Update()
	{
		if( bDrag  && bEnableDrag)  
		{
			
			x = Input.mousePosition.x;
			y = Input.mousePosition.y;
			
			transform.position =     Vector3.Lerp (transform.position, Camera.main.ScreenToWorldPoint(new Vector3(x ,y,10.0f) )  ,10* Time.deltaTime)  ;
			 
			//transform.position =  new Vector3(transform.position .x,transform.position.y, 0);
		}
	}
	
	
	public void OnEndDrag (PointerEventData eventData)
	{
		if(bDrag  &&  !bFinished)
		{
			transform.localScale = StartScale;
			bDrag = false;
			
			CancelInvoke("TestDistance");
			StartCoroutine("MoveBack" );
		}
	}
	
	
	public void OnDrag (PointerEventData eventData)
	{
		
	}
	
	
	IEnumerator SnapToParent()
	{
		Tutorial.bTutorial = false;	
		bEnableDrag = false;
		transform.GetComponent<PolygonCollider2D>().enabled = false;

		transform.SetParent(ParentOld); 
		CancelInvoke("TestDistance");
		float timeMove = 0;
		bool animationStarted = false;
		 
		if( SoundManager.Instance!=null ) 
		{
			SoundManager.Instance.BonePuzzleInPlace.Stop();
			SoundManager.Instance.Play_Sound( SoundManager.Instance.BonePuzzleInPlace );
		}

		while  (timeMove  <1 )
		{
			yield return new WaitForFixedUpdate();
			transform.position = Vector3.Lerp (transform.position, EndPosition , timeMove)  ;
			transform.localScale  = Vector3.Lerp (transform.localScale,  EndLocationScale,  timeMove );
			timeMove += Time.fixedDeltaTime*2;
			
		}
 
		yield return new WaitForFixedUpdate();
		
		 
		Camera.main.SendMessage("BonePositioned", this , SendMessageOptions.DontRequireReceiver);

		bEnableDrag = true;
		
		this.enabled = false;
		Tutorial.bTutorial = true;	
		
	}


	bool appFoucs = true;
	void OnApplicationFocus( bool hasFocus )
	{
		if(  !appFoucs && hasFocus )
		{
			if(  bDrag )
			{
				bDrag = false;
				
				CancelInvoke("TestDistance");
				StartCoroutine("MoveBack" );
			}
		}
		appFoucs = hasFocus;
		
	}
	

}
