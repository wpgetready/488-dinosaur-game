﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameData : ScriptableObject {

  
	public static string sTestiranje = "";

	public static int WatchVideoCounter = 0;

	public static int LastUnlockedLevel = 4;
	public static int LastCompletedLevel = 0;
	public static int SelectedLevel = 1;
	public static int LevelToAnimateUnlocking = -1;
 
	static int[] LevelUnlockScore = new int[]   {20, 25, 30, 35, 40, 45, 50, 60};//{3, 6, 9, 12, 15, 18, 21, 24, 27, 30};


	public static void Init()
	{
/*	 
#if UNITY_EDITOR
		if(   true  ) 
		{
			//brisi
			//PlayerPrefs.SetInt("Level",0) ;
			//Level  = 0;

			sTestiranje = "Test;"
			 	//+ "OverrideShopCall;"  
				//+ "TestPopUpTransaction;"	
			 	+ "WatchVideo;"
				+ "InternetOff;"
			 ;

			//Debug.Log("TESTIRANJE UKLJUCENO: " + sTestiranje);

		}
	#endif
*/	 
 
		 
	}


 

	public  static void SetCompletedLevelToPP(  )
	{
		LastCompletedLevel++;
		SetLevelsData(); 
	}

	public  static void UnlockNewLevel(  )
	{
		//Debug.Log(LastUnlockedLevel);
		LastUnlockedLevel++;
		LevelToAnimateUnlocking = LastUnlockedLevel;
		//Debug.Log("UNLOCEKD: "+LastUnlockedLevel);
		SetLevelsData(); 
	}


  
	 
	public static void GetLevelsData()
	{
		string tmp = PlayerPrefs.GetString("Data1","6549");
		tmp= tmp.Replace("<","9");
		tmp= tmp.Replace("7>q","8");
		tmp= tmp.Replace("nmFs","7");
		tmp= tmp.Replace("Vy;","6");
		tmp= tmp.Replace("*2","5");
		tmp= tmp.Replace("H","4");
		tmp= tmp.Replace("JE","3");
		tmp= tmp.Replace("B#","2");
		tmp= tmp.Replace("+0","1");
		tmp= tmp.Replace("Kce","0");
		
		int tmpPurchased = int.Parse(tmp);
		int tmp1 = tmpPurchased - 6149;

 
		LastUnlockedLevel = Mathf.FloorToInt( tmp1/100);
		LastCompletedLevel = tmp1%100;
		// LastUnlockedLevel = 18;
		// LastCompletedLevel = 18;
 
		//Debug.Log(LastUnlockedLevel +  " ,  " + LastCompletedLevel);
 
	}

	public static void SetLevelsData()
	{
   
 
		int tmp1 =  LastUnlockedLevel*100+LastCompletedLevel;
 
		string tmp = (tmp1+6149).ToString();
		
		tmp= tmp.Replace("0","Kce");
		tmp= tmp.Replace("1","+0");
		tmp= tmp.Replace("2","B#");
		tmp= tmp.Replace("3","JE");
		tmp= tmp.Replace("4","H");
		tmp= tmp.Replace("5","*2");
		tmp= tmp.Replace("6","Vy;");
		tmp= tmp.Replace("7","nmFs");
		tmp= tmp.Replace("8","7>q");
		tmp= tmp.Replace("9","<");
  
		PlayerPrefs.SetString("Data1", tmp);
		PlayerPrefs.Save();
	}

 

	public static void ResetGameProgress()
	{

		LastUnlockedLevel = 4;
		LastCompletedLevel = 0;
		SetLevelsData();
	}



	public static int ReturnRequiredScoreToUnlockLevel()
	{
		int i = LastUnlockedLevel-4;
		if(i<LevelUnlockScore.Length)
			return LevelUnlockScore[i];
		else
			return LevelUnlockScore[LevelUnlockScore.Length-1];
	}
	 
	//BROJANJE KLIKOVA ZA PRIKAZ REKLAMA
 
 
	 
	public static void IncrementBackButtonClickedCount()
	{
		//if (Shop.RemoveAds !=2    )
		//{ 
			 AdsManager.Instance.ShowInterstitial();
		//}
	}

	public static void IncrementForwardButtonClickedCount()
	{
		//if (Shop.RemoveAds !=2    )
		//{
			 AdsManager.Instance.ShowInterstitial();
		//}
	}
 
}

 

