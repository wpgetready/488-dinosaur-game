using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class CleaningTool : MonoBehaviour  , IBeginDragHandler, IDragHandler, IEndDragHandler
{


 
	public ToolType toolType;
	Vector3 StartPosition ;
	bool bIskoriscen = false;
	[HideInInspector()]
	public  bool bDrag = false;
	 
	public static int OneToolEnabledNo = 1;
	 
	float x;
	float y;
	Vector3 diffPos = new Vector3(0,0,0);
	float testDistance = .5f;//1;// .25f;

	public int ToolNo = 0;
	 
	public static int activeToolNo = 0;
	public static bool bCleaning = false;
	public static GameObject  itemBeingDragged;
 
	Vector3 offPos = Vector3.zero;
	Transform trPom;

	Transform TestPoint;

	public ToolBehavior toolBehavior; 
	ParticleSystem psFinishCleaningAll;

	PointerEventData pointerEventData;
	public Quaternion StartRotation;

	public Animator AnimationChild; 


	Transform ParentOld;
	Transform DragItemParent;

	int ToolCollectedStars = 0;
	Transform StarsHolder;  

	public GameObject BubblesPref;
	public List< Animator> bubblesAnim= new  List<Animator>();
	int bubblesCount;
	public CleaningTool AssociatedTool;
	public void Awake()
	{
		//OneToolEnabledNo = 1;//na pocetku je dozvoljena upotreba samo jednog alata (prvog) ...kada se postavi -1 mogu da se koriste svi, a ako je 0 onda ni jedan..
		OneToolEnabledNo = 0;
		bCleaning = false;
		bDrag = false;
		activeToolNo = 0;
	}

	IEnumerator Start()
	{
		yield return new WaitForSeconds(0.1f);
		 
		DragItemParent = GameObject.Find("DragItemParent").transform;
 
		StartPosition  = transform.position;
		 
		TestPoint = transform.FindChild("TestPoint");
		ParentOld = transform.parent;
		StarsHolder = ParentOld.FindChild("StarsHolder");
		ResetTool();
	}
	 
	void Update()
	{ 
		if(   bDrag )
		{
			x = Input.mousePosition.x;
			y = Input.mousePosition.y;

		//	transform.position =     Vector3.Lerp (transform.position, Camera.main.ScreenToWorldPoint(new Vector3(x ,y,100.0f))  - offPos ,10* Time.deltaTime);
		//	if( transform.position.y > 2.5f) transform.position  = new Vector3(transform.position .x,2.5f,transform.position.z);
		//	//else if( transform.position.y < -3.5f) transform.position  = new Vector3(transform.position .x,-3.5f,transform.position.z);
 
 		 
	 
			Vector3 posM = Camera.main.ScreenToWorldPoint(new Vector3(x ,y,100f) ) - offPos;
			if(posM.x<-1.5f && posM.y<-1.3f) posM = new Vector3(posM.x,-1.3f,posM.z);
			else if(posM.y>2.5f) posM = new Vector3(posM.x,2.5f,posM.z);
			else if(posM.y<-3.5f) posM = new Vector3(posM.x,-3.5f,posM.z);
			transform.position =  Vector3.Lerp (transform.position, posM  , 10 * Time.deltaTime)  ;
		}
	}

	void SetStars( )
	{
		//if( toolType != ToolType.cloth )
		{
			for (int i = 1; i <= StarsHolder.childCount; i++) 
			{
				StarsHolder.FindChild("Star"+i.ToString()+"BG").GetChild(0).gameObject.SetActive(i<=ToolCollectedStars); 
			}
		}
//		else
//		{
//			for (int i = 1; i <= StarsHolder.childCount; i++) 
//			{
//				StarsHolder.FindChild("Star"+i.ToString()+"BG").GetChild(0).gameObject.SetActive(i<=ToolCollectedStars); 
//			}
//		}
		 
	}

	public void ResetTool()
	{
		ToolCollectedStars = 0;
		//if( toolType != ToolType.cloth )  
			SetStars( );
		bIskoriscen = false;
		transform.FindChild("Finished").GetComponent<Image>().enabled = false;
		if( toolType == ToolType.sponge  ) bubblesAnim.Clear();
	}


	
	#region IBeginDragHandler implementation

	public void OnBeginDrag (PointerEventData eventData)
	{
		if(bMovingBack) return;
		StopAllCoroutines();//("MoveBack");
		pointerEventData = eventData;
		bCleaning = false;
		if(OneToolEnabledNo >-1 && ToolNo != OneToolEnabledNo)
		{
			bDrag = false;
			return;
		}
 
		if(  !bIskoriscen   && !bDrag  )
		{
			transform.localScale = 1.7f*Vector3.one;
			AnimationChild.transform.parent.rotation = Quaternion.Euler(0,0,0);
			activeToolNo = ToolNo;
		//	SoundManager.Instance.Play_ToolClick();
			bDrag = true;
			diffPos =transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition)   ;
			diffPos = new Vector3(diffPos.x,diffPos.y,0);

			//transform.parent = ActiveToolParent;


			InvokeRepeating("TestClean",0f, .1f);

			transform.SetParent(DragItemParent);

	 	 	if( toolType == ToolType.sponge  )
		  	{
				//if(ToolCollectedStars<2)
				//{
					InvokeRepeating("CreateBubbles",0f, .3f);
				//}

				if( Tutorial.Instance!=null) 	Tutorial.Instance.StopTutorial();
			}


		}
 
  
	}

	int startBubblesCount = 0;
	void CreateBubbles()
	{
		if(toolBehavior == ToolBehavior.AnimateOnlyWhenMovingOverObject && !pointerEventData.IsPointerMoving()) return;
		Collider2D[] hitColliders = Physics2D.OverlapCircleAll(TestPoint.position, testDistance  , 1 << LayerMask.NameToLayer("Tool"+ToolNo.ToString()+"Interact")); //layermask to filter the varius colliders
		if(hitColliders.Length > 0  )
		{
			bubblesCount++;
			GameObject bp = GameObject.Instantiate(BubblesPref);
			bp.transform.SetParent ( hitColliders[0].transform.parent);//.parent.parent );
			bp.transform.position = transform.position;
			bubblesAnim.Add(bp.GetComponent<Animator>() );

			if(bubblesCount >=(startBubblesCount+3) && ToolCollectedStars<5)
			{

				trPom.GetChild(0).GetChild(0).SendMessage("Cleaned",ToolNo,SendMessageOptions.DontRequireReceiver);
				startBubblesCount = bubblesCount;

				ToolCollectedStars++;
				
				SetStars( );
				if(toolType == ToolType.sponge && ToolCollectedStars ==5)
				{
					for (int j = bubblesAnim.Count-1; j >=0; j--) 
					{
						bubblesAnim[j].SetTrigger("tHide");
						GameObject.Destroy(bubblesAnim[j].gameObject,2f);
					}	 
					CancelInvoke("CreateBubbles");
					bubblesAnim.Clear();

					if(ToolCollectedStars ==5) ToolCleaningFinished();
				}
			}
		}	
	}
 
	void TestClean()
	{ 
		if(bCleaning) return;
		if(toolBehavior == ToolBehavior.AnimateOnlyWhenMovingOverObject && !pointerEventData.IsPointerMoving()) return;
		Collider2D[] hitColliders;
	 	if(ToolNo<4)
		 	hitColliders = Physics2D.OverlapCircleAll(TestPoint.position, testDistance  , 1 << LayerMask.NameToLayer("Tool"+ToolNo.ToString()+"Interact")); //layermask to filter the varius colliders
		else hitColliders = Physics2D.OverlapCircleAll(TestPoint.position, testDistance  , 1 << LayerMask.NameToLayer("Tool3Interact"));
		 
		if(hitColliders.Length > 0  )
		{
			 
			trPom = null;
			for (int i =0 ; i<hitColliders.Length; i++)    
			{
				//hitColliders[i].transform.SendMessage("FadeOut_CleaningTool",SendMessageOptions.DontRequireReceiver);
				//RoomScene.ToolActionsLeft[ToolNo-1] --;
				trPom = hitColliders[i].transform;
				//if(RoomScene.ToolActionsLeft[ToolNo-1] <= 0) bIskoriscen = true;
				bCleaning = true;
				break;
			}


			if( toolType == ToolType.sponge  )
			{
				AnimationChild.SetTrigger("tUpDownClean");  
				if( SoundManager.Instance!=null )  SoundManager.Instance.StopAndPlay_Sound(SoundManager.Instance.SpongeSound);
			}
			else if( toolType == ToolType.glass_spray ) 
			{
				AnimationChild.SetTrigger("tGlassSpary");
				//AnimationChild.transform.GetChild(0).GetComponent<ParticleSystem>().Play();
				if( SoundManager.Instance!=null ) SoundManager.Instance.StopAndPlay_Sound(SoundManager.Instance.SpraySound);
			}
			else  	if( toolType == ToolType.cloth ) 
			{
				 
				AnimationChild.SetTrigger("tCloth");
				if( SoundManager.Instance!=null ) SoundManager.Instance.StopAndPlay_Sound(SoundManager.Instance.ClothSound);
			}
			else if( toolType == ToolType.chisel) 
			{
				AnimationChild.SetTrigger("tUpDownClean");
				AnimationChild.transform.GetChild(0).GetComponent<ParticleSystem>().Play();
				if( SoundManager.Instance!=null )  SoundManager.Instance.StopAndPlay_Sound(SoundManager.Instance.ChiselSound);
			}
			 
		}
		 
	}


	void ToolCleaningFinished()
	{
		if(!bIskoriscen)
		{
			bIskoriscen = true;
			bDrag = false;
			activeToolNo++;
			OneToolEnabledNo = activeToolNo;
			//Debug.Log(activeToolNo);
			if(!bMovingBack)
			{
				StopAllCoroutines();
				StartMoveBack();
			}
			if(activeToolNo == 5) 
			
				Camera.main.SendMessage("NextPhase",SendMessageOptions.DontRequireReceiver);
		}
	}

	 

	public void CleaningAnimationFinished()
	{
		bCleaning = false;
		//if(toolBehavior == ToolBehavior.AnimateOnlyWhenMovingOverObject && !pointerEventData.IsPointerMoving()) return;
		if(trPom!=null) 
		{
			Collider2D[] hitColliders ;
			if(ToolNo==3)
				hitColliders = Physics2D.OverlapCircleAll(TestPoint.position, testDistance *10  , 1 << LayerMask.NameToLayer("Tool3Interact")); //layermask to filter the varius colliders
			else if(ToolNo<4)
				hitColliders = Physics2D.OverlapCircleAll(TestPoint.position, testDistance *4  , 1 << LayerMask.NameToLayer("Tool"+ToolNo.ToString()+"Interact")); //layermask to filter the varius colliders
			else hitColliders = Physics2D.OverlapCircleAll(TestPoint.position, testDistance *4  , 1 << LayerMask.NameToLayer("Tool3Interact"));

				 
			if(hitColliders.Length > 0 )
			{
				for (int i =0 ; i<hitColliders.Length; i++)    
				{
					if(trPom == hitColliders[i].transform)
					{
						if( toolType == ToolType.sponge  )
						{
							trPom.GetChild(0).GetChild(0).SendMessage("Cleaned",ToolNo,SendMessageOptions.DontRequireReceiver);
							startBubblesCount = bubblesCount;
						}
						else if( toolType != ToolType.glass_spray  )
							trPom.SendMessage("Cleaned",ToolNo,SendMessageOptions.DontRequireReceiver);

						if(ToolCollectedStars<5 )
						{
							ToolCollectedStars++;
							SetStars( );
						
							if(toolType == ToolType.sponge && ToolCollectedStars ==5)
							{
								for (int j = bubblesAnim.Count-1; j >=0; j--) 
								{
									bubblesAnim[j].SetTrigger("tHide");
									GameObject.Destroy(bubblesAnim[j].gameObject,2f);
								}	 
								CancelInvoke("CreateBubbles");
								bubblesAnim.Clear();
							}

							 

						}
//
//						if(toolType == ToolType.glass_spray && ToolCollectedStars ==1)
//						{
//							bIskoriscen = true;
//							bDrag = false;
//							activeToolNo++;
//							OneToolEnabledNo = activeToolNo;
//							StopAllCoroutines();
//							StartMoveBack();
//
//							AssociatedTool.ToolCollectedStars = 1;
//						}

					 
						if(ToolCollectedStars ==5) ToolCleaningFinished();
						break;
					}
				}
			}
		}

		if( toolType == ToolType.sponge  )
		{ 
			//SoundManager.Instance.Stop_Sound(SoundManager.Instance.SpongeSound);
		}
		else if( toolType == ToolType.glass_spray ) 
		{
			//SoundManager.Instance.Stop_Sound(SoundManager.Instance.SpraySound);
		}
 		else  if( toolType == ToolType.cloth ) 
		{
		//	SoundManager.Instance.Stop_Sound(SoundManager.Instance.CleaningCloth);
		}
		else if( toolType == ToolType.chisel ) 
		{
			//SoundManager.Instance.Stop_Sound(SoundManager.Instance.CrumbsCleaner);
		}
		
	}
	


	#endregion
	
	#region IDragHandler implementation
	
	public void OnDrag (PointerEventData eventData)
	{
		 
	}
	
	#endregion
	
	#region IEndDragHandler implementation
	
	public void OnEndDrag (PointerEventData eventData)
	{
		if(toolType == ToolType.sponge) CancelInvoke("CreateBubbles");
		if(  !bIskoriscen &&  bDrag 	) //&& activeToolNo == ToolNo  )
		{

			 
			bDrag = false;

			CancelInvoke("TestClean");
			StartCoroutine("MoveBack" );
			if( toolType == ToolType.chisel ||  toolType == ToolType.glass_spray ) 
			{
				AnimationChild.transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
				//Debug.Log(AnimationChild.transform.GetChild(0).name+ "***");
			}
		}
	}
		#endregion

	 
	bool bMovingBack = false;
	IEnumerator MoveBack(  )
	{
		if(!bMovingBack)
		{
			bMovingBack = true;

			//SoundManager.Instance.StopAndPlay_Sound(SoundManager.Instance.ShowMenu);
			yield return new WaitForEndOfFrame( );
			if(SoundManager.Instance!=null)
			{
				if( toolType == ToolType.sponge  )
				{ 
					 SoundManager.Instance.Stop_Sound(SoundManager.Instance.SpongeSound);
				}
				else if( toolType == ToolType.glass_spray ) 
				{
					 SoundManager.Instance.Stop_Sound(SoundManager.Instance.SpraySound);
				}
				else  if( toolType == ToolType.cloth ) 
				{
					 SoundManager.Instance.Stop_Sound(SoundManager.Instance.ClothSound);
				}
				else if( toolType == ToolType.chisel ) 
				{
					SoundManager.Instance.Stop_Sound(SoundManager.Instance.ChiselSound);
				}
			}

			float pom = 0;
			while(pom<1 )
			{ 
				pom+=Time.deltaTime;
				transform.position = Vector3.Lerp(transform.position, StartPosition,pom);
 
				if(transform.localScale.x >  1) transform.localScale =  (1.7f - 2*pom)*Vector3.one;
				else transform.localScale =  Vector3.one;
				yield return new WaitForEndOfFrame( );
			}
		 
			transform.SetParent(ParentOld);
			transform.position = StartPosition;
				
	
			//activeToolNo = 0;
			bMovingBack = false;
			if(bIskoriscen) 
			{

				transform.FindChild("Finished").GetComponent<Image>().enabled = true;
				//SoundManager.Instance.StopAndPlay_Sound(SoundManager.Instance.CleaningFinished);
			}
		}
		Tutorial.bPause = false;	 
	}

	public void StartMoveBack()
	{
		CancelInvoke("TestClean");
		StartCoroutine("MoveBack" );
	
	}

	bool appFoucs = true;
	void OnApplicationFocus( bool hasFocus )
	{
		if(  !appFoucs && hasFocus )
		{
			if(  !bIskoriscen &&  bDrag )
			{
				bDrag = false;
				
				CancelInvoke("TestClean");
				StartCoroutine("MoveBack" );
			}
		}
		appFoucs = hasFocus;
		
	}

	 
}

public enum ToolBehavior
{
	AnimateWhenHoveringOverObject,
	AnimateWhenDroppedOnObject,
	AnimateOnlyWhenMovingOverObject

}

public enum ToolType
{

	sponge,
	roller_brush,
	duster,
	cloth,
	brush,
	glass_spray,
	chisel,
	none
	
}
