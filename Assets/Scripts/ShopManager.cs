﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
 

public class ShopManager : MonoBehaviour {

	/*
	MenuManager menuManager;
	 
	 
	 
	 
	public Text txtPrice_RemoveAds;
	public Text txtSmallCoinPack;
	public Text txtMediumCoinPack;
	public Text txtLargeCoinPack;
	public Text txtSpecialOffer;

	public Button buttonSpecialOffer;
	public Button buttonBuySpecialOffer;
 
	CanvasGroup BlockAll;
 
	public static bool bShowShop = false;

	public static ShopManager Instance;

	void Awake () {
		InitPrices();
		Instance= this;
	}

	void Start () {



		bShowShop = false;
		menuManager = GameObject.Find("Canvas").GetComponent<MenuManager>();
		 
		BlockAll = GameObject.Find("BlockAll").GetComponent<CanvasGroup>();

		InitPrices();
		SetShopItems();

	 
//		if(Shop.RemoveAds !=2   ) GameObject.Find("PopUps").GetComponent<RectTransform>().anchoredPosition = new Vector2(0,-30);

		#if UNITY_ANDROID
		 
//		GameObject restoreP= GameObject.Find("TabButtonRestorePurchase"); 
//		restoreP.transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2 (600, 400);
//		GameObject.Destroy(restoreP);
		
		#endif
	}


	void InitPrices()
	{
		if(txtPrice_RemoveAds !=null) txtPrice_RemoveAds .text = WebelinxAndroidInApps.removeAdsPrice; 
		//if(txtSmallCoinPack !=null) txtSmallCoinPack.text =   WebelinxAndroidInApps.smallCoinPackPrice;	 
		//if(txtMediumCoinPack !=null) txtMediumCoinPack.text =   WebelinxAndroidInApps.mediumCoinPackPrice;	 
		//if(txtLargeCoinPack !=null) txtLargeCoinPack.text =   WebelinxAndroidInApps.largeCoinPackPrice;	 
		if(txtSpecialOffer !=null) txtSpecialOffer.text =   WebelinxAndroidInApps.specialOfferPrice;	 
 
		//*******************************************************
 
		if(  Shop.RemoveAds == 2 )
		{
			txtPrice_RemoveAds.text = "bought"; 
			txtPrice_RemoveAds.transform.parent.parent.GetComponent<Button>().enabled = false;
			txtPrice_RemoveAds.transform.parent.GetComponent<Button>().enabled = false;
			txtPrice_RemoveAds.transform.parent.GetComponent<Image>().enabled = false;//  .color = col;

			if(buttonSpecialOffer!=null)
			{
				GameObject.Destroy(buttonSpecialOffer.gameObject);
				buttonSpecialOffer = null;
			}
			if(buttonBuySpecialOffer!=null && MenuManager.activeMenu == "PopUpSpecialOffer")
			{
				buttonBuySpecialOffer.enabled = false;
				buttonBuySpecialOffer.GetComponentInChildren<Text>().text = "bought";
			}

		}

	//konzumabilni
 
		if(  Shop.SmallCoinPack == 2 )
		{
			txtPrice_Room3.text = "bought"; 
			txtPrice_Room3.transform.parent.parent.GetComponent<Button>().enabled = false;
			txtPrice_Room3.transform.parent.GetComponent<Button>().enabled = false;
			txtPrice_Room3.transform.parent.GetComponent<Image>().enabled = false;
		}

		if(  Shop.MediumCoinPack == 2 )
		{
			txtPrice_Room4.text = "bought"; 
			txtPrice_Room4.transform.parent.parent.GetComponent<Button>().enabled = false;
			txtPrice_Room4.transform.parent.GetComponent<Button>().enabled = false;
			txtPrice_Room4.transform.parent.GetComponent<Image>().enabled = false;
		}

		if(  Shop.LargeCoinPack == 2 )
		{
			txtPrice_Room5.text = "bought"; 
			txtPrice_Room5.transform.parent.parent.GetComponent<Button>().enabled = false;
			txtPrice_Room5.transform.parent.GetComponent<Button>().enabled = false;
			txtPrice_Room5.transform.parent.GetComponent<Image>().enabled = false;
		}
 
 
 
	}


	public void btnTestBuyClick(string btnID)
	{
	 
		if(btnID == "remove_ads" &&  Shop.RemoveAds == 0)  WebelinxAndroidInApps.Instance.InAppBought(btnID+"#0");
		if(btnID == "small_coin_pack"   )  WebelinxAndroidInApps.Instance.InAppBought(btnID+"#0");
		if(btnID == "medium_coin_pack"  )  WebelinxAndroidInApps.Instance.InAppBought(btnID+"#0");
		if(btnID == "large_coin_pack"  )  WebelinxAndroidInApps.Instance.InAppBought(btnID+"#0");
		if(btnID == "special_offer" &&  Shop.RemoveAds == 0 )  WebelinxAndroidInApps.Instance.InAppBought(btnID+"#0");

		if(btnID == "watch_video" )  WebelinxCMS.Instance.VideoRewardStatus( "Reward#"+WebelinxCMS.WATCH_VIDEO_ID.ToString());
		 
	}
	public void btnBuyClick(string btnID)
	{
 
		BlockClicks.Instance.SetBlockAll(true);
		BlockClicks.Instance.SetBlockAllDelay(1f,false);
	
		if(btnID == "RemoveAds" &&  Shop.RemoveAds == 0)  Shop.Instance.SendShopRequest(btnID);
		if(btnID == "SmallCoinPack"   )  Shop.Instance.SendShopRequest(btnID);
		if(btnID == "MediumCoinPack"  )  Shop.Instance.SendShopRequest(btnID);
		if(btnID == "LargeCoinPack"  )  Shop.Instance.SendShopRequest(btnID);
		if(btnID == "SpecialOffer"  &&  Shop.RemoveAds == 0)  Shop.Instance.SendShopRequest(btnID);
 
		 
		if(SoundManager.Instance !=null ) SoundManager.Instance.Play_ButtonClick();

 
	}





	public void SetShopItems()
	{
	 
		if(  Shop.RemoveAds == 2 )
		{
			//txtPrice_RemoveAds.transform.parent.GetComponent<Image>().color = col;
			txtPrice_RemoveAds.text = "bought"; 
			txtPrice_RemoveAds.transform.parent.parent.GetComponent<Button>().enabled = false;
			txtPrice_RemoveAds.transform.parent.GetComponent<Button>().enabled = false;
			txtPrice_RemoveAds.transform.parent.GetComponent<Image>().enabled = false;//  .color = col;

			if(buttonSpecialOffer!=null)
			{
				GameObject.Destroy(buttonSpecialOffer.gameObject);
				buttonSpecialOffer = null;
			}
			if(buttonBuySpecialOffer!=null && MenuManager.activeMenu == "PopUpSpecialOffer")
			{
				buttonBuySpecialOffer.enabled = false;
				buttonBuySpecialOffer.GetComponentInChildren<Text>().text = "bought";
			}

			BlockClicks.Instance.SetBlockAll(true);
			BlockClicks.Instance.SetBlockAllDelay(1f,false);
		}

		 
 
 
	}
 
	public void ShowPopUpShop()
	{
		if(BlockAll == null) BlockAll = GameObject.Find("BlockAll").GetComponent<CanvasGroup>();
		BlockAll.blocksRaycasts = true;
		InitPrices();
		if(menuManager == null) menuManager = GameObject.Find("Canvas").GetComponent<MenuManager>();
 
		menuManager.ShowPopUpMenu (gameObject);
		StartCoroutine(SetBlockAll(1f,false));
 
		bShowShop = true;
 
	}

	 
 
	//**********************************************************
	 

	IEnumerator SetBlockAll(float time, bool blockRays)
	{
		if(BlockAll == null) BlockAll = GameObject.Find("BlockAll").GetComponent<CanvasGroup>();
		yield return new WaitForSeconds(time);
		BlockAll.blocksRaycasts = blockRays;
		 
	}


	//ovo je funkcija koja sluzi samo da pusti zvuk
	public void btnClicked_PlaySound()
	{
		if(SoundManager.Instance !=null ) SoundManager.Instance.Play_ButtonClick();
	}

	public void btnRestorePurchaseClick()
	{
		 
	}



	 */
}
