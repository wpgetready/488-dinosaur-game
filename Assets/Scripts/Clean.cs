﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Clean : MonoBehaviour {
	float alpha = 1;
	Image dirtImg;
	public void Cleaned()
	{
		alpha-=0.2f;
		if(dirtImg == null) dirtImg = transform.GetComponent<Image>();
		dirtImg.color = new Color(1,1,1,alpha);
	}
}
