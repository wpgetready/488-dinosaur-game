﻿using UnityEngine;
using System.Collections;

public class MoveCatchedItem : MonoBehaviour {

	public Transform Destiantion;
	public Vector3 StartPosition;
 


	IEnumerator SetGlowAndMove()
	{
		LevelGameFishing.bEnableFishing = false;
		//SoundManager.Instance.Play_Sound(SoundManager.Instance.Good);
		yield return new WaitForSeconds(1);
		transform.SetParent(Destiantion);
		yield return new WaitForEndOfFrame();
		float pom =0;
		while(pom<1f)
		{
			pom+=Time.deltaTime*3f;
			transform.position = Vector3.Lerp(StartPosition, Destiantion.position, pom);
			yield return new WaitForEndOfFrame();
		}
		transform.position = Destiantion.position;

		LevelGameFishing.bEnableFishing = true;
		this.enabled= false;
	
	}


	
	IEnumerator DisposeJunk()
	{
		 
		LevelGameFishing.bEnableFishing = false;
		float pomX = Destiantion.position.x -transform.position.x;
		float pomY = Destiantion.position.y - transform.position.y;
		Vector3 pos;
		float startX   = StartPosition.x;
		float startY   = StartPosition.y;
		float speedY =  pomY/pomX;
		float dX = pomX;
		
		//SoundManager.Instance.Play_Sound(SoundManager.Instance.Trash);
		int breaklock = 0;
		// float dist = Vector2.Distance(transform.position,Destiantion.position);
		while  ((transform.position.x < Destiantion.position.x-.3f) && breaklock <5000) 
			//(dist  >0.3f && dist <20 )
		{
			breaklock++;
			yield return new WaitForEndOfFrame();
			//Debug.Log (dist) ;
			pos = transform.position;
			dX  = (pos.x - startX);
			
			pos.x +=pomX*Time.deltaTime;
			pos.y  =   startY  +  dX*  speedY  +      5*   (Destiantion.position.x -transform.position.x)/pomX *dX /pomX  ;
			
			transform.position = pos;
			transform.Rotate( new Vector3(0,0, 360*Time.deltaTime ));
			//dist = Vector2.Distance(transform.position,Destiantion.position);
		}
 
		//transform.position =Destiantion.position;

		LevelGameFishing.bEnableFishing = true;
		yield return new WaitForFixedUpdate();
		//Debug.Log("Destroy");
		if(transform.tag == "Fish" ) GameObject.Destroy( transform.parent.gameObject);
		else	GameObject.Destroy(gameObject);
		
	}
}
