﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public class MapScript : MonoBehaviour {

	public ScrollRect scrollRect;
	public RectTransform LevelsHolder;
	float timeToMoveBack = 0;
	float timePom = 0;
	bool bMoveBack;
	public ParticleSystem psLevel;
	Vector2 LastLevelAnchoredPosition;

	public Transform NativeAdHolder;
	public Transform NativeAdHolderTopLeft;
	public Transform NativeAdHolderBottomRight;
	public Transform ButtonTopLeft;
	public Transform ButtonBottomRight;
	public Transform CanvasBottomRight;


	bool bHideNativeAds = false;

	public GameObject CharacterHolder;
	public GameObject UnlockLevelPanel;
	public RectTransform CharacterImage;

	public Sprite Lock;
	public Sprite Pin;
	public Sprite Pin2;

	public static int miniGameNo = 0;
	bool bStartScene = true;



	private const float inchToCm = 2.54f;
	private EventSystem eventSystem = null;
	private float dragThresholdCM =  1f;
	

	private void SetDragThreshold()
	{
		if (eventSystem != null)
		{
			eventSystem.pixelDragThreshold = (int)(dragThresholdCM *  Screen.dpi / inchToCm);
		}
	}
	
 
	
	IEnumerator Start () {
		if (eventSystem == null)
		{
			eventSystem = GetComponent<EventSystem>();
		}
		SetDragThreshold();
 
	//	InvokeRepeating("test",5,5);
		//	GameData.Level = 5;
		SetLevelButtons();

		yield return new WaitForSeconds(0.2f);
		LevelTransition.Instance.ShowScene();

		BlockClicks.Instance.SetBlockAll(true); 


		if(bStartScene)
		{
			yield return new WaitForSeconds(1f);
			bStartScene = false;
			if(GameData.LevelToAnimateUnlocking > 0)
			{
				LevelsHolder.GetChild(GameData.LevelToAnimateUnlocking).GetChild(2).GetComponent<Image>().enabled = true; 
				LevelsHolder.GetChild(GameData.LevelToAnimateUnlocking).GetChild(4).GetComponent<Image>().enabled = true;
				LevelsHolder.GetChild(GameData.LevelToAnimateUnlocking).GetComponent<Animator>().SetTrigger("tUnlockLevel");
				GameData.LevelToAnimateUnlocking = -1;
				yield return new WaitForSeconds(2f);
			}
		}

		BlockClicks.Instance.SetBlockAllDelay(2,false); 
	}


	 
	
	// Update is called once per frame
	void Update () {
		timeToMoveBack+=Time.deltaTime;

		if(timeToMoveBack > 10)
		{
			timeToMoveBack =0;
			bMoveBack = true;
			timePom = 0;
		}

		if(bMoveBack)
		{
			timePom  +=Time.deltaTime; 
			LevelsHolder.anchoredPosition = Vector2.Lerp(LevelsHolder.anchoredPosition ,LastLevelAnchoredPosition,timePom);
			if(timePom > 1) 
			{
				bMoveBack = false;
				timeToMoveBack =0;
			}


			if(scrollRect.horizontalNormalizedPosition <0) scrollRect.horizontalNormalizedPosition= 0;
			if(scrollRect.horizontalNormalizedPosition >1) scrollRect.horizontalNormalizedPosition= 1;
			
			if(scrollRect.verticalNormalizedPosition <0) scrollRect.verticalNormalizedPosition= 0;
			if(scrollRect.verticalNormalizedPosition >1) scrollRect.verticalNormalizedPosition= 1;

		}

	}

	void LateUpdate () 
	{

		if(UnlockLevelPanel.activeSelf)
		{
			NativeAdHolder.position = CharacterHolder.transform.position;

		}

	}

	public void SetLevelButtons()
	{
		GameData.GetLevelsData();
		 
		for(int i =1;i<21;i++)
		{
			if(i<=GameData.LastUnlockedLevel ) 
			{ 
				//OTKLJUCANI NIVOI
				if(i <=  (GameData.LastCompletedLevel+1)  &&  i  != GameData.LevelToAnimateUnlocking )
					LevelsHolder.GetChild(i).GetComponent<Button>().enabled = true; 
				else
					LevelsHolder.GetChild(i).GetComponent<Button>().enabled =  true;//false;  

				LevelsHolder.GetChild(i).GetChild(2).GetComponent<Image>().enabled = false;//ne vidi se katanac
				LevelsHolder.GetChild(i).GetChild(4).GetComponent<Image>().sprite = Pin;
				if(i>GameData.LastCompletedLevel+1 )
				{
					LevelsHolder.GetChild(i).GetChild(3).GetComponent<Image>().enabled = false;//ne vidi se dinosaurus
					LevelsHolder.GetChild(i).GetChild(4).GetComponent<Image>().enabled = true; //pin
				}
				else
				{
					if(i ==  (GameData.LastCompletedLevel+1) )
					{
						//child
						 
						CharacterHolder.transform.position = LevelsHolder.GetChild(i).transform.position;
						CharacterImage .anchoredPosition = new Vector2(130,-80);
						UnlockLevelPanel.SetActive(false);
						bHideNativeAds = true;
						LevelsHolder.GetChild(i).GetChild(3).GetComponent<Image>().enabled = false;//ne vidi se dinosaurus
						LevelsHolder.GetChild(i).GetChild(4).GetComponent<Image>().enabled = true;
						LevelsHolder.GetChild(i).GetChild(4).GetComponent<Image>().sprite = Pin2;
					}
					else
					{
						LevelsHolder.GetChild(i).GetChild(3).GetComponent<Image>().enabled = true;//  vidi se dinosaurus
						LevelsHolder.GetChild(i).GetChild(4).GetComponent<Image>().enabled = false;
						if(   GameData.LastUnlockedLevel== 20 &&  i== 20) 
						{
							CharacterHolder.transform.position = LevelsHolder.GetChild(i).transform.position;
							CharacterImage .anchoredPosition = new Vector2(130,-80);
							UnlockLevelPanel.SetActive(false);
							bHideNativeAds = true;
						}

					}
				}


			}

			if(i>GameData.LastUnlockedLevel ) 
			{
				//ZAKLJUCANI NIVOI
				if(i>GameData.LastUnlockedLevel +1)
					LevelsHolder.GetChild(i).GetComponent<Button>().enabled = true;//false; 
				else
				{
					//prvo sledece dugme je dozvoljeno zbog otkljucavanja
					LevelsHolder.GetChild(i).GetComponent<Button>().enabled = true;
					if(GameData.LastUnlockedLevel == GameData.LastCompletedLevel)
					{
						CharacterHolder.transform.position = LevelsHolder.GetChild(i).transform.position;
						CharacterImage .anchoredPosition = new Vector2(130,-80);
						UnlockLevelPanel.SetActive(false);
						bHideNativeAds = true;
					}

				}

				LevelsHolder.GetChild(i).GetChild(2).GetComponent<Image>().enabled = true;//vidi se katanac
				LevelsHolder.GetChild(i).GetChild(3).GetComponent<Image>().enabled = false;//ne vidi se dinosaurus
				LevelsHolder.GetChild(i).GetChild(4).GetComponent<Image>().enabled = false;//ne vidi se pin
			}


		}

		//Canvas.ForceUpdateCanvases();
		LevelsHolder.GetComponent<RectTransform>().anchoredPosition = 
			(Vector2) scrollRect.transform.InverseTransformPoint(LevelsHolder.position) -
				(Vector2) scrollRect.transform.InverseTransformPoint(CharacterHolder.transform.position +new Vector3(0,3.0f,0));// new Vector3(0,1.5f,0));
		
		if(scrollRect.horizontalNormalizedPosition <0) scrollRect.horizontalNormalizedPosition= 0;
		if(scrollRect.horizontalNormalizedPosition >1) scrollRect.horizontalNormalizedPosition= 1;
		
		if(scrollRect.verticalNormalizedPosition <0) scrollRect.verticalNormalizedPosition= 0;
		if(scrollRect.verticalNormalizedPosition >1) scrollRect.verticalNormalizedPosition= 1;
		
		
		LastLevelAnchoredPosition =  LevelsHolder.GetComponent<RectTransform>().anchoredPosition;

		if(!bStartScene)
		{
			if(GameData.LevelToAnimateUnlocking > 0)
			{
				LevelsHolder.GetChild(GameData.LevelToAnimateUnlocking).GetChild(2).GetComponent<Image>().enabled = true; 
				LevelsHolder.GetChild(GameData.LevelToAnimateUnlocking).GetChild(4).GetComponent<Image>().enabled = true;
				LevelsHolder.GetChild(GameData.LevelToAnimateUnlocking).GetComponent<Animator>().SetTrigger("tUnlockLevel");
				GameData.LevelToAnimateUnlocking = -1;
			}
		}
		else
		{
			if(GameData.LevelToAnimateUnlocking>0)
			{
				LevelsHolder.GetChild(GameData.LevelToAnimateUnlocking).GetChild(2).GetComponent<Image>().enabled = true;//vidi se katanac
				LevelsHolder.GetChild(GameData.LevelToAnimateUnlocking).GetChild(3).GetComponent<Image>().enabled = false;//ne vidi se dinosaurus
				LevelsHolder.GetChild(GameData.LevelToAnimateUnlocking).GetChild(4).GetComponent<Image>().enabled = false;//ne vidi se pin
			}
			
		}
	}

	void SetMovePosition()
	{
		LastLevelAnchoredPosition = 
			(Vector2) scrollRect.transform.InverseTransformPoint(LevelsHolder.position) -
				(Vector2) scrollRect.transform.InverseTransformPoint(CharacterHolder.transform.position +  new Vector3(0,3.0f,0));// new Vector3(0,1.5f,0)) 
	}

	bool bMapPointerDown = false;
	public void MapPointerDown()
	{
		bMapPointerDown = true;
		 
	}


	public void MapPointerUp()
	{
		if(bMapPointerDown)
		{
			//Debug.Log("MAP CLICKED");
			bHideNativeAds = true;
			if(UnlockLevelPanel.activeSelf)
			{
				UnlockLevelPanel.SetActive(false);
			}
		}
	}

	public void ButtonLevelClicked(int _level)
	{
		//Debug.Log("LEVEL " + _level.ToString());
		SoundManager.Instance.Play_ButtonClick();
		if(_level <= GameData.LastUnlockedLevel && _level <=  (GameData.LastCompletedLevel+1)  ) //nivo je odigran ili je otkljucan
		{
			CharacterImage.anchoredPosition = new Vector2(130,-80);
			psLevel.transform.position = LevelsHolder.GetChild(_level).transform.position;
			psLevel.Play();
			 LoadLevelGame(_level);
			CharacterHolder.transform.position = LevelsHolder.GetChild(_level).transform.position;
			bHideNativeAds = true;

			SetMovePosition();
			timeToMoveBack =  0;
			bMoveBack = true;
			timePom = 0;
			if(UnlockLevelPanel.activeSelf)
			{
				UnlockLevelPanel.SetActive(false);
			}
		}
		else 	if(_level == (GameData.LastUnlockedLevel+1) && GameData.LastUnlockedLevel == GameData.LastCompletedLevel ) 
		{
			GameData.SelectedLevel = _level;
			// Debug.Log("SELECTED LEVEL " + GameData.SelectedLevel.ToString());
			CharacterHolder.transform.position = LevelsHolder.GetChild(_level).transform.position;
			CharacterImage.anchoredPosition = new Vector2(210,25);
			bHideNativeAds = false;
			UnlockLevelPanel.SetActive(true);

			SetMovePosition();
			timePom = 0;
			timeToMoveBack = 0f;
			bMoveBack = true;

		}
		SoundManager.Instance.Play_Sound( SoundManager.Instance.MoveCharacterMap );

	}


	public void ButtonWatchVideoToUnlockClicked()
	{

		Shop.Instance.WatchVideo();
		SoundManager.Instance.Play_ButtonClick();
		BlockClicks.Instance.SetBlockAll(true);
		BlockClicks.Instance.SetBlockAllDelay(1f,false);
	}

 

	public void FinishWatchingVideo()
	{

		UnlockLevel();
		//Debug.Log("LEVEL " + GameData.LastUnlockedLevel.ToString());
		 
	}


	public void ButtonPlayMiniGameToUnlockLevelClicked()
	{
  
		miniGameNo++;
		if(miniGameNo>1) miniGameNo = 0;

		//Debug.Log("SELECTED LEVEL " + GameData.SelectedLevel.ToString());
		if(miniGameNo == 0)
	 		LevelTransition.Instance.HideSceneAndLoadNext("FeedTheAnimal");
		else
			LevelTransition.Instance.HideSceneAndLoadNext("JumpThePenguinGameScreen");

		SoundManager.Instance.Play_ButtonClick();

		//UnlockLevel();
	}


	public void UnlockLevel()
	{
		BlockClicks.Instance.StopAllCoroutines();
		BlockClicks.Instance.SetBlockAll(true); 
		BlockClicks.Instance.SetBlockAllDelay(3,false); 

		GameData.UnlockNewLevel();
		SetLevelButtons();
	}
 
	public void ScrollRectMove(Vector2 vect)
	{
		timeToMoveBack = 0;
		bMapPointerDown = false;
	}

	void LoadLevelGame(int selectedLevel)
	{
		BlockClicks.Instance.SetBlockAll(true); 
		StartCoroutine(DelayLoadLevelGame(  selectedLevel));
		GameData.SelectedLevel = selectedLevel;
		//Debug.Log(" sel   "+ GameData.SelectedLevel + "  , unlocked   "+ GameData.LastUnlockedLevel + "  , completed "+  GameData.LastCompletedLevel);
		SoundManager.Instance.Play_ButtonClick();

	}

	IEnumerator DelayLoadLevelGame(int selectedLevel)
	{
		bHideNativeAds = true;
		yield return new WaitForSeconds(1);
		int levelGame = selectedLevel%4;
		if(levelGame == 0 ) levelGame = 4;
		LevelTransition.Instance.HideSceneAndLoadNext("LevelGame"+(levelGame).ToString());//(levelGame+1).ToString());
		 //LevelTransition.Instance.HideSceneAndLoadNext("MainGamePuzzle");
	}

	public void ButtonHomeClicked()
	{
		bHideNativeAds = true;
		LevelTransition.Instance.HideSceneAndLoadNext("HomeScene");
		SoundManager.Instance.Play_ButtonClick();
		BlockClicks.Instance.SetBlockAll(true);
	}
}
