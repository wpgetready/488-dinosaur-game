﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.EventSystems;

public class LevelGame_Cave : MonoBehaviour {

	public GameObject FlashLightObject;
	public VerticalLayoutGroup BonesHolder;
	public GameObject HolderFillTop;

	int bonesCollected = 0;
	public Transform[] BoneHolders;
	public Transform[] BonePositions;

	public ParticleSystem psCollected;
	 
	GameObject CollectedBoneObject = null;
	Vector2 TargetPos  = Vector2.zero;

	public CanvasGroup NextButton;
	public Sprite NextButtonSprite;

	public Transform BoneDragHolder;

	void Awake()
	{
		FlashLightObject.SetActive(true);
 
		int offset  = Mathf.FloorToInt((Screen.width/(float)Screen.height  - 1.7777f)* 57);

		BonesHolder.padding.top=offset;

		if(Shop.RemoveAds == 2) 
		{
			HolderFillTop.SetActive(false);
		}


		Transform posBonePom;
		int a =0;
		int b =0;
		for(int i =0; i<BonePositions.Length*2; i++)
		{
			a = Random.Range(0,BonePositions.Length);
			posBonePom = BonePositions[a];
			b = Random.Range(0,BonePositions.Length);
			BonePositions[a] = BonePositions[b];
			BonePositions[b] = posBonePom;
		}
		CreateBones();

 
		 
	}

	void Start()
	{
		LevelTransition.Instance.ShowScene();
	}


	void CreateBones()
	{
		for(int i=1;i<=6;i++)
		{
			GameObject b = GameObject.Instantiate(  Resources.Load<GameObject>(  "Dinos/"+ GameData.SelectedLevel.ToString()+"/Bone"+i.ToString()));
			b.transform.SetParent(BonePositions[i-1]);

			Vector2 size = b.GetComponent<RectTransform>().sizeDelta;
			float scale = 140f/size.x;
			float scale2 = 100/size.y;
			if(scale2<scale) scale = scale2;
			
			b.transform.localScale = scale*	Vector3.one;
			//b.transform.localScale = 0.6f*Vector3.one;
			b.transform.localPosition = Vector3.zero;
			Destroy( b.GetComponent<Mask>() );

			CircleCollider2D cc = (CircleCollider2D) b.AddComponent<CircleCollider2D>();
			cc.radius = 60/scale;
		}

		FlashLight.bEnableDrag = true;
	}

	 

	 

	void Update () {
	
 
		if(    Input.GetMouseButtonDown(0)  && FlashLight.bEnableDrag )
		{
			if(MenuManager.activeMenu != "") return;
			PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
			eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
			List<RaycastResult> results = new List<RaycastResult>();
			EventSystem.current. RaycastAll(eventDataCurrentPosition, results);
			if(results.Count  >= 2) 
			{
				bool bFL = false;
				foreach(RaycastResult r in results) if(r.gameObject.name == "FlashLightParent") bFL = true;  //da je kliknuto u zoni lampe

				if( bFL && ( results[0].gameObject.tag == "Bone" || results[1].gameObject.tag == "Bone"))
				{
					 if(results[0].gameObject.tag == "Bone")
						CollectedBoneObject  = results[0].gameObject;
						 
					else 
						CollectedBoneObject  = results[1].gameObject;

					psCollected.transform.position = CollectedBoneObject.transform.position;
				 
					psCollected.Play();
	 				//ANIMATE BONE
					bonesCollected++;
					FlashLight.bEnableDrag = false;
					StartCoroutine("MoveBone");

					GameObject parentOld = CollectedBoneObject.transform.parent.gameObject;
					CollectedBoneObject.transform.SetParent(BoneDragHolder);
				}

			}

		}

	}

	IEnumerator MoveBone()
	{
		if( SoundManager.Instance!=null ) SoundManager.Instance.Play_Sound( SoundManager.Instance.ItemFoundCave );
		FlashLight.bEnableDrag = false;
		yield return new WaitForSeconds(.5f);
		if( SoundManager.Instance!=null ) SoundManager.Instance.Play_Sound( SoundManager.Instance.MoveBoneLeft );
		CollectedBoneObject.transform.SetParent(BoneHolders[bonesCollected-1]);

		Vector3 StartPosition = CollectedBoneObject.transform.position;

		CollectedBoneObject.transform.GetComponent<CircleCollider2D>().enabled = false;
		CollectedBoneObject.transform.GetComponent<PolygonCollider2D>().enabled = false;


		yield return new WaitForEndOfFrame();
		float pom =0;
		while(pom<1f)
		{
			pom+=Time.deltaTime*3f;
			CollectedBoneObject.transform.position = Vector3.Lerp(StartPosition, BoneHolders[bonesCollected-1].position, pom);
			yield return new WaitForEndOfFrame();
		}

		CollectedBoneObject.transform.position = BoneHolders[bonesCollected-1].position;
		CollectedBoneObject.transform.SetParent(BoneHolders[bonesCollected-1]);
		CollectedBoneObject = null;
 
		FlashLight.bEnableDrag = true;
		 

		if(bonesCollected == 6)
		{
			//Debug.Log ("KRAJ");
			NextButton.alpha = 1;
			NextButton.GetComponent<Button>().enabled = true;
			NextButton.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = NextButtonSprite;
			NextButton.transform.parent.GetComponent<Animator>().SetTrigger("tPulse");
			if( SoundManager.Instance!=null ) SoundManager.Instance.Play_Sound( SoundManager.Instance.LevelFinished );
		}
	}

	public void ButtonBackClicked()
	{
		LevelTransition.Instance.HideSceneAndLoadNext("Map");
		SoundManager.Instance.Play_ButtonClick();
		BlockClicks.Instance.SetBlockAll(true);
		GameData.IncrementBackButtonClickedCount();
	}

	public void ButtonNextClicked()
	{
		LevelTransition.Instance.HideSceneAndLoadNext("MainGamePuzzle");
		SoundManager.Instance.Play_ButtonClick();
		BlockClicks.Instance.SetBlockAll(true);
		GameData.IncrementForwardButtonClickedCount();
	}
}
