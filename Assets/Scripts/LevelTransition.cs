﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
 

public class LevelTransition : MonoBehaviour {

 


	public Animator anim;
 
	public static LevelTransition Instance;
	static string nextLevelName = "";
	bool bLoadScene = false;  

	void Start () 
	{
		DontDestroyOnLoad(this.gameObject);
		 
		anim = GameObject.Find("TransitionImage").GetComponent<Animator>();
		anim.gameObject.SetActive(false);
	}
 
	void Awake()
	{
		if(nextLevelName != "" &&  Application.loadedLevelName == "TransitionScene") 
		{
//			Debug.Log("Transition SCENE");
			Application.LoadLevel(nextLevelName);
			 
			return;
		}
		else
		{
			if(Instance !=null && Instance != this ) 
			{
				GameObject.Destroy(gameObject);
				 
			}
			else Instance = this;
			 
		}
	}

 	public void OnLevelWasLoaded(int level)
	{
		if(Application.loadedLevelName != "TransitionScene" &&
		   (!MiniGameManager.opened &&   Application.loadedLevelName != "JumpThePenguinGameScreen" && Application.loadedLevelName != "FeedTheAnimal"))
		    
		{
			anim = GameObject.Find("TransitionImage").GetComponent<Animator>();
			anim.Play("DefaultClosed"); 
		}
		
	}
	
	IEnumerator  WaitAndDestroy()
	{
		yield return new WaitForEndOfFrame();
		if(Instance.anim == null) Instance.anim = GameObject.Find("TransitionImage").GetComponent<Animator>();
		Instance.anim.gameObject.SetActive(false);
		GameObject.Destroy(gameObject);
	}



	public void HideSceneAndLoadNext(string levelName)
	{
		GlobalVariables.ClearAllPauseEvents();

		if(bLoadScene) return;
		bLoadScene = true;
		nextLevelName = levelName;
		StopAllCoroutines();
		//StartCoroutine(SetBlockAll(0,true));
		BlockClicks.Instance.SetBlockAll(true);
		anim.gameObject.SetActive(true);
		StartCoroutine("LoadScene" , levelName);

		// if(anim == null) anim = GameObject.Find("TransitionImage").GetComponent<Animator>();
		if(anim != null) 
		{ 
			anim.SetBool("bClose",true);
			SoundManager.Instance.Play_Sound( SoundManager.Instance.OpenMap );
		}
	 
			
	}

	public void ShowScene()
	{
		if(anim == null) anim = GameObject.Find("TransitionImage").GetComponent<Animator>();
		if(anim!=null) 
		{
			anim.SetBool("bClose",false);
			if(!MenuManager.bFirstLoadMainScene) SoundManager.Instance.Play_Sound( SoundManager.Instance.OpenMiniGameMap );
		}
 
		BlockClicks.Instance.SetBlockAll(true);
		BlockClicks.Instance.SetBlockAllDelay(1f,false);
		
	}

	 
	IEnumerator LoadScene (string levelName)
	{
//		try {
//			if (Shop.RemoveAds !=2 && DataManager.Instance.Tutorial >= 4 && Application.loadedLevelName != "Gameplay4" ) WebelinxCMS.Instance.ShowInterstitial(7);
//		}
//		catch{ Debug.Log("Show Interstitial Error"); } 
		yield return new WaitForSeconds(1.2f);
		bLoadScene = false;
		//Gameplay.Instance = null;
	//	HomeScene.Instance = null;
 
		Application.LoadLevel("TransitionScene");
		
	}

	public void HideAndShowSceneWithoutLoading( )
	{
		StopAllCoroutines();
		//StartCoroutine(SetBlockAll(0,true));
		BlockClicks.Instance.SetBlockAll(true);
		 
		anim.gameObject.SetActive(true);
		anim.SetBool("bClose",true);
		StartCoroutine("WaitHideAndShowScene");

	}

	IEnumerator WaitHideAndShowScene ( )
	{
		yield return new WaitForSeconds(2.0f);
		 
		anim.SetBool("bClose",false);
		//StartCoroutine(SetBlockAll(1,false));
		BlockClicks.Instance.SetBlockAll(true);
		BlockClicks.Instance.SetBlockAllDelay(1f,false);
	}

	public void AnimEventHideSceneAnimStarted()
	{

	}

	public void AnimEventShowSceneAnimFinished()
	{
		
		anim.gameObject.SetActive(false);
	}
	
}

