﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LeveGame_CleanBones : MonoBehaviour {

	public VerticalLayoutGroup CleanBonesHolder;
	public GameObject Holder1FillTop;
	public VerticalLayoutGroup DirtyBonesHolder;
	public GameObject Holder2FillTop;

	public Transform CleaningPosition;
	public Transform[] BoneEndHolders;
	public Transform[] BoneStartPositions;

	public CleaningTool[] AllCleaningTools;

	public GameObject DirtPrefab;

	public static int gamePhase = 0;
	int cleanedBones;

	public CanvasGroup NextButton;
	public Sprite NextButtonSprite;

	public Transform NativeAdHolderPosition;
	public Transform NativeAdHolder;

	public ParticleSystem psCollected;

	void Awake()
	{
		Input.multiTouchEnabled = false;
		gamePhase = 0;
		int offset  = Mathf.FloorToInt((Screen.width/(float)Screen.height  - 1.7777f)* 57);
		CleanBonesHolder.padding.top=offset;
		DirtyBonesHolder.padding.top=offset;
 		if(Shop.RemoveAds == 2) 
		{
			Holder1FillTop.SetActive(false);
			Holder2FillTop.SetActive(false);
		}
 
		Transform posBonePom;
		int a =0;
		int b =0;
		for(int i =0; i<BoneStartPositions.Length*2; i++)
		{
			a = Random.Range(0,BoneStartPositions.Length);
			posBonePom = BoneStartPositions[a];
			b = Random.Range(0,BoneStartPositions.Length);
			BoneStartPositions[a] = BoneStartPositions[b];
			BoneStartPositions[b] = posBonePom;
		}

		//scale native ads


		CreateBones();

	}

	void Start() {
		NativeAdHolder.localScale = (.8f +(1.3333f - Screen.width/(float)Screen.height)*.236f)*Vector3.one;
		NativeAdHolder.position = NativeAdHolderPosition.position;

		DragToLocation.bEnableDrag = true;
		CleaningTool.OneToolEnabledNo = 0;
 
		LevelTransition.Instance.ShowScene();
	}

	void CreateBones()
	{
		for(int i=1;i<=6;i++)
		{
			//Debug.Log( GameData.SelectedLevel.ToString() );
			GameObject b = GameObject.Instantiate(  Resources.Load<GameObject>( "Dinos/" + GameData.SelectedLevel.ToString() +"/Bone"+i.ToString()));
 
			Vector3 tmp =b.transform.localScale;
			b.transform.SetParent(BoneStartPositions[i-1]);


			Vector2 size = b.GetComponent<RectTransform>().sizeDelta;
			float scale = 140f/size.x;
			float scale2 = 100/size.y;
			if(scale2<scale) scale = scale2;
			
			b.transform.localScale = scale*	Vector3.one;

			//b.transform.localScale = 0.75f*tmp;


			b.transform.localPosition = Vector3.zero;
			DragToLocation dtl = b.AddComponent<DragToLocation>();
			dtl.StartScale = scale*	Vector3.one;
			b.GetComponent<Mask>().enabled = true;

			GameObject dirt = GameObject.Instantiate(  DirtPrefab);
			dirt.transform.SetParent(b.transform);
			dirt.transform.localPosition = Vector3.zero;
			dirt.transform.localScale = Vector3.one;
			 
			CircleCollider2D cc = (CircleCollider2D) b.AddComponent<CircleCollider2D>();
			cc.radius = 60/scale;
		}
	}



	public void NextPhase()
	{
		gamePhase ++;
		//Debug.Log("GAME PHASE: " + gamePhase);
		if(gamePhase%3 == 0) DragToLocation.bEnableDrag = true;
		if(gamePhase%3 == 1) 
		{
			CleaningTool.OneToolEnabledNo = 1;// bEnableDrag = true;
			if( Tutorial.Instance!=null) 	 Tutorial.Instance.StartTutorial();
		}
		if(gamePhase%3 == 2) 
		{
			CleaningTool.OneToolEnabledNo = 0;  
			cleanedBones++;
			StartCoroutine("MoveCleanedBone");

			if( Tutorial.Instance!=null)
			{
				GameObject.Destroy(Tutorial.Instance.gameObject);
			}
		}

		if(cleanedBones == 6) 
		{
			//Debug.Log ("KRAJ");
			NextButton.alpha = 1;
			NextButton.GetComponent<Button>().enabled = true;
			NextButton.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = NextButtonSprite;
			NextButton.transform.parent.GetComponent<Animator>().SetTrigger("tPulse");
			if( SoundManager.Instance!=null ) SoundManager.Instance.Play_Sound( SoundManager.Instance.LevelFinished );
		}

	}


	IEnumerator MoveCleanedBone()
	{
		psCollected.transform.position = CleaningPosition.position;
		psCollected.Play();
		if( SoundManager.Instance!=null ) SoundManager.Instance.Play_Sound( SoundManager.Instance.MoveBoneLeft );
		yield return new WaitForSeconds(1);

		DragToLocation dtl  = CleaningPosition.GetChild(0).GetComponent<DragToLocation>();
		dtl.transform.SetParent(BoneEndHolders[cleanedBones-1]);
		float pom = 0;
		while(pom<1 )
		{ 
			pom+=Time.deltaTime;
			dtl.transform.localPosition = Vector3.Lerp(dtl.transform.localPosition, Vector3.zero, pom);
			dtl.transform.localScale = Vector3.Lerp(dtl.transform.localScale, dtl.StartScale,pom ); 
			yield return new WaitForEndOfFrame( );
		}
		dtl.gameObject.layer =   LayerMask.NameToLayer("Item");
		dtl.transform.GetComponent<PolygonCollider2D>().enabled = false;
		dtl.enabled = false;
		GameObject.Destroy( dtl.transform.GetChild(0).gameObject);
		 
		NextPhase(); //move to clean holder
		for (int i = 0; i < AllCleaningTools.Length; i++) 
		{
			AllCleaningTools[i].ResetTool();
		}
		 
	}

	

	public void ButtonBackClicked()
	{
		LevelTransition.Instance.HideSceneAndLoadNext("Map");
		SoundManager.Instance.Play_ButtonClick();
		BlockClicks.Instance.SetBlockAll(true);
		GameData.IncrementBackButtonClickedCount();
	}

	public void ButtonNextClicked()
	{
		LevelTransition.Instance.HideSceneAndLoadNext("MainGamePuzzle");
		SoundManager.Instance.Play_ButtonClick();
		BlockClicks.Instance.SetBlockAll(true);
		GameData.IncrementForwardButtonClickedCount();
	}
}
