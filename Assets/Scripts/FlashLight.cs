﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class FlashLight : MonoBehaviour  , IBeginDragHandler, IDragHandler, IEndDragHandler
{
	public static bool bEnableDrag = true;
	float x;
	float y;
	Vector3 diffPos = new Vector3(0,0,0);
	bool bDrag = false;

	public void OnBeginDrag (PointerEventData eventData)
	{
		if(!bEnableDrag) return;
		if(!bDrag )
		{
			bDrag = true;
			diffPos =transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition)   ;
			diffPos = new Vector3(diffPos.x,diffPos.y,0);
			//InvokeRepeating("TestDistance",0, .2f);
			//			Tutorial.Instance.HidePointer();
			//			Tutorial.bPause = true;
			 
		}
	}
	
	void Update()
	{
		if( bDrag  && bEnableDrag)  
		{
			
			x = Input.mousePosition.x;
			y = Input.mousePosition.y;
			
			transform.position =     Vector3.Lerp (transform.position, Camera.main.ScreenToWorldPoint(new Vector3(x ,y,10.0f) )  ,10* Time.deltaTime)  ;
			
			
		}
	}
	
	
	public void OnEndDrag (PointerEventData eventData)
	{
		if(bDrag  )
		{
			bDrag = false;
			//CancelInvoke("TestDistance");
			 
		}
	}
	
	
	public void OnDrag (PointerEventData eventData)
	{
		
	}
}
