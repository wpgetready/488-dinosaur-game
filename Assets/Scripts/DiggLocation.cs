﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class DiggLocation : MonoBehaviour// , IPointerClickHandler
{
	public int phase = 0;
	LevelGame_Digger game;
	Image crackMask;
	Image crack;
	bool bDigg = false;
 
 
	public void StartDigging()
	{
	 

		phase++;
		if(game == null) game = Camera.main.GetComponent<LevelGame_Digger>();
		if(crackMask == null) crackMask = transform.GetChild(0).GetComponent<Image>();
		if(crack == null) crack = transform.GetComponent<Image>();
		
		if(phase<=4)
		{
			crackMask.sprite = game.CrackMasks[phase-1];
			crack.sprite =  game.Cracks[phase-1];			
		}
		else 
		{
			game.FoundBone(transform.GetChild(0).GetChild(0).gameObject);		 
			this.enabled = false;
		}
		 
	}





	/*
	public void OnPointerClick(PointerEventData eventData)
	{
		if(!bDigg)
		{
			bDigg = true;
			StartCoroutine("Digg");
		}
	}


	IEnumerator Digg()
	{
		LevelGame_Digger.timeTutorial = 0;//-1;
		phase++;
		if(game == null) game = Camera.main.GetComponent<LevelGame_Digger>();
		if(crackMask == null) crackMask = transform.GetChild(0).GetComponent<Image>();
		if(crack == null) crack = transform.GetComponent<Image>();

		if(phase<=4)
		{
			StartCoroutine (game.DiggForBone(transform.GetChild(0).GetChild(0).gameObject));
			yield return new WaitForSeconds(0.5f);
			crackMask.sprite = game.CrackMasks[phase-1];
			crack.sprite =  game.Cracks[phase-1];

		}
		else 
		{

			StartCoroutine (game.DiggForBone(transform.GetChild(0).GetChild(0).gameObject));
			yield return new WaitForSeconds(0.5f);
			game.FoundBone(transform.GetChild(0).GetChild(0).gameObject);
			//Camera.main.SendMessage("FoundBone", transform.GetChild(0).gameObject, SendMessageOptions.DontRequireReceiver);
			//crack.sprite =  game.Cracks[2];
			this.enabled = false;
		}
		yield return new WaitForSeconds(.1f);//(0.7f);
		bDigg = false;
	}
*/
	 
}
