﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;


public class MainGamePuzzle : MonoBehaviour {

	public VerticalLayoutGroup BonesHolder;
	public GameObject HolderFillTop;
	
	int bonesPositioned = 0;
	public Transform[] BoneHolders;
	public Transform BonesLayout;
	public Transform BonesLayout2;

	public Image [] DinoImages;

	public ParticleSystem psCollected;
	
	GameObject CollectedBoneObject = null;
	Vector2 TargetPos  = Vector2.zero;
	public Animator animFinish;

	public CanvasGroup NextButton;
	public Sprite NextButtonSprite;

	public RectTransform canv;
	public Tutorial tutorial;

	public List<DragBonePuzzle> dbpList = new List<DragBonePuzzle>(); 

	void Awake()
	{

		int offset  = Mathf.FloorToInt((Screen.width/(float)Screen.height  - 1.7777f)* 57);
		BonesHolder.padding.top=offset;
		if(Shop.RemoveAds == 2) 
		{
			HolderFillTop.SetActive(false);
		}

		// CreateBones();

		Sprite dino = Resources.Load<GameObject>(  "Dinos/"+ GameData.SelectedLevel.ToString() +"/Dino").GetComponent<Image>().sprite;
		foreach(Image img in DinoImages)
		{
			img.sprite = dino;
		}

		Transform posBonePom;
		int a =0;
		int b =0;
		for(int i =0; i<BoneHolders.Length*2; i++)
		{
			a = Random.Range(0,BoneHolders.Length);
			posBonePom = BoneHolders[a];
			b = Random.Range(0,BoneHolders.Length);
			BoneHolders[a] = BoneHolders[b];
			BoneHolders[b] = posBonePom;
		}
	}

	IEnumerator Start()
	{
 		yield return new WaitForSeconds(0.1f);
 		CreateBones();
		yield return new WaitForSeconds(0.3f);
		LevelTransition.Instance.ShowScene();
	}
	
	void CreateBones()
	{
		for(int i=1;i<=6;i++)
		{
			//ucitavanje kosti za podlogu
			GameObject bPom = GameObject.Instantiate(  Resources.Load<GameObject>(  "Dinos/" + GameData.SelectedLevel.ToString() +"/Bone"+i.ToString()));
			bPom.transform.SetParent(BonesLayout2);
			bPom.transform.position =(bPom.transform.position ) * canv.localScale.x  + BonesLayout2.transform.position ;
			bPom.transform.localScale = Vector3.one;
			bPom.transform.localRotation = Quaternion.identity;
			bPom.GetComponent<Image>().color  = new Color( .5f, .1f, .05f, .35f);

			//uvitavanje kosti za igru
			GameObject b = GameObject.Instantiate(  Resources.Load<GameObject>(  "Dinos/"+ GameData.SelectedLevel.ToString() +"/Bone"+i.ToString()));
			 
			b.transform.SetParent(BonesLayout);
			b.transform.localScale = Vector3.one;
			 
	 		DragBonePuzzle dbp = b.AddComponent<DragBonePuzzle>();
			StartCoroutine(DlyInit(b,dbp, i));
			b.transform.name = "Bone"+i.ToString();
 
			dbpList.Add(dbp); 
		}
	}

	IEnumerator DlyInit( GameObject b, DragBonePuzzle dbp, int i)
	{

		yield return new WaitForEndOfFrame();
		yield return new WaitForEndOfFrame();
// 		Debug.Log(b.transform.position);
		 dbp.StartPosition = b.transform.position;
//		Debug.Log(b.GetComponent<RectTransform>().anchoredPosition);//.position);
//		Debug.Log(b.transform.position);

		///dbp.recTr = b.GetComponent<RectTransform>();
		//dbp.EndPosition = dbp.recTr.anchoredPosition ;
		dbp.EndPosition = (b.transform.position ) * canv.localScale.x  + BonesLayout.transform.position ;
		b.transform.position = BoneHolders[i-1].position;
		yield return new WaitForEndOfFrame();
		dbp.StartPosition = b.transform.position;

		Vector2 size = b.GetComponent<RectTransform>().sizeDelta;
		float scale = 120f/size.x;
		float scale2 = 100/size.y;
		if(scale2<scale) scale = scale2;

		b.transform.localScale = scale*	Vector3.one;

		//b.transform.localScale = 0.6f*	Vector3.one;


	

		dbp.StartScale = b.transform.localScale;
		dbp.startRotation = b.transform.localRotation;

		CircleCollider2D cc = (CircleCollider2D) b.AddComponent<CircleCollider2D>();
		cc.radius = 60/scale;
		Destroy( b.GetComponent<Mask>() );

	}

	public void BonePositioned(  DragBonePuzzle dbp)
	{
		Vector3 bonePos = dbp.transform.position;
		dbpList.Remove(dbp);

		psCollected.transform.position  = bonePos;
		psCollected.Play();

		bonesPositioned++;
		if(bonesPositioned == 6)
		{
			tutorial.StopTutorial();
			//Debug.Log("KRAJ");
			StartCoroutine("ShowEnd");
		}
	}

	IEnumerator ShowEnd()
	{
		if( SoundManager.Instance!=null ) 
		{
			SoundManager.Instance.Play_Sound( SoundManager.Instance.DinoCompleted );
			SoundManager.Instance.FadeOutAndIn(SoundManager.Instance.MenuMusic,SoundManager.Instance.DinoCompleted.clip.length);
		}
		//Debug.Log(" sel   "+ GameData.SelectedLevel + "  , unlocked   "+ GameData.LastUnlockedLevel + "  , completed "+  GameData.LastCompletedLevel);
		if((GameData.LastCompletedLevel+1)  == GameData.SelectedLevel) GameData.SetCompletedLevelToPP();
		yield return new WaitForSeconds(1);
		animFinish.SetTrigger("tShow");

		yield return new WaitForSeconds(2);
		NextButton.alpha = 1;
		NextButton.GetComponent<Button>().enabled = true;
		NextButton.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = NextButtonSprite;
		NextButton.transform.parent.GetComponent<Animator>().SetTrigger("tPulse");
	}

	public void ButtonBackClicked()
	{
		LevelTransition.Instance.HideSceneAndLoadNext("Map");
		SoundManager.Instance.Play_ButtonClick();
		BlockClicks.Instance.SetBlockAll(true);
		GameData.IncrementBackButtonClickedCount();
	}

	public void ButtonNextClicked()
	{
		LevelTransition.Instance.HideSceneAndLoadNext("Map");
		SoundManager.Instance.Play_ButtonClick();
		BlockClicks.Instance.SetBlockAll(true);
		GameData.IncrementForwardButtonClickedCount();
	}
}
