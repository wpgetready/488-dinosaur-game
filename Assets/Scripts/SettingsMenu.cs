﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour {

	public Image SoundOn;
	public Image SoundOff;

	public Image SettingsOpen;
	public Image SettingsClose;
	public Animator animSettings;

	public void ButtonHomeClicked()
	{
		LevelTransition.Instance.HideSceneAndLoadNext("Map");
		SoundManager.Instance.Play_ButtonClick();
		BlockClicks.Instance.SetBlockAll(true);
		//BlockClicks.Instance.SetBlockAllDelay(.3f,false);
	}

	public void ButtonSettingsClicked()
	{
		SettingsOpen.enabled = !SettingsOpen.enabled;
		SettingsClose.enabled = !SettingsOpen.enabled;	
		animSettings.SetBool("bOpen",SettingsClose.enabled);
		BlockClicks.Instance.SetBlockAll(true);
		BlockClicks.Instance.SetBlockAllDelay(.3f,false);
		SoundManager.Instance.Play_ButtonClick();
	}

	public void ButtonSoundClicked()
	{
		if(SoundManager.soundOn == 1)
		{
			SoundOff.enabled = true;
			SoundOn.enabled = false;
			SoundManager.soundOn = 0;
			//Debug.Log ("BRISI SOUND");
			//return;
			SoundManager.Instance.MuteAllSounds();
		}
		else
		{
			SoundOff.enabled = false;
			SoundOn.enabled = true;
			SoundManager.soundOn = 1;
			//Debug.Log ("BRISI SOUND");
			//return;
			SoundManager.Instance.UnmuteAllSounds();
			SoundManager.Instance.Play_ButtonClick();
		}

		if(SoundManager.musicOn == 1)
		{
			SoundManager.Instance.Stop_Music();
			SoundManager.musicOn = 0;
		}
		else
		{
			SoundManager.musicOn = 1;
			SoundManager.Instance.Play_Music();
		}
		
		PlayerPrefs.SetInt("SoundOn",SoundManager.soundOn);
		PlayerPrefs.SetInt("MusicOn",SoundManager.musicOn);
		PlayerPrefs.Save();

		BlockClicks.Instance.SetBlockAll(true);
		BlockClicks.Instance.SetBlockAllDelay(.3f,false);

	}
}
