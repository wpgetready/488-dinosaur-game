﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Tutorial : MonoBehaviour {
	public static bool bTutorial = false;
	public static bool bPause = false;
	public static Tutorial instance;

	MainGamePuzzle mgPuzzle;
 
	Animator animTutorial;
	 

	public static float timeTutorial = 4;
	public static float ShowHelpPeriod = 6;
	bool bHidden = true;
 
	public static Tutorial Instance;
	public static Transform copyPositionTransform;

	Color visibleCol = new Color(1,1,1,.5f);
	Color hiddenCol = new Color(1,1,1,0);

	void Start () {
		
		Instance = this;
		bPause = false;
		animTutorial = transform.GetComponent<Animator>();
		 
		StartTutorial();
	}

	void Update()
	{
		 
	}

	public void StartTutorial()
	{
		timeTutorial = 4;
		instance = this;
		bTutorial = true;
		gameObject.SetActive(true);

		if(Application.loadedLevelName == "LevelGame3" )
		{
			animTutorial.transform.GetChild(0).GetComponent<Image>().enabled = false;
		}
		InvokeRepeating("TestTutorial",1,1);
	}


	public void StopTutorial()
	{
		bTutorial = false;
		 
		CancelInvoke();
		StopAllCoroutines();
		gameObject.SetActive(false);
		//GameObject.Destroy(this.gameObject);
	}


	public void HideTutorial()
	{
		animTutorial.ResetTrigger("moveStart");
		if(!bHidden)
			animTutorial.SetTrigger("Hide");
		else
			animTutorial.Play("hiddenDef");

		timeTutorial = 0;
		bHidden = true;
		StopCoroutine( "StartPointingAndHide");
		if(Application.loadedLevelName == "MainGamePuzzle")
		{
			Image boneImg = animTutorial.transform.GetChild(0).GetComponent<Image>();
			boneImg.enabled = false;
		}
	}
	 
	void TestTutorial()
	{
		 
		if(timeTutorial <0) CancelInvoke("TestTutorial");
		if(bTutorial )
			timeTutorial ++;
		else return;
		
		if(timeTutorial >= ShowHelpPeriod)
		{

			timeTutorial = 0;
			StartCoroutine("StartPointingAndHide");

		}
		 
	}

	IEnumerator  StartPointingAndHide(  )
	{
		 
		bHidden = false;
		animTutorial.ResetTrigger("Hide");
		animTutorial.SetTrigger("moveStart");

		if(Application.loadedLevelName == "MainGamePuzzle")
		{

			if(mgPuzzle == null) mgPuzzle = Camera.main.GetComponent<MainGamePuzzle>();

			int i = Random.Range(0,mgPuzzle.dbpList.Count);


			GameObject b = mgPuzzle.dbpList[i].gameObject;//GameObject.Find("CanvasBG/DinoBonesLayout/Bone"+i.ToString());
			// if(b!=null )
			//{
				DragBonePuzzle dbp = mgPuzzle.dbpList[i];//b.GetComponent<DragBonePuzzle>();

				animTutorial.transform.position =dbp.StartPosition;
				Image boneImg = animTutorial.transform.GetChild(0).GetComponent<Image>();
				boneImg.enabled = true;

				boneImg.sprite = dbp.GetComponent<Image>().sprite;
				boneImg.rectTransform.sizeDelta =  dbp.GetComponent<RectTransform>().sizeDelta;

				 yield return new WaitForSeconds(1.5f);
				float timeMove = 0;
				while( timeMove<1)
				{
					timeMove+=  Time.deltaTime;
					animTutorial.transform.position = Vector3.Lerp ( dbp.StartPosition, dbp.EndPosition, timeMove);

					yield return new WaitForEndOfFrame();
				}
				yield return new WaitForSeconds(1.5f);
				boneImg.enabled = false;
			//}
		}


		if(Application.loadedLevelName == "LevelGame3") //CISCENJE KOSTIJU
		{
			if(LeveGame_CleanBones.gamePhase == 0) //prevlacenje kosti 
			{
				int i = Random.Range(1,6);
				Transform b = GameObject.Find("DirtyBonesHolder/Panel/Holder"+i.ToString()).transform.GetChild(0);
				if(b!=null )
				{
				 
					animTutorial.transform.position =b.position;
					Image boneImg = animTutorial.transform.GetChild(0).GetComponent<Image>();
					boneImg.enabled = true;
					
					boneImg.sprite = b.GetComponent<Image>().sprite;
					boneImg.rectTransform.sizeDelta =  b.GetComponent<RectTransform>().sizeDelta;
					boneImg.rectTransform.localScale =  b.localScale*1.5f;
					boneImg.color = new Color(.25f, 0.07f,0.07f,.5f);
					yield return new WaitForSeconds(1.5f);
					float timeMove = 0;
					while( timeMove<1)
					{
						timeMove+=  Time.deltaTime;
						animTutorial.transform.position = Vector3.Lerp ( b.position, Vector3.zero, timeMove);
						
						yield return new WaitForEndOfFrame();
					}
					yield return new WaitForSeconds(1.5f);
					boneImg.enabled = false;
				}
			}

			if(LeveGame_CleanBones.gamePhase == 1) //ciscenje sundjerom
			{

				Transform b = GameObject.Find("CleaningTools/Tool1Holder/Sponge/SpongeAnim").transform;
				if(b!=null )
				{
					
					animTutorial.transform.position =b.position;
					Image spongeImage = animTutorial.transform.GetChild(0).GetComponent<Image>();
					spongeImage.enabled = true;
					
					spongeImage.sprite = b.GetComponent<Image>().sprite;
					spongeImage.rectTransform.sizeDelta =  b.GetComponent<RectTransform>().sizeDelta;
					spongeImage.rectTransform.localScale =  b.localScale*1.2f;
					spongeImage.color = new Color(1f, 1f,1f,.5f);
					yield return new WaitForSeconds(1.5f);
					float timeMove = 0;
					while( timeMove<1)
					{
						timeMove+=  Time.deltaTime;
						animTutorial.transform.position = Vector3.Lerp ( b.position, Vector3.zero, timeMove);
						
						yield return new WaitForEndOfFrame();
					}
					yield return new WaitForSeconds(1.5f);
					spongeImage.enabled = false;
				}
			}

		}


		animTutorial.ResetTrigger("moveStart");
		animTutorial.SetTrigger("Hide");
		timeTutorial = 0;
		yield return new WaitForEndOfFrame();
		bHidden = true;
	}
 
	 
}
