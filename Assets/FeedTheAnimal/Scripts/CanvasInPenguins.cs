using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CanvasInPenguins : MonoBehaviour {

	static CanvasInPenguins instance;
	public static CanvasInPenguins Instance
	{
		get
		{
			if(instance == null)
				instance = GameObject.FindObjectOfType(typeof(CanvasInPenguins)) as CanvasInPenguins;
			
			return instance;
		}
	}
	enum orientation {
		
		Portrait = 0,
		Landscape = 1
	}
	int currentOrientation;
	Text scoreText;
	Text hiscoreText;
	Text coinsText;
	string prefsName = "";
	bool paused = false;
	bool canClick = true;

	 MenuManager menuManager;

	void Awake()
	{
		scoreText = GameObject.Find("Score").GetComponent<Text>();
		hiscoreText = GameObject.Find("Highscore").GetComponent<Text>();
		coinsText = GameObject.Find("Coins").GetComponent<Text>();

		coinsText.text = GameData.ReturnRequiredScoreToUnlockLevel().ToString();
	}

	void Start () 
	{
		menuManager = transform.GetComponent<MenuManager>();
		currentOrientation = (Camera.main.aspect < 1.0f) ? (int)orientation.Portrait : (int)orientation.Landscape;
		MiniGameManager.Instance.StartMeasureTime();
	}

	void Update ()
	{
		if(Input.GetKeyUp(KeyCode.Escape) && MiniGameManager.Instance.canPlay)
		{
			if(!paused)
			{
				if(canClick)
				{
					if(MiniGameManager.Instance.canPlay)
						PauseGame(0);
				}
			}
			else
			{
				if(canClick)
				{
					ResumeGame(0);
				}
			}
		}
	}

	void UnpauseWithDelay()
	{
		paused = false;
	}

	void ResetCanClick()
	{
		canClick = true;
	}

	public void PauseGame (int popUpMenuIndex) {
		//TODO
		//SoundManager.Instance.Play_ButtonClick4();
		if(ButtonsScript.Instance != null)
			ButtonsScript.Instance.canPlay = false;
		//canPlay = false;
		//gamePaused = true;
		//owl.GetComponent<Rigidbody2D> ().isKinematic = true;
		
		//int numOfPopUps = GameObject.Find ("Canvas").GetComponent<MenuManager_FTA> ().disabledObjects.Length / 2;
		//GameObject.Find ("Canvas").GetComponent<MenuManager_FTA> ().ShowPopUpMenu (GameObject.Find ("Canvas").GetComponent<MenuManager_FTA> ().disabledObjects [3 * currentOrientation + popUpMenuIndex]); 

		//marko
		//GameObject.Find ("Canvas").GetComponent<MenuManager_FTA> ().ShowPopUpMenu (GameObject.Find ("Canvas").GetComponent<MenuManager_FTA> ().disabledObjects [popUpMenuIndex]); 
		//Debug.Log(menuManager.name);
		menuManager.ShowPopUpMenu(menuManager.disabledObjects[popUpMenuIndex]);


		MiniGameManager.Instance.StopMeasureTime();
		if(Application.loadedLevelName.Equals("FeedTheAnimal") || Application.loadedLevelName.Equals("MemoryGame") || Application.loadedLevelName.Equals("WhackAMole"))
		{
			transform.Find("UI/TimerLifeBg/Timer").SendMessage("PauseTimer",SendMessageOptions.DontRequireReceiver);
		}
		if(Application.loadedLevelName.Equals("FruitGameScene"))
		{
			Camera.main.SendMessage("TurnOffCollider",SendMessageOptions.DontRequireReceiver);
		}
		paused = true;
		canClick = false;
		CancelInvoke("ResetCanClick");
		Invoke("ResetCanClick",1.25f);
	}
	
	public void ResumeGame (int popUpMenuIndex) {
		//TODO
		//SoundManager.Instance.Play_ButtonClick4();
		if(ButtonsScript.Instance != null)
			ButtonsScript.Instance.canPlay = true;
		//canPlay = true;
		//gamePaused = false;
		//owl.GetComponent<Rigidbody2D> ().isKinematic = false;
		
		//int numOfPopUps = GameObject.Find ("Canvas").GetComponent<MenuManager_FTA> ().disabledObjects.Length / 2;
		//GameObject.Find ("Canvas").GetComponent<MenuManager_FTA> ().ClosePopUpMenu (GameObject.Find ("Canvas").GetComponent<MenuManager_FTA> ().disabledObjects [3 * currentOrientation + popUpMenuIndex]);

		//marko
		//GameObject.Find ("Canvas").GetComponent<MenuManager_FTA> ().ClosePopUpMenu (GameObject.Find ("Canvas").GetComponent<MenuManager_FTA> ().disabledObjects [popUpMenuIndex]);
		menuManager.ClosePopUpMenu(menuManager.disabledObjects[popUpMenuIndex]);

		MiniGameManager.Instance.StartMeasureTime();
		if(Application.loadedLevelName.Equals("FeedTheAnimal") || Application.loadedLevelName.Equals("MemoryGame") || Application.loadedLevelName.Equals("WhackAMole"))
		{
			transform.Find("UI/TimerLifeBg/Timer").SendMessage("UnpauseTimer",SendMessageOptions.DontRequireReceiver);
		}
		if(Application.loadedLevelName.Equals("FruitGameScene"))
		{
			Camera.main.SendMessage("TurnOnCollider",SendMessageOptions.DontRequireReceiver);
		}
		//Invoke("UnpauseWithDelay",1f);
		paused = false;
		canClick = false;
		CancelInvoke("ResetCanClick");
		Invoke("ResetCanClick",1.25f);
	}

	public void LoadLevel()
	{
		Time.timeScale = 1;

		if(GlobalVariables_FTA.chosenMiniGameMusic == 1)
		{
			//TODO
			//SoundManager.Instance.Stop_GameplayMusicMiniGames1();
		}
		else
		{
			//TODO
			//SoundManager.Instance.Stop_GameplayMusicMiniGames2();
		}

		if(!GlobalVariables_FTA.removeAdsBought)
		{
			//TODO
			//WebelinxCMS.Instance.ShowInterstitial(3);
		}

		//if(LevelTransition.Instance != null) LevelTransition.Instance.HideSceneAndLoadNext(Application.loadedLevelName);

		 Application.LoadLevel(Application.loadedLevelName);

	}

	public void GameOver () 
	{
		//Debug.Log("GAME OVER");
		//TODO
		//WebelinxCMS.Instance.IsVideoRewardAvailable(6);
		/*if(Screen.orientation == ScreenOrientation.Landscape)
			currentOrientation = 0;
		Debug.Log("current orientation: " + currentOrientation);*/
		if(GlobalVariables_FTA.chosenMiniGameMusic == 1)
		{
			//TODO
			//SoundManager.Instance.Stop_GameplayMusicMiniGames1();
		}
		else
		{
			//TODO
			//SoundManager.Instance.Stop_GameplayMusicMiniGames2();
		}
		SoundManagerMiniGames.Instance.Play_GameOver();
		MiniGameManager.Instance.FinalScore();
		if(!GlobalVariables_FTA.removeAdsBought)
			MiniGameManager.Instance.ShowAd();
		ButtonsScript.Instance.canPlay = false;
		//int numOfPopUps = GameObject.Find ("Canvas").GetComponent<MenuManager_FTA> ().disabledObjects.Length / 2;
		//GameObject.Find ("Canvas").GetComponent<MenuManager_FTA> ().ShowPopUpMenu (GameObject.Find ("Canvas").GetComponent<MenuManager_FTA> ().disabledObjects [2 + currentOrientation * 3]);

		//marko
		//GameObject.Find ("Canvas").GetComponent<MenuManager_FTA> ().ShowPopUpMenu (GameObject.Find ("Canvas").GetComponent<MenuManager_FTA> ().disabledObjects [2]);
		menuManager.ShowPopUpMenu(menuManager.disabledObjects[2]);

		MiniGameManager.Instance.IncreaseMoney();
		MiniGameManager.Instance.StopMeasureTime();
		MiniGameManager.Instance.canPlay = false;
	}

	public void SetScore(int value)
	{
		scoreText.text = value.ToString();
		SetHighscore(value);
	}

	public void SetCoins(int value)
	{
		coinsText.text = value.ToString();
		MiniGameManager.Instance.SetMoney(value);
	}

	public void SetHighscore(int score)
	{
		if(prefsName != "")
		{
			if (!PlayerPrefs.HasKey (prefsName) || (PlayerPrefs.HasKey (prefsName) && PlayerPrefs.GetInt (prefsName) < score))
			{
				PlayerPrefs.SetInt (prefsName, score);
				hiscoreText.text = score.ToString();
			}
		}
	}

	public void CheckHighscore(string prefsNameForMiniGame)
	{
		prefsName = prefsNameForMiniGame;
		if(PlayerPrefs.HasKey (prefsName))
		{
			hiscoreText.text = PlayerPrefs.GetInt(prefsName).ToString();
		}
	}

}
