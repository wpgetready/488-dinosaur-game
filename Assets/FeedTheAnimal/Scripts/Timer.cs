﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Scene: MiniGame_3
/// Object:N/A
/// Description: Used for counting down time in pop the cat min game
/// </summary>

public class Timer : MonoBehaviour {

	public Text timer;
	public float minutes = 0;
	public float seconds = 20;
	float miliseconds = 0;

	string secondsString;
	string milisecondsString;

	private bool gameoverCalled;

	public bool gamePaused;

	public static Timer instance;
	Image progressBar;
	float reduceTimeRatio;
	public float maxTime;

	void Awake()
	{
		timer = GetComponent<Text>();

		gameoverCalled = false;

		gamePaused = true;

		//seconds = MemoryGameManager.memoryGameManager.startingTime;
		timer.text = seconds.ToString();// + ":00";

		instance = this;
		progressBar = GameObject.Find("TimerBarr").GetComponent<Image>();
		//reduceTimeRatio = 1/MemoryGameManager.memoryGameManager.startingTime;
		progressBar.color = Color.green;
	}

	void SetSeconds(float startingTime)
	{
		seconds = startingTime;
		reduceTimeRatio = 1/startingTime;
		maxTime = startingTime;
	}
	
	public void AddTime(float sec)
	{
		seconds += sec;
		if(seconds > maxTime) seconds = maxTime;
		progressBar.fillAmount += reduceTimeRatio*sec;
		if(progressBar.fillAmount < 0.16f)
			progressBar.color = Color.red;
		else if(progressBar.fillAmount < 0.5f)
			progressBar.color = Color.yellow;
		else
			progressBar.color = Color.green;
	}

	void PauseTimer()
	{
		gamePaused = true;
	}

	void UnpauseTimer()
	{
		gamePaused = false;
	}

	void Update(){

		if (!gamePaused)
		{
			if(miliseconds <= 0)
			{
				if(seconds <= 0)
				{
					minutes--;
					seconds = 59;
				}
				else if(seconds >= 0)
				{
					seconds--;
					progressBar.fillAmount -= reduceTimeRatio;
					if(progressBar.fillAmount < 0.16f)
						progressBar.color = Color.red;
					else if(progressBar.fillAmount < 0.5f)
						progressBar.color = Color.yellow;
					else
						progressBar.color = Color.green;
				}
				
				miliseconds = 100;
			}
			
			miliseconds -= Time.deltaTime * 100;

			if (seconds < 10)
				secondsString = "0" + seconds.ToString();
			else
				secondsString = seconds.ToString();

			// If seconds less than 5 colour lettrers in red, else white
			if (seconds < 5)
			{
				Color c = GetComponent<Text>().color;

				c.r = 1f;
				c.g = 0;
				c.b = 0;

				//GetComponent<Text>().color = c;

				// If beeping is not playing play
//				if (!SoundManager.Instance.beepSound.isPlaying)
//					SoundManager.Instance.Play_BeepSound();
			}
			else
			{
				Color c = GetComponent<Text>().color;
				
				c.r = 1f;
				c.g = 1f;
				c.b = 1f;
				
				//GetComponent<Text>().color = c;

//				SoundManager.Instance.Stop_BeepSound();
			}
			if(seconds <= 0)
				secondsString = "0";

			if (miliseconds < 10)
			{
				int ms;
				ms = (int)miliseconds;
				milisecondsString = "0" + ms.ToString();
			}
			else
			{
				int ms;
				ms = (int)miliseconds;
				milisecondsString = ms.ToString();
			}

			if (minutes >= 0)
			{
				timer.text = /*minutes.ToString() + ":" + */secondsString;// + ":" + milisecondsString;
			}
			else
			{
				timer.text = "0";

				if (!gameoverCalled)
				{
					gameoverCalled = true;
					gamePaused = true;
					CanvasInPenguins.Instance.GameOver();
					// Play won sound
//					SoundManager.Instance.Play_WonSound();
//
//					UndressingGameManager.instance.GameOver();
				}
			}
		}
	}
}
