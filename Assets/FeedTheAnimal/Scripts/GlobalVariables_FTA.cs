﻿using UnityEngine;
using System.Collections;

public class GlobalVariables_FTA : MonoBehaviour {

	public static bool removeAdsBought;
	public static bool doubleCoinsBought;
	public static bool parrotBought;
	public static string priceInapp1 = System.String.Empty;
	public static string priceInapp2 = System.String.Empty;
	public static string priceInapp3 = System.String.Empty;
	public static string priceInapp4 = System.String.Empty;
	public static string priceRemoveAds = System.String.Empty;
	public static string priceParrot = System.String.Empty;
	public static string priceDoubleCoins = System.String.Empty;
	public static int WatchVideoReward = 150;
	public static bool showWebelinxBanner = true;
	public static bool petIsSleeping = false;
	public static int allowNotifications = 1;
	public static int chosenMiniGameMusic = 1;
	public static bool interstitialHasShown = false;
	public static bool adDisplayed = false;
	public static bool MainSceneEnteredFirstTime = true;
	public static int timeRewardStatus = 0; // 0 - default, 1 - ticking, 2 - done
	public static int timeRewardAmount = 100;

	void Awake ()
	{
		DontDestroyOnLoad(this.gameObject);

		if(PlayerPrefs.HasKey("DCForever"))
		{
			doubleCoinsBought = true;
		}
		else
		{
			doubleCoinsBought = false;
		}

		if(PlayerPrefs.HasKey("RADSForever"))
		{
			removeAdsBought = true;
			showWebelinxBanner = false;
		}
		else
		{
			removeAdsBought = false;
			showWebelinxBanner = true;
		}

	}

//	public static void FlurryEvent(string eventName, string key, string value)
//	{
//#if UNITY_ANDROID
//		try
//		{
//			using (AndroidJavaClass cls_UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer")) 
//			{
//				using (AndroidJavaObject obj_Activity = cls_UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity")) 
//				{
//					obj_Activity.Call("FlurryEvents",eventName + "#" + key + "#" + value);
//				}
//			} 
//			return;
//		}
//		catch
//		{
//			Debug.Log("Error Contacting Android! Flurry call.");
//			return;
//		}
//#elif UNITY_IOS
//		WebelinxFlurryBinding.sendMessage("FlurryEvents#"+eventName+"#"+key+"#"+value);
//#endif
//	}
	
}
