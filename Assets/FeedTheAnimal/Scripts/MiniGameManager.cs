using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class MiniGameManager : MonoBehaviour {

	static MiniGameManager instance;
	public static MiniGameManager Instance
	{
		get
		{
			if(instance == null)
				instance = GameObject.FindObjectOfType(typeof(MiniGameManager)) as MiniGameManager;
			
			return instance;
		}
	}
	static int totalSeconds = 0;
	DateTime beginTime;
	DateTime stopTime;
	static int money = 0;
	bool measuring = false;
	bool statusOnPause = false;
	public static bool opened = false;
	[HideInInspector] public bool canPlay = false;
	public static bool orientationIsLandscape = false;

	void Awake () 
	{
//		switch(Application.loadedLevelName)
//		{
//		case "BasketballGameScene": Screen.orientation = ScreenOrientation.Portrait; orientationIsLandscape = false; break;
//		case "ElevatorsGameScene": Screen.orientation = ScreenOrientation.Portrait; orientationIsLandscape = false; break;
//		case "FruitGameScene": Screen.orientation = ScreenOrientation.Portrait; orientationIsLandscape = false; break;
//		case "OwlGameScene": Screen.orientation = ScreenOrientation.Portrait; orientationIsLandscape = false; break;
//		case "PlanetHopGameScene": Screen.orientation = ScreenOrientation.Portrait; orientationIsLandscape = false; break;
//		case "ClimbTheTower": Screen.orientation = ScreenOrientation.Portrait; orientationIsLandscape = false; break;
//		case "StickHero": Screen.orientation = ScreenOrientation.Portrait; orientationIsLandscape = false; break;
//		case "FeedTheAnimal": Screen.orientation = ScreenOrientation.Landscape; orientationIsLandscape = true; break;
//		case "WhackAMole": Screen.orientation = ScreenOrientation.Portrait; orientationIsLandscape = false; break;
//		case "AnimalEvade": Screen.orientation = ScreenOrientation.Portrait; orientationIsLandscape = false; break;
//		case "JumpThePenguinGameScreen": Screen.orientation = ScreenOrientation.Landscape; orientationIsLandscape = true; break;
//		case "MemoryGame": Screen.orientation = ScreenOrientation.Portrait; orientationIsLandscape = false; break;
//		}
		Transform canvas = GameObject.Find("Canvas").transform;

#if UNITY_ANDROID
//		if(Screen.orientation == ScreenOrientation.Landscape && Camera.main.aspect < 1)
//			Camera.main.aspect = 1/Camera.main.aspect;
#elif UNITY_IOS
//		if(orientationIsLandscape == true && Camera.main.aspect < 1)
//		{
//			Camera.main.aspect = 1/Camera.main.aspect;
//			Screen.orientation = ScreenOrientation.Landscape;
//		}
#endif

		SoundManagerMiniGames.Instance.Stop_GameOver();
		if(!GlobalVariables_FTA.adDisplayed)
		{
			Invoke("CheckMiniGamesMusic",1f);
		}

		if(GlobalVariables_FTA.doubleCoinsBought)
		{
			canvas.Find("PopUps/PopUpGameOverPortrait/AnimationHolder/Body/BodyContent/PriceHolder").gameObject.SetActive(false);
			canvas.Find("PopUps/PopUpGameOverPortrait/AnimationHolder/Body/BodyContent/WatchVideoHolder").localPosition = canvas.Find("PopUps/PopUpGameOverPortrait/AnimationHolder/Body/BodyContent/PriceHolder").localPosition;

		}
//		canvas.Find("PopUps/PopUpGameOverPortrait/AnimationHolder/Body/BodyContent/WatchVideoHolder").gameObject.SetActive(false);

		if(GlobalVariables_FTA.removeAdsBought)
		{
			canvas.Find("Banner").gameObject.SetActive(false);
			canvas.Find("WebelinxBanner").gameObject.SetActive(false);
			canvas.Find("UI/TimerLifeBg").GetComponent<RectTransform>().anchoredPosition = new Vector2(canvas.Find("UI/TimerLifeBg").GetComponent<RectTransform>().anchoredPosition.x, -119);
		}

		if(!GlobalVariables_FTA.showWebelinxBanner)
		{
			canvas.Find("WebelinxBanner").gameObject.SetActive(false);
			
		}
	}

	void CheckMiniGamesMusic()
	{
		if(opened) // nije isao na Home, tj. nije pozvano ExitMiniGame()
		{
			if(GlobalVariables_FTA.chosenMiniGameMusic == 1)
			{
				//TODO
				//SoundManager.Instance.Play_GameplayMusicMiniGames1();
			}
			else
			{
				//TODO
				//SoundManager.Instance.Play_GameplayMusicMiniGames2();
			}
		}
	}

	void Start ()
	{
		if(!opened)
		{
			//GameObject.Find("Canvas").transform.Find("GameLoadingHolderInMiniGames/AnimationHolder").GetComponent<Animator>().Play("LoadingGameplayDeparting");
			if(LevelTransition.Instance != null) LevelTransition.Instance.ShowScene();
			opened = true;
			GameObject.Find("Canvas").transform.Find("PopUpTutorial").gameObject.SetActive(true);
			Invoke("OpenTutorial",0.5f);
		}
		else
		{
			if(LevelTransition.Instance != null) LevelTransition.Instance.ShowScene();
			//GameObject.Find("Canvas").transform.Find("GameLoadingHolderInMiniGames").gameObject.SetActive(false);
			GameObject.Find("Canvas").transform.Find("TapScreenToStart").gameObject.SetActive(true);
			//Camera.main.SendMessage("StartGame",SendMessageOptions.DontRequireReceiver);
			//canPlay = true;
		}

		 
	}

	void OpenTutorial()
	{
		GameObject.Find("Canvas").transform.Find("PopUpTutorial").GetComponent<Animator>().Play("Open");
	}

	public void FinalScore()
	{
		canPlay = false;
		Transform bodyContent = GameObject.Find("Canvas").transform.Find("PopUps/PopUpGameOver/AnimationHolder/Body/BodyContent");
		Transform interfaceContent = GameObject.Find("Canvas").transform.Find("UI/TimerLifeBg/InterfaceBg");
		bodyContent.Find("RequiredFinalSlot/RequiredFinalSlotText").GetComponent<Text>().text = interfaceContent.Find("Coins").GetComponent<Text>().text;
		bodyContent.Find("ScoreFinalSlot/ScoreFinalSlotText").GetComponent<Text>().text = interfaceContent.Find("Score").GetComponent<Text>().text;
		bodyContent.Find("HiscoreFinalSlot/HiScoreFinalSlotText").GetComponent<Text>().text = interfaceContent.Find("Highscore").GetComponent<Text>().text;

		//Debug.Log("WV sl:" + GameData.SelectedLevel +"    lul:" + (GameData.LastUnlockedLevel +1) +  "  req:"+ GameData.ReturnRequiredScoreToUnlockLevel() + "  pls:"+ ButtonsScript.Instance.playerScore);
		if( GameData.SelectedLevel == (GameData.LastUnlockedLevel+1) &&   GameData.ReturnRequiredScoreToUnlockLevel() <= ButtonsScript.Instance.playerScore )
		{
			GameData.UnlockNewLevel();
			bodyContent.parent.Find("WatchVideoBody").gameObject.SetActive(false);
			 
			bodyContent.Find("LevelUnlockedLabel").GetComponent<Text>().text = "new level is unlocked";
		}
		else if( GameData.SelectedLevel <= (GameData.LastUnlockedLevel))
		{
			bodyContent.parent.Find("WatchVideoBody").gameObject.SetActive(false);
 
			bodyContent.Find("LevelUnlockedLabel").GetComponent<Text>().text = "new level is unlocked";
		}
	}

	public void StartMeasureTime()
	{
		measuring = true;
		beginTime = System.DateTime.Now;
	}

	public void StopMeasureTime()
	{
		if(measuring)
		{
			measuring = false;
			stopTime = System.DateTime.Now;
			TimeSpan difference = stopTime - beginTime;
			totalSeconds = (int)difference.TotalSeconds;
		}
	}

	public void AddMoney(int amount)
	{
		money += amount;
	}

	public void SetMoney(int amount)
	{
		money = amount;
	}

	public void IncreaseMoney()
	{
		//TODO
		//VariablesManager.money += money;
		//PlayerPrefs.SetInt("Kinta",VariablesManager.money);
		PlayerPrefs.Save();
		money = 0;
	}

	void OnApplicationPause(bool status)
	{
		if(status) //game paused
		{
			statusOnPause = measuring;
			StopMeasureTime();
		}
		else //game resumed
		{
			if(statusOnPause)
				StartMeasureTime();
		}
	}

	public void ExitMiniGame()
	{
		Time.timeScale = 1;
		StopMeasureTime();
		//TODO
		//VariablesManager.money += money;
		//PlayerPrefs.SetInt("Kinta",VariablesManager.money);
		PlayerPrefs.Save();
		if(GlobalVariables_FTA.chosenMiniGameMusic == 1)
		{
			//TODO
			//SoundManager.Instance.Stop_GameplayMusicMiniGames1();
		}
		else
		{
			//TODO
			//SoundManager.Instance.Stop_GameplayMusicMiniGames2();
		}
		StartCoroutine(LoadMainScene());
	}

	IEnumerator LoadMainScene()
	{
		//TODO
		//SoundManager.Instance.Play_DrinkPotion();
		opened = false;
//		GameObject.Find("Canvas").transform.Find("GameLoadingHolderInMiniGames").gameObject.SetActive(true);
//		GameObject.Find("Canvas/GameLoadingHolderInMiniGames/AnimationHolder").GetComponent<Animator>().Play("LoadingGameplayArriving");
		if(!GlobalVariables_FTA.removeAdsBought)
		{
			//TODO
			//WebelinxCMS.Instance.ShowInterstitial(3);
		}
		//yield return new WaitForSeconds(2f);
		//Application.LoadLevel("LoadingAfterMiniGame");
		yield return new WaitForSeconds(.2f);
		if(LevelTransition.Instance != null) LevelTransition.Instance.HideSceneAndLoadNext("Map");
	}

	public void CloseTutorial()
	{
		//TODO
		//SoundManager.Instance.Play_ButtonClick4();
		GameObject.Find("Canvas").transform.Find("PopUpTutorial").GetComponent<Animator>().Play("MainMenuClosed");
		GameObject.Find("Canvas").transform.Find("PopUpTutorial/AnimationHolder/Body/ButtonsHolder/ButtonClose").GetComponent<Button>().interactable = false;
		GameObject.Find("Canvas").transform.Find("PopUpTutorial/AnimationHolder/Body").GetComponent<Button>().interactable = false;
		Invoke("DisableTutorial",0.9f); //0.25
	}

	void DisableTutorial()
	{
		canPlay = true;
		Camera.main.SendMessage("StartGame",SendMessageOptions.DontRequireReceiver);
		GameObject.Find("Canvas").transform.Find("PopUpTutorial").gameObject.SetActive(false);
	}

	public void ShowNoInappPopup()
	{
		GameObject.Find("Canvas").transform.Find("PopUpNoInapp").gameObject.SetActive(true);
		GameObject.Find("Canvas").transform.Find("PopUpNoInapp").GetComponent<Animator>().Play("Open");
	}

	public void CloseNoInappPopup()
	{
		GameObject.Find("Canvas").transform.Find("PopUpNoInapp").GetComponent<Animator>().Play("MainMenuClosed");
		Invoke("DisableNoInappPopup",0.9f);
	}

	public void DisableNoInappPopup()
	{
		GameObject.Find("Canvas").transform.Find("PopUpNoInapp").gameObject.SetActive(false);
	}

	public void ShowNoWatchVideoPopup()
	{
		GameObject.Find("Canvas").transform.Find("PopUpNoInapp").gameObject.SetActive(true);
		GameObject.Find("Canvas").transform.Find("PopUpNoInapp").GetComponent<Animator>().Play("Open");
	}
	
	public void CloseNoWatchVideoPopup()
	{
		GameObject.Find("Canvas").transform.Find("PopUpNoInapp").GetComponent<Animator>().Play("MainMenuClosed");
		Invoke("DisableNoInappPopup",0.9f);
	}
	
	public void DisableNoWatchVideoPopup()
	{
		GameObject.Find("Canvas").transform.Find("PopUpNoInapp").gameObject.SetActive(false);
	}

	public void BuyInApp(string ID)
	{
		//TODO
		//WebelinxAndroidInApps.Instance.BuyInApp(ID);
//		GlobalVariables_FTA.FlurryEvent("Inapp Events","Inapp bought",ID);
	}

	public void ShowAd()
	{
		//Invoke("ShowAdWithDelay",1.75f);
	}

	void ShowAdWithDelay()
	{
		if(!GlobalVariables_FTA.removeAdsBought)
		{
			//TODO
			//WebelinxCMS.Instance.ShowInterstitial(3);
		}
	}

	public void TapScreenToStart()
	{
		canPlay = true;
		Camera.main.SendMessage("StartGame",SendMessageOptions.DontRequireReceiver);
		GameObject.Find("Canvas").transform.Find("TapScreenToStart").gameObject.SetActive(false);
	}

/*	//UPDATE!!!!!
	public void WatchVideoForDoubleCoins()
	{
		//TODO
		//WebelinxCMS.Instance.IsVideoRewardAvailable(6);
	}

	//UPDATE!!!!!
	public void WatchVideoAvailability(bool status)
	{
		if(status)
		{
			Transform canvas = GameObject.Find("Canvas").transform;
			if(canvas.Find("PopUps/PopUpGameOverPortrait/AnimationHolder/Body/BodyContent/WatchVideoHolder").gameObject.activeSelf)
			{
				//TODO
				//WebelinxCMS.Instance.ShowVideoReward(6);
			}
			else
			{
				canvas.Find("PopUps/PopUpGameOverPortrait/AnimationHolder/Body/BodyContent/WatchVideoHolder").gameObject.SetActive(true);
			}
		}
		else
		{
			ShowNoWatchVideoPopup();
		}
	}

	//UPDATE!!!!!
	public void DoubleMoney()
	{
		Transform bodyContent = GameObject.Find("Canvas").transform.Find("PopUps/PopUpGameOverPortrait/AnimationHolder/Body/BodyContent");
		int currentEarnedMoney = int.Parse(bodyContent.Find("CoinsFinalSlot/CoinsFinalSlotText").GetComponent<Text>().text);
		Debug.Log("current earned money: " + currentEarnedMoney + ", trenutno u money: " + money);
		SetMoney(currentEarnedMoney);
		IncreaseMoney();
		bodyContent.Find("CoinsFinalSlot/CoinsFinalSlotText").GetComponent<Text>().text = (currentEarnedMoney*2).ToString();
		bodyContent.Find("WatchVideoHolder").gameObject.SetActive(false);
	}
	 */



	
	public void ButtonWatchVideoToUnlockClicked()
	{
		Shop.Instance.WatchVideo();
		SoundManager.Instance.Play_ButtonClick();
		BlockClicks.Instance.SetBlockAll(true);
	}
	
	
	
	public void FinishWatchingVideo()
	{
		GameData.UnlockNewLevel();
		//Debug.Log("LEVEL " + GameData.LastUnlockedLevel.ToString());
		Transform bodyContent = GameObject.Find("Canvas").transform.Find("PopUps/PopUpGameOver/AnimationHolder/Body/BodyContent");
		bodyContent.parent.Find("WatchVideoBody").gameObject.SetActive(false);
		bodyContent.Find("LevelUnlockedLabel").GetComponent<Text>().text = "new level is unlocked";
		StartCoroutine(LoadMainScene());
	}

}
