﻿using UnityEngine;
using System.Collections;

public class SoundManagerMiniGames : MonoBehaviour {

	static SoundManagerMiniGames instance;
	
	public static SoundManagerMiniGames Instance
	{
		get
		{
			if(instance == null)
			{
				instance = GameObject.FindObjectOfType(typeof(SoundManagerMiniGames)) as SoundManagerMiniGames;
			}
			
			return instance;
		}
	}

	public AudioSource gameplayMusicMiniGames1;
	public AudioSource gameOver;
	public AudioSource collectCoin;
	public AudioSource throwBall;
	public AudioSource hitHoop;
	public AudioSource ballFallDown;
	public AudioSource score;
	public AudioSource boardHit;
	public AudioSource jumpElevator;
	public AudioSource foodCollect;
	public AudioSource storeInBasket;
	public AudioSource bombHit;
	public AudioSource collectShield;
	public AudioSource collectScoreBoost;
	public AudioSource prepareForJump;
	public AudioSource jumpOwl;
	public AudioSource spikesHit;
	public AudioSource jumpPlanet;
	public AudioSource landPlanet;
	public AudioSource starCollect;
	public AudioSource hop;
	public AudioSource bounce;
	public AudioSource explodeClimb;
	public AudioSource extendStick;
	public AudioSource nextPlatform;
	public AudioSource swapPosition;
	public AudioSource eatFood;
	public AudioSource moveAssembly;
	public AudioSource timeBonus;
	public AudioSource timePenalty;
	public AudioSource blocksHit;
	public AudioSource running;
	public AudioSource rockFallDown;
	public AudioSource blades;
	public AudioSource woodenDoorOpen;
	public AudioSource hitTreeOrBox;
	public AudioSource explodeEvade;
	public AudioSource fall;
	public AudioSource jumpJumping;
	public AudioSource waterSplash;
	public AudioSource cardClick;
	public AudioSource cardsPaired;
	public AudioSource clearBoard;

	public void Play_GameplayMusicMiniGames1()
	{
		if(gameplayMusicMiniGames1.clip != null && SoundManager.musicOn == 1)// && !forceTurnOff)
		{
			gameplayMusicMiniGames1.Play();
		}
	}
	
	public void Stop_GameplayMusicMiniGamesFadeOut1()
	{
		if(gameplayMusicMiniGames1.clip != null && SoundManager.musicOn == 1)
		{
			StartCoroutine(FadeOut(gameplayMusicMiniGames1, 0.005f));
		}
	}
	
	public void Stop_GameplayMusicMiniGames1()
	{
		if(gameplayMusicMiniGames1.clip != null && SoundManager.musicOn == 1)
			gameplayMusicMiniGames1.Stop();
	}

	public void Play_GameOver()
	{
		if(gameOver.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			gameOver.Play();
	}
	
	public void Stop_GameOver()
	{
		if(gameOver.clip != null && SoundManager.soundOn == 1)
			gameOver.Stop();
	}
	
	public void Play_CoinCollect()
	{
		if(collectCoin.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			collectCoin.Play();
	}
	
	public void Play_ThrowBall()
	{
		if(throwBall.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			throwBall.Play();
	}
	
	public void Play_HitHoop()
	{
		if(hitHoop.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			hitHoop.Play();
	}
	
	public void Play_BallFallDown()
	{
		if(ballFallDown.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			ballFallDown.Play();
	}
	
	public void Play_Score()
	{
		if(score.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			score.Play();
	}
	
	public void Play_BoardHit()
	{
		if(boardHit.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			boardHit.Play();
	}
	
	public void Play_JumpElevators()
	{
		if(jumpElevator.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			jumpElevator.Play();
	}
	
	public void Play_FoodCollect()
	{
		if(foodCollect.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			foodCollect.Play();
	}
	
	public void Play_StoreInBasket()
	{
		if(storeInBasket.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			storeInBasket.Play();
	}
	
	public void Play_BombHit()
	{
		if(bombHit.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			bombHit.Play();
	}
	
	public void Play_CollectShield()
	{
		if(collectShield.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			collectShield.Play();
	}
	
	public void Play_CollectScoreBoost()
	{
		if(collectScoreBoost.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			collectScoreBoost.Play();
	}
	
	public void Play_PrepareForJump()
	{
		if(prepareForJump.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			prepareForJump.Play();
	}
	
	public void Stop_PrepareForJump()
	{
		if(prepareForJump.clip != null && SoundManager.soundOn == 1)
			prepareForJump.Stop();
	}
	
	public void Play_JumpOwl()
	{
		if(jumpOwl.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			jumpOwl.Play();
	}
	
	public void Play_SpikesHit()
	{
		if(spikesHit.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			spikesHit.Play();
	}
	
	public void Play_JumpPlanet()
	{
		if(jumpPlanet.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			jumpPlanet.Play();
	}
	
	public void Play_LandPlanet()
	{
		if(landPlanet.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			landPlanet.Play();
	}
	
	public void Play_StarCollect()
	{
		if(starCollect.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			starCollect.Play();
	}
	
	public void Play_Hop()
	{
		if(hop.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			hop.Play();
	}
	
	public void Play_Bounce()
	{
		if(bounce.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			bounce.Play();
	}
	
	public void Play_ExplodeClimb()
	{
		if(explodeClimb.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			explodeClimb.Play();
	}
	
	public void Play_ExtendStick()
	{
		if(extendStick.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			extendStick.Play();
	}
	
	public void Stop_ExtendStick()
	{
		if(extendStick.clip != null && SoundManager.soundOn == 1)
			extendStick.Stop();
	}
	
	public void Play_NextPlatform()
	{
		if(nextPlatform.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			nextPlatform.Play();
	}
	
	public void Play_SwapPosition()
	{
		if(swapPosition.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			swapPosition.Play();
	}
	
	public void Play_EatFood()
	{
		if(eatFood.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			eatFood.Play();
	}
	
	public void Play_MoveAssembly()
	{
		if(moveAssembly.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			moveAssembly.Play();
	}
	
	public void Play_TimeBonus()
	{
		if(timeBonus.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			timeBonus.Play();
	}
	
	public void Play_TimePenalty()
	{
		if(timePenalty.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			timePenalty.Play();
	}
	
	public void Play_BlocksHit()
	{
		if(blocksHit.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			blocksHit.Play();
	}
	
	public void Play_Running()
	{
		if(running.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			running.Play();
	}
	
	public void Stop_Running()
	{
		if(running.clip != null && SoundManager.soundOn == 1)
			running.Stop();
	}
	
	public void Play_RockFallDown()
	{
		if(rockFallDown.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			rockFallDown.Play();
	}
	
	public void Play_Blades()
	{
		if(blades.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			blades.Play();
	}
	
	public void Play_WoodenDoorOpen()
	{
		if(woodenDoorOpen.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			woodenDoorOpen.Play();
	}
	
	public void Play_HitTreeOrBox()
	{
		if(hitTreeOrBox.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			hitTreeOrBox.Play();
	}
	
	public void Play_ExplodeEvade()
	{
		if(explodeEvade.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			explodeEvade.Play();
	}
	
	public void Play_Fall()
	{
		if(fall.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			fall.Play();
	}
	
	public void Play_JumpJumping()
	{
		if(jumpJumping.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			jumpJumping.Play();
	}
	
	public void Play_WaterSplash()
	{
		if(waterSplash.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			waterSplash.Play();
	}
	
	public void Play_CardClick()
	{
		if(cardClick.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			cardClick.Play();
	}
	
	public void Play_CardsPaired()
	{
		if(cardsPaired.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			cardsPaired.Play();
	}
	
	public void Play_ClearBoard()
	{
		if(clearBoard.clip != null && SoundManager.soundOn == 1)// && !forceTurnOff)
			clearBoard.Play();
	}

	IEnumerator FadeOut(AudioSource sound, float time)
	{
		float originalVolume = sound.volume;
		while(sound.volume != 0)
		{
			sound.volume = Mathf.MoveTowards(sound.volume, 0, time);
			yield return null;
		}
		sound.Stop();
		sound.volume = originalVolume;
	}
	
}
