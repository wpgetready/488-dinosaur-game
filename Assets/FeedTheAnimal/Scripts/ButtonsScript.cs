using UnityEngine;
using System.Collections;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;
using UnityEngine.EventSystems;

public class ButtonsScript : MonoBehaviour
{
	static ButtonsScript instance;

	public GameObject mouseDownObject;
	public GameObject mouseUpObject;
	public GameObject tutorialHolder;
	public GameObject pauseHolder;
	public GameObject finalPopUpHolder;

	//public MainMenuScript mainMenuScript;
	//public FacebookCoins facebookScript;

	public int playerScore = 0;
	public int coins = 0;

	public bool canPlay = false;
	public bool internetOK = false;
	public bool internetCheckDone = false;

	public string nextScene = "game";

	public static ButtonsScript Instance
	{
		get
		{
			if (instance == null)
				instance = GameObject.FindObjectOfType (typeof (ButtonsScript)) as ButtonsScript;
			
			return instance;
		}
	}

	// Use this for initialization
	void Start ()
	{
		//DontDestroyOnLoad (this.gameObject);
		Screen.sleepTimeout = SleepTimeout.NeverSleep;

		//if (PlayerPrefs.HasKey ("Coins"))
		//	coins = PlayerPrefs.GetInt ("Coins");

		//facebookScript = gameObject.GetComponent<FacebookCoins> ();
		StartCoroutine (CheckForInternetConnection ());
	}
	
	// Update is called once per frame
	public void Update ()
	{

		if (Input.GetMouseButtonDown (0))
		{
			//if(EventSystem.current.currentSelectedGameObject == null)
			{
				mouseDownObject = RaycastFunction (Input.mousePosition);
				if (mouseDownObject != null && mouseDownObject.tag == "Button")
					AnimateButton (mouseDownObject, 1);

//				Debug.Log (Input.mousePosition);
//				if (mouseDownObject == null)
//					Debug.Log ("null");
//				else
//					Debug.Log (mouseDownObject.name);
			}
		}

		if (Input.GetMouseButtonUp (0))
		{
			//if(EventSystem.current.currentSelectedGameObject == null)
			{
				if (mouseDownObject != null && mouseDownObject.tag == "Button")
					AnimateButton (mouseDownObject, 2);

				mouseUpObject = RaycastFunction (Input.mousePosition);

				if (mouseUpObject != null && mouseUpObject == mouseDownObject)
				{
					switch (mouseUpObject.name)
					{
						case "PlayButton":
							StartCoroutine (StartGame ());
							break;
						case "ExitButton":
							Application.Quit ();
							break;
						case "SettingsButton":
							//ShowSettingsScreen ();
							break;
						case "CoinsButton":
							ShowCoinsScreen ();
							break;
						case "LeaderboardButton":
							//GooglePlayGameServices.Instance.ShowLeaderboard (GooglePlayGameServices.Instance.leaderboardID);
							break;
						case "BackButtonSettingsFrame":
							//ShowMainMenuScreen ();
							break;
						case "BackButtonShopFrame":
							//ShowMainMenuScreen ();
							break;
						case "OnTextTop":
							//SoundManager.Instance.SetMusic (1);
							break;
						case "OffTextTop":
							//SoundManager.Instance.SetMusic (0);
							break;
						case "OnTextBottom":
							//SoundManager.Instance.SetSound (1);
							break;
						case "OffTextBottom":
							//SoundManager.Instance.SetSound (0);
							break;
						case "PauseScreenButton":
							ShowPauseScreen ();
							break;
						case "ResumeGameButton":
							HidePauseScreen ();
							HideTutorial ();
							break;
						case "RestartGameButton":
							RestartGame (true);
							break;
						case "RestartGamePauseButton":
							RestartGame (false);
							break;
						case "MainMenuButton":
							ExitGame (false);
							break;
						case "HomeScreenButton":
							ExitGame (true);
							break;
						case "TutorialScreenButton":
							ShowTutorial ();
							break;
						case "TutorialContinueButton":
							HideTutorial ();
							break;
						case "TutorialBackButton":
							ExitGame (false);
							break;
						case "WatchVideoButton":
							//WebelinxCMS.Instance.StartCMSAction ("reward");
							break;
						//case "LikePageWebelinxButton":
						//	facebookScript.OpenPageWebelinx ();
						//	break;
					}
				}
			}
		}
	}

	public GameObject RaycastFunction (Vector3 v)
	{
		Ray ray = Camera.main.ScreenPointToRay (v);
		RaycastHit hit;

		//if(EventSystem.current.currentSelectedGameObject == null)
		{
			if (Physics.Raycast (ray, out hit, 500))
				return hit.transform.gameObject;
		}

		return null;
	}

	// OnOff == 1 for mouse down on button, 2 for mouse up (to deselect button)
	void AnimateButton (GameObject button, int OnOff)
	{
		if (OnOff == 1) 
		{
			button.transform.localScale *= 0.9f;
			//SoundManager.Instance.PlaySound (//SoundManager.Instance.buttonClick);
		}
		else
			button.transform.localScale /= 0.9f;
	}

//	void ShowSettingsScreen ()
//	{
//		mainMenuScript.homeScreenHolder.SetActive (false);
//		mainMenuScript.settingsHolder.SetActive (true);
//	}

	void ShowCoinsScreen ()
	{
		//SetCoinsText ();
		//SetPageLiked ("Webelinx");

		//mainMenuScript.homeScreenHolder.SetActive (false);
		//mainMenuScript.coinsScreenHolder.SetActive (true);
	}

//	void ShowMainMenuScreen ()
//	{
//		mainMenuScript.settingsHolder.SetActive (false);
//		mainMenuScript.coinsScreenHolder.SetActive (false);
//		mainMenuScript.homeScreenHolder.SetActive (true);
//
//		mainMenuScript.homeScreenHolder.transform.Find ("AnimationHolder").animation.PlayQueued ("PlayButtonShine", QueueMode.CompleteOthers);
//	}

	void RestartGame (bool showInterstitial)
	{
		//WebelinxCMS.Instance.HideBanner ();
		//if (WebelinxCMS.cmsReady && showInterstitial)
		//	WebelinxCMS.Instance.StartCMSAction ("gameOver");

		playerScore = 0;
		nextScene = "game";
		Application.LoadLevel (Application.loadedLevelName);
	}

	void ExitGame (bool showInterstitial)
	{
		//WebelinxCMS.Instance.HideBanner ();
		//if (WebelinxCMS.cmsReady && showInterstitial)
		//	WebelinxCMS.Instance.StartCMSAction ("gameOver");

		playerScore = 0;
		nextScene = "main";
		//Application.LoadLevel ("ClimbTheTower");
		MiniGameManager.Instance.ExitMiniGame();
	}

	void PauseGame ()
	{
		canPlay = false;
		GameObject[] buttons = GameObject.FindGameObjectsWithTag ("Button");
		
		foreach (GameObject button in buttons)
			button.layer = LayerMask.NameToLayer ("Ignore Raycast");
	}

	void ResumeGame ()
	{
		canPlay = true;
		GameObject[] buttons = GameObject.FindGameObjectsWithTag ("Button");
		
		foreach (GameObject button in buttons)
			button.layer = LayerMask.NameToLayer ("Default");
	}

	IEnumerator StartGame ()
	{
		//Animation loadingGateAnimation = mainMenuScript.loadingGateHolder.transform.Find ("AnimationHolder").animation;
		//loadingGateAnimation.Play ("LoadingGateClose");

		//yield return new WaitForSeconds (loadingGateAnimation["LoadingGateClose"].clip.length);

		yield return null;

		nextScene = "main";
		Application.LoadLevel ("ClimbTheTower");
	}

	public IEnumerator InitializeGame ()
	{
		tutorialHolder = GameObject.Find ("TutorialHolder");
		pauseHolder = GameObject.Find ("PausePopUpHolder");
		finalPopUpHolder = GameObject.Find ("FinalPopUpHolder");

//		if (internetCheckDone && internetOK)
//		{
//			Transform rightButtonHolder = GameObject.Find ("RightButtonHolder").transform;
//			rightButtonHolder.position = new Vector3 (rightButtonHolder.position.x, rightButtonHolder.position.y - 2.0f, rightButtonHolder.position.z);
//		}
		
		//WebelinxCMS.Instance.ShowBanner ("banner");

		if(AspectChangeScript.Instance != null)
			AspectChangeScript.Instance.RepositionButtons ("game");

		HidePauseScreen ();
		HideTutorial ();
		//finalPopUpHolder.SetActive (false);

		//Animation loadingGateAnimation = Camera.main.transform.Find ("LoadingGateHolder/AnimationHolder").GetComponent<Animation>();
	//	loadingGateAnimation.Play ("LoadingGateOpen");

		bool tutorialWatched = PlayerPrefs.HasKey ("WatchedTutorial");

		if (!tutorialWatched)
			ShowTutorial ();

		//yield return new WaitForSeconds (loadingGateAnimation["LoadingGateOpen"].clip.length);

		yield return new WaitForSeconds(1);
		if (tutorialWatched)
			canPlay = true;
	}

	void ShowTutorial ()
	{
//		PauseGame ();
//		tutorialHolder.SetActive (true);
//
//		if (!PlayerPrefs.HasKey ("WatchedTutorial"))
//			PlayerPrefs.SetInt ("WatchedTutorial", 1);
	}

	void HideTutorial ()
	{
		//tutorialHolder.SetActive (false);
		ResumeGame ();
	}

	void ShowPauseScreen ()
	{
		PauseGame ();
		pauseHolder.SetActive (true);
	}

	void HidePauseScreen ()
	{
		//pauseHolder.SetActive (false);
		ResumeGame ();
	}

	public void ShowFinal ()
	{
		////SoundManager.Instance.PlaySound (//SoundManager.Instance.gameOver);
		canPlay = false;

		GameObject[] buttons = GameObject.FindGameObjectsWithTag ("Button");
		foreach (GameObject button in buttons)
			if (button.name != "RestartGameButton" && button.name != "HomeScreenButton" && button.name != "MainMenuButton")
				button.layer = LayerMask.NameToLayer ("Ignore Raycast");
		
		finalPopUpHolder.SetActive(true);
		finalPopUpHolder.transform.Find("AnimationHolder/BlackPopUp/ScoreText").gameObject.GetComponent<TextMesh>().text = playerScore.ToString();

		if (playerScore > PlayerPrefs.GetInt ("PlayerScore"))
		{
			PlayerPrefs.SetInt ("PlayerScore", playerScore);
			//GooglePlayGameServices.Instance.PostScoreToLeaderboard (playerScore, GooglePlayGameServices.Instance.leaderboardID);
		}
	}

	public void AddScore (int score)
	{
		playerScore += score;
		CanvasInPenguins.Instance.SetScore(playerScore);
	}

//	public void AddCoins (int value)
//	{
//		coins += value;
//		PlayerPrefs.SetInt ("Coins", coins);
//		CanvasInPenguins.Instance.SetCoins(coins);
//		//SetCoinsText ();
//	}

//	void SetCoinsText ()
//	{
//		if (mainMenuScript.coinsScreenHolder != null)
//		{
//			mainMenuScript.coinsScreenHolder.transform.Find ("GameSelectorButtonFrame/GameSelectorButtonText").GetComponent<TextMesh> ().text = coins.ToString ();
//			mainMenuScript.coinsScreenHolder.transform.Find ("GameSelectorButtonFrame/GameSelectorButtonText/Shadow").GetComponent<TextMesh> ().text = coins.ToString ();
//		}
//	}

//	public void SetPageLiked (string pageName)
//	{
//		if (PlayerPrefs.HasKey ("Like" + pageName))
//		{
//			mainMenuScript.coinsScreenHolder.transform.Find ("LikePage" + pageName + "Button").gameObject.layer = LayerMask.NameToLayer ("Ignore Raycast");
//			mainMenuScript.coinsScreenHolder.transform.Find ("LikePage" + pageName + "Button/LikePageText").GetComponent<TextMesh> ().color = Color.gray;
//		}
//	}
	
	public IEnumerator CheckForInternetConnection()
	{
		WWW www = new WWW("https://www.google.com");
		yield return www;
		if(!string.IsNullOrEmpty(www.error))
		{
			internetOK = false;
			internetCheckDone = true;
		}
		else
		{
			internetOK = true;
			internetCheckDone = true;
		}
	}
}

