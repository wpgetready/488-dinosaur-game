﻿using UnityEngine;
using System.Collections;

public class AspectChangeScript : MonoBehaviour {

	static AspectChangeScript instance;
	//MainMenuScript mainMenuScript;

	// Use this for initialization
	void Start () {
		//DontDestroyOnLoad(this.gameObject);
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static AspectChangeScript Instance
	{
		get
		{
			if (instance == null)
				instance = GameObject.FindObjectOfType (typeof (AspectChangeScript)) as AspectChangeScript;
			
			return instance;
		}
	}

	public void RepositionButtons(string screen)
	{
		GameObject[] buttons = GameObject.FindGameObjectsWithTag ("Button");

		foreach (GameObject button in buttons)
		{
			float buttonPosX = button.transform.position.x, buttonPosY = button.transform.position.y;

			if (Camera.main.aspect >= 1.0f)
				buttonPosX *= Camera.main.aspect / (16f / 9f);
			else if (screen == "main" && !button.transform.name.Contains("TextTop") && !button.transform.name.Contains("TextBottom") ||
			         screen == "game" && (button.transform.name.Contains("PauseScreenButton") || button.transform.name.Contains("TutorialScreenButton")))
				buttonPosX *= Camera.main.aspect / (9f / 16f);
			/*else if (screen == "main" || screen == "preload")
			{
				if (!button.transform.name.Contains("TextTop") && !button.transform.name.Contains("TextBottom"))
				{
					buttonPosX *= Camera.main.aspect;
					buttonPosY /= Camera.main.aspect / (4f / 3f);
				}
			}*/

			Vector3 buttonPos = new Vector3 (buttonPosX, buttonPosY, button.transform.position.z);
			button.transform.position = buttonPos;
		}

		/*if (Camera.main.aspect < 1.0f)
		{
			if (screen == "main")
			{
				mainMenuScript = GameObject.Find ("Main Camera").GetComponent<MainMenuScript> ();

				mainMenuScript.homeScreenHolder.transform.localScale *= Camera.main.aspect / (4f / 3f);
				mainMenuScript.settingsHolder.transform.localScale *= Camera.main.aspect / (4f / 3f);
				mainMenuScript.coinsScreenHolder.transform.localScale *= Camera.main.aspect / (4f / 3f);
			}

			if (screen == "game")
			{
				ButtonsScript.Instance.finalPopUpHolder.transform.localScale *= Camera.main.aspect / (4f / 3f); 
				ButtonsScript.Instance.tutorialHolder.transform.localScale *= Camera.main.aspect / (16f / 9f);
				ButtonsScript.Instance.pauseHolder.transform.localScale *= Camera.main.aspect / (4f / 3f);
			}

			GameObject.Find ("LogoGate").transform.localScale *= Camera.main.aspect / (4f / 3f);
		}*/
	}
}
