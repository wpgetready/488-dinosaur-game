﻿using UnityEngine;
using System.Collections;

/**
  * Scene:All
  * Object:SoundManager
  * Description: Skripta zaduzena za zvuke u apliakciji, njihovo pustanje, gasenje itd...
  **/
public class SoundManager2 : MonoBehaviour {

/*	public static int musicOn = 1;
	public static int soundOn = 1;
	public static bool forceTurnOff = false;
	public static bool disableSoundWhileAdShown = false;

	public AudioSource gameplayMusicMiniGames1;
	public AudioSource gameplayMusicMiniGames2;
	public AudioSource buttonClick;
	public AudioSource buttonClick2;
	public AudioSource buttonClick3;
	public AudioSource buttonClick4;
	public AudioSource chewingFood;
	public AudioSource cashRegister;
	public AudioSource collectPlantedFood;
	public AudioSource mixer;
	public AudioSource drinkPotion;
	public AudioSource switchOnOff;
	public AudioSource petThrilled;
	public AudioSource levelUp;
	public AudioSource popupOpen;
	public AudioSource popupClose;
	public AudioSource petYawning;
	public AudioSource petSnoring;
	public AudioSource petWakingUp;
	public AudioSource threadmillLevel1;
	public AudioSource threadmillLevel2;
	public AudioSource threadmillLevel3;
	public AudioSource liftingWeights;
	public AudioSource petTickle;
	public AudioSource legHop;
	public AudioSource stomachPunch;
	public AudioSource neckStretching;
	public AudioSource bite;
	public AudioSource eyePoke;
	public AudioSource lickScreen;
	public AudioSource glassShatter;
	public AudioSource hungryMessageShow;
	public AudioSource parrotIdle;
	public AudioSource parrotClick;
	public AudioSource spendMoney;
	public AudioSource earnMoney;
	public AudioSource plantSeeded;


	static SoundManager instance;

	public static SoundManager Instance
	{
		get
		{
			if(instance == null)
			{
				instance = GameObject.FindObjectOfType(typeof(SoundManager)) as SoundManager;
			}

			return instance;
		}
	}

	void Awake () 
	{
		DontDestroyOnLoad(this.gameObject);
		gameObject.name = this.GetType().ToString();

		if(PlayerPrefs.HasKey("SoundOn"))
		{
			musicOn = PlayerPrefs.GetInt("MusicOn");
			soundOn = PlayerPrefs.GetInt("SoundOn");
		}
		else
		{
			PlayerPrefs.SetInt("MusicOn",musicOn);
			PlayerPrefs.SetInt("SoundOn",soundOn);
			PlayerPrefs.Save();
		}

		Screen.sleepTimeout = SleepTimeout.NeverSleep; 
	}

	public void Play_GameplayMusicMiniGames1()
	{
		if(!disableSoundWhileAdShown)
		{
			if(gameplayMusicMiniGames1.clip != null && musicOn == 1)// && !forceTurnOff)
			{
				gameplayMusicMiniGames1.Play();
			}
		}
	}
	
	public void Stop_GameplayMusicMiniGamesFadeOut1()
	{
		if(!disableSoundWhileAdShown)
		{
			if(gameplayMusicMiniGames1.clip != null && musicOn == 1)
			{
				StartCoroutine(FadeOut(gameplayMusicMiniGames1, 0.005f));
			}
		}
	}
	
	public void Stop_GameplayMusicMiniGames1()
	{
		if(!disableSoundWhileAdShown)
		{
			if(gameplayMusicMiniGames1.clip != null && musicOn == 1)
				gameplayMusicMiniGames1.Stop();
		}
	}

	public void Play_GameplayMusicMiniGames2()
	{
		if(!disableSoundWhileAdShown)
		{
			if(gameplayMusicMiniGames2.clip != null && SoundManager.musicOn == 1)// && !forceTurnOff)
			{
				gameplayMusicMiniGames2.Play();
			}
		}
	}
	
	public void Stop_GameplayMusicMiniGamesFadeOut2()
	{
		if(!disableSoundWhileAdShown)
		{
			if(gameplayMusicMiniGames2.clip != null && SoundManager.musicOn == 1)
			{
				StartCoroutine(FadeOut(gameplayMusicMiniGames2, 0.005f));
			}
		}
	}
	
	public void Stop_GameplayMusicMiniGames2()
	{
		if(!disableSoundWhileAdShown)
		{
			if(gameplayMusicMiniGames2.clip != null && SoundManager.musicOn == 1)
				gameplayMusicMiniGames2.Stop();
		}
	}

	public void Play_ButtonClick()
	{
		if(!disableSoundWhileAdShown)
		{
			if(buttonClick.clip != null && soundOn == 1)// && !forceTurnOff)
				buttonClick.Play();
		}
	}

	public void Play_ButtonClick2()
	{
		if(!disableSoundWhileAdShown)
		{
			if(buttonClick2.clip != null && soundOn == 1)// && !forceTurnOff)
				buttonClick2.Play();
		}
	}

	public void Play_ButtonClick3()
	{
		if(!disableSoundWhileAdShown)
		{
			if(buttonClick3.clip != null && soundOn == 1)// && !forceTurnOff)
				buttonClick3.Play();
		}
	}

	public void Play_ButtonClick4()
	{
		if(!disableSoundWhileAdShown)
		{
			if(buttonClick4.clip != null && soundOn == 1)// && !forceTurnOff)
				buttonClick4.Play();
		}
	}

	public void Play_ChewingFood()
	{
		if(!disableSoundWhileAdShown)
		{
			if(chewingFood.clip != null && soundOn == 1 && !forceTurnOff)
				chewingFood.Play();
		}
	}

	public void Play_CashRegister()
	{
		if(!disableSoundWhileAdShown)
		{
			if(cashRegister.clip != null && soundOn == 1)// && !forceTurnOff)
				cashRegister.Play();
		}
	}

	public void Play_CollectPlantedFood()
	{
		if(!disableSoundWhileAdShown)
		{
			if(collectPlantedFood.clip != null && soundOn == 1)// && !forceTurnOff)
				collectPlantedFood.Play();
		}
	}

	public void Play_Mixer()
	{
		if(!disableSoundWhileAdShown)
		{
			if(mixer.clip != null && soundOn == 1)// && !forceTurnOff)
				mixer.Play();
		}
	}

	public void Play_DrinkPotion()
	{
		if(!disableSoundWhileAdShown)
		{
			if(drinkPotion.clip != null && soundOn == 1)// && !forceTurnOff)
				drinkPotion.Play();
		}
	}

	public void Play_SwitchOnOff()
	{
		if(!disableSoundWhileAdShown)
		{
			if(switchOnOff.clip != null && soundOn == 1)// && !forceTurnOff)
				switchOnOff.Play();
		}
	}

	public void Play_PetThrilled()
	{
		if(!disableSoundWhileAdShown)
		{
			if(petThrilled.clip != null && soundOn == 1 && !forceTurnOff)
				petThrilled.Play();
		}
	}

	public void Play_LevelUp()
	{
		if(!disableSoundWhileAdShown)
		{
			if(levelUp.clip != null && soundOn == 1)// && !forceTurnOff)
				levelUp.Play();
		}
	}

	public void Play_PopupOpen()
	{
		if(!disableSoundWhileAdShown)
		{
			if(popupOpen.clip != null && soundOn == 1)// && !forceTurnOff)
				popupOpen.Play();
		}
	}

	public void Play_PopupClose()
	{
		if(!disableSoundWhileAdShown)
		{
			if(popupClose.clip != null && soundOn == 1)// && !forceTurnOff)
				popupClose.Play();
		}
	}

	public void Play_PetYawning()
	{
		if(!disableSoundWhileAdShown)
		{
			if(petYawning.clip != null && soundOn == 1 && !forceTurnOff)
				petYawning.Play();
		}
	}

	public void Play_PetSnoring()
	{
		if(!disableSoundWhileAdShown)
		{
			if(petSnoring.clip != null && soundOn == 1 && !forceTurnOff)
				petSnoring.Play();
		}
	}

	public void Stop_PetSnoring()
	{
		if(!disableSoundWhileAdShown)
		{
			if(petSnoring.clip != null && soundOn == 1)
				petSnoring.Stop();
		}
	}

	public void Play_PetWakingUp()
	{
		if(!disableSoundWhileAdShown)
		{
			if(petWakingUp.clip != null && soundOn == 1 && !forceTurnOff)
				petWakingUp.Play();
		}
	}

	public void Play_ThreadmillLevel1()
	{
		if(!disableSoundWhileAdShown)
		{
			if(threadmillLevel1.clip != null && soundOn == 1 && !forceTurnOff)
				threadmillLevel1.Play();
		}
	}

	public void Stop_ThreadmillLevel1()
	{
		if(!disableSoundWhileAdShown)
		{
			if(threadmillLevel1.clip != null)// && soundOn == 1)
				threadmillLevel1.Stop();
		}
	}

	public void Play_ThreadmillLevel2()
	{
		if(!disableSoundWhileAdShown)
		{
			if(threadmillLevel2.clip != null && soundOn == 1 && !forceTurnOff)
				threadmillLevel2.Play();
		}
	}

	public void Stop_ThreadmillLevel2()
	{
		if(!disableSoundWhileAdShown)
		{
			if(threadmillLevel2.clip != null)// && soundOn == 1)
				threadmillLevel2.Stop();
		}
	}

	public void Play_ThreadmillLevel3()
	{
		if(!disableSoundWhileAdShown)
		{
			if(threadmillLevel3.clip != null && soundOn == 1 && !forceTurnOff)
				threadmillLevel3.Play();
		}
	}

	public void Stop_ThreadmillLevel3()
	{
		if(!disableSoundWhileAdShown)
		{
			if(threadmillLevel3.clip != null)// && soundOn == 1)
				threadmillLevel3.Stop();
		}
	}

	public void Play_LiftingWeights()
	{
		if(!disableSoundWhileAdShown)
		{
			if(liftingWeights.clip != null && soundOn == 1 && !forceTurnOff)
				liftingWeights.Play();
		}
	}

	public void Stop_LiftingWeights()
	{
		if(!disableSoundWhileAdShown)
		{
			if(liftingWeights.clip != null && soundOn == 1 && !forceTurnOff)
				liftingWeights.Stop();
		}
	}

	public void Play_PetTickle()
	{
		if(!disableSoundWhileAdShown)
		{
			if(petTickle.clip != null && soundOn == 1 && !forceTurnOff)
				petTickle.Play();
		}
	}

	public void Stop_PetTickle()
	{
		if(!disableSoundWhileAdShown)
		{
			if(petTickle.clip != null && soundOn == 1)
				petTickle.Stop();
		}
	}

	public void Play_LegHop()
	{
		if(!disableSoundWhileAdShown)
		{
			if(legHop.clip != null && soundOn == 1 && !forceTurnOff)
				legHop.Play();
		}
	}

	public void Stop_LegHop()
	{
		if(!disableSoundWhileAdShown)
		{
			if(legHop.clip != null && soundOn == 1)
				legHop.Stop();
		}
	}

	public void Play_StomachPunch()
	{
		if(!disableSoundWhileAdShown)
		{
			if(stomachPunch.clip != null && soundOn == 1 && !forceTurnOff)
				stomachPunch.Play();
		}
	}

	public void Play_NeckStretching()
	{
		if(!disableSoundWhileAdShown)
		{
			if(neckStretching.clip != null && soundOn == 1 && !forceTurnOff)
				neckStretching.Play();
		}
	}

	public void Stop_NeckStretching()
	{
		if(!disableSoundWhileAdShown)
		{
			if(neckStretching.clip != null && soundOn == 1 && !forceTurnOff)
				neckStretching.Stop();
		}
	}

	public void Play_Bite()
	{
		if(!disableSoundWhileAdShown)
		{
			if(bite.clip != null && soundOn == 1 && !forceTurnOff)
				bite.Play();
		}
	}

	public void Play_EyePoke()
	{
		if(!disableSoundWhileAdShown)
		{
			if(eyePoke.clip != null && soundOn == 1 && !forceTurnOff)
				eyePoke.Play();
		}
	}

	public void Play_LickScreen()
	{
		if(!disableSoundWhileAdShown)
		{
			if(lickScreen.clip != null && soundOn == 1 && !forceTurnOff)
				lickScreen.Play();
		}
	}

	public void Play_GlassShatter()
	{
		if(!disableSoundWhileAdShown)
		{
			if(glassShatter.clip != null && soundOn == 1 && !forceTurnOff)
				glassShatter.Play();
		}
	}

	public void Play_HungryMessageShow()
	{
		if(!disableSoundWhileAdShown)
		{
			if(hungryMessageShow.clip != null && soundOn == 1 && !forceTurnOff)
				hungryMessageShow.Play();
		}
	}

	public void Play_ParrotIdle()
	{
		if(!disableSoundWhileAdShown)
		{
			if(parrotIdle.clip != null && soundOn == 1)// && !forceTurnOff)
				parrotIdle.Play();
		}
	}

	public void Play_ParrotClick()
	{
		if(!disableSoundWhileAdShown)
		{
			if(parrotClick.clip != null && soundOn == 1)// && !forceTurnOff)
				parrotClick.Play();
		}
	}

	public void Play_SpendMoney()
	{
		if(!disableSoundWhileAdShown)
		{
			if(spendMoney.clip != null && soundOn == 1)// && !forceTurnOff)
				spendMoney.Play();
		}
	}

	public void Play_EarnMoney()
	{
		if(!disableSoundWhileAdShown)
		{
			if(earnMoney.clip != null && soundOn == 1)// && !forceTurnOff)
				earnMoney.Play();
		}
	}

	public void Play_PlantSeeded()
	{
		if(!disableSoundWhileAdShown)
		{
			if(plantSeeded.clip != null && soundOn == 1)// && !forceTurnOff)
				plantSeeded.Play();
		}
	}
	

	/// <summary>
	/// Corutine-a koja za odredjeni AudioSource, kroz prosledjeno vreme, utisava AudioSource do 0, gasi taj AudioSource, a zatim vraca pocetni Volume na pocetan kako bi AudioSource mogao opet da se koristi
	/// </summary>
	/// <param name="sound">AudioSource koji treba smanjiti/param>
	/// <param name="time">Vreme za koje treba smanjiti Volume/param>
	IEnumerator FadeOut(AudioSource sound, float time)
	{
		float originalVolume = sound.volume;
		while(sound.volume != 0)
		{
			sound.volume = Mathf.MoveTowards(sound.volume, 0, time);
			yield return null;
		}
		sound.Stop();
		sound.volume = originalVolume;
	}
	*/
}
