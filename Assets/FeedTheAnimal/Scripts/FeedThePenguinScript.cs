using UnityEngine;
using System.Collections;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;
using UnityEngine.UI;

public class FeedThePenguinScript : MonoBehaviour {

	public GameObject[] foodPrefabs;
	List<GameObject> foodList;

	bool canClickButton = true;
	bool firstTimeEat = true;

	public float timeRemaining;
	public float timeToAdd;
	public int correctAnswersToAddTime;
	int currentCorrectAnswers = 0;
	public float timeToRemove;
	int coins = 0;
	bool gameOverCalled = false;
	Transform timerLifeBg;
	Image progressBar;
	float reduceTimeRatio;

	// Use this for initialization
	void Start () {
		CanvasInPenguins.Instance.CheckHighscore("FeedTheAnimal_Highscore");
		timerLifeBg = GameObject.Find("TimerLifeBg").transform;
		timerLifeBg.Find("NumberPlusMinusHolder/+2").GetComponent<Text>().text = "+"+timeToAdd;
		timerLifeBg.Find("NumberPlusMinusHolder/-2").GetComponent<Text>().text = "-"+timeToAdd;
		progressBar = GameObject.Find("TimerBarr").GetComponent<Image>();
		reduceTimeRatio = 1/timeRemaining;
		progressBar.color = Color.green;
		//InitializeGame ();

		GlobalVariables.OnPauseGame += FLPauseGame;
	}

	void Awake ()
	{
		GameObject.Find("Timer").SendMessage("SetSeconds", timeRemaining, SendMessageOptions.DontRequireReceiver);
	}
	
	// Update is called once per frame
	void Update () {
	
		if (canClickButton && Input.GetMouseButtonUp (0) && MiniGameManager.Instance.canPlay)
		{
			if (ButtonsScript.Instance.mouseDownObject != null && ButtonsScript.Instance.mouseDownObject.name.Contains ("FoodButton")) 
				ClickOnButton (ButtonsScript.Instance.mouseDownObject);
		}

//		if (timeRemaining == 0f && !gameOverCalled)
//		{
//			gameOverCalled = true;
//			CanvasInPenguins.Instance.GameOver();
//			//ButtonsScript.Instance.ShowFinal ();
//		}
	}

	void InitializeGame ()
	{
		foodList = new List<GameObject> ();
		CreateFood (6);
		//GameObject.Find ("Timer").GetComponent<Text> ().text = timeRemaining.ToString ("n2");
		//StartCoroutine (UpdateTime ());
		Timer.instance.gamePaused = false;
		StartCoroutine (ButtonsScript.Instance.InitializeGame ());
	}

	void CreateFood (int number)
	{
		float startingPos = (number == 1) ? 30.9f : 3.7f; //26.9 i -1.3 (2.7 posle)
		for (int i = 0; i < number; i++)
		{
			int foodType = Random.Range (0, 5);
			Vector3 position = new Vector3 (startingPos + 4.7f * i, foodPrefabs[foodType].transform.position.y, foodPrefabs[foodType].transform.position.z);
			GameObject food = Instantiate (foodPrefabs [foodType], position, Quaternion.identity) as GameObject;
			foodList.Add (food);
		}
	}

	void ClickOnButton (GameObject button)
	{
		if(!MiniGameManager.Instance.canPlay) return;
		if(	SoundManager.Instance != null ) SoundManager.Instance.Play_ButtonClick();
		string foodType = foodList [0].name.Split (new string[] { "Prefab" }, System.StringSplitOptions.None) [0];
		if (button.name.Contains (foodType))
		{
			canClickButton = false;
			currentCorrectAnswers++;
			
			if (currentCorrectAnswers == correctAnswersToAddTime) {
				
				AddTime (timeToAdd);
				SoundManagerMiniGames.Instance.Play_TimeBonus();
				currentCorrectAnswers = 0;
			}

			GameObject.Find ("FeedTheAnimalHolder/Treadmil/AnimationHolder").GetComponent<Animation>().Play ("FeedTheAnimalTreadmil");

//			if (GameObject.Find ("FeedTheAnimalHolder/PetHolder/AnimationHolder").GetComponent<Animation>().IsPlaying ("PenguinEat"))
//				GameObject.Find ("FeedTheAnimalHolder/PetHolder/AnimationHolder").GetComponent<Animation>().Rewind ("PenguinEat");
//			else
//				GameObject.Find ("FeedTheAnimalHolder/PetHolder/AnimationHolder").GetComponent<Animation>().Play ("PenguinEat");
			GameObject.Find ("FeedTheAnimalHolder/PetHolder").GetComponent<Animator>().Play ("FeedTheAnimalPetEat",0,0);

			if (firstTimeEat)
				firstTimeEat = false;
			else
				for (int i = 0; i < foodList.Count; i++)
				{
					Vector3 position = new Vector3 (foodList[i].transform.position.x - 4.7f, foodList[i].transform.position.y, foodList[i].transform.position.z);
					foodList[i].transform.position = position;
				}

			foodList[0].transform.GetChild (0).GetComponent<Animation>().Play ("EatFood");
			for (int i = 1; i < foodList.Count; i++)
				foodList[i].transform.GetChild (0).GetComponent<Animation>().Play ("MoveFood");
			SoundManagerMiniGames.Instance.Play_MoveAssembly();
			SoundManagerMiniGames.Instance.Play_EatFood();
			StartCoroutine (EatingAnimation ());
		}
		else
		{
			AddTime (-timeToRemove);
			SoundManagerMiniGames.Instance.Play_TimePenalty();
			//CanvasInPenguins.Instance.GameOver();
			//ButtonsScript.Instance.ShowFinal ();
		}
	}

	IEnumerator EatingAnimation ()
	{
		yield return new WaitForSeconds (foodList[0].transform.GetChild (0).GetComponent<Animation>()["EatFood"].length);
		
		canClickButton = true;
		Destroy (foodList [0]);
		foodList.RemoveAt (0);
		CreateFood (1);
		ButtonsScript.Instance.AddScore (1);
		coins++;
		//CanvasInPenguins.Instance.SetCoins( (GlobalVariables_FTA.doubleCoinsBought) ? coins*2 : coins);
		
		//yield return new WaitForSeconds (GameObject.Find ("FeedTheAnimalHolder/Penguin/AnimationHolder").GetComponent<Animation>()["PenguinEat"].length - foodList[0].transform.GetChild (0).GetComponent<Animation>()["EatFood"].length);

		//if (!GameObject.Find ("FeedTheAnimalHolder/Penguin/AnimationHolder").GetComponent<Animation>().IsPlaying ("PenguinMove"))
		//	GameObject.Find ("FeedTheAnimalHolder/Penguin/AnimationHolder").GetComponent<Animation>().Play ("PenguinMove");
	 
		if(!MiniGameManager.Instance.canPlay )
		{
			//   Transform bodyContent = GameObject.Find("Canvas").transform.Find("PopUps/PopUpGameOver/AnimationHolder/Body/BodyContent");
			//   Transform interfaceContent = GameObject.Find("Canvas").transform.Find("UI/TimerLifeBg/InterfaceBg");
			//   bodyContent.Find("ScoreFinalSlot/ScoreFinalSlotText").GetComponent<Text>().text = interfaceContent.Find("Score").GetComponent<Text>().text;
			GameObject.Find("Canvas").transform.Find("PopUps/PopUpGameOver/AnimationHolder/Body/BodyContent/HiscoreFinalSlot/HiScoreFinalSlotText").GetComponent<Text>().text =
				GameObject.Find("Canvas").transform.Find("UI/TimerLifeBg/InterfaceBg/Highscore").GetComponent<Text>().text;
			GameObject.Find("Canvas").transform.Find("PopUps/PopUpGameOver/AnimationHolder/Body/BodyContent/ScoreFinalSlot/ScoreFinalSlotText").GetComponent<Text>().text =
				GameObject.Find("Canvas").transform.Find("UI/TimerLifeBg/InterfaceBg/Score").GetComponent<Text>().text;
		}
 
	}

	IEnumerator UpdateTime ()
	{
		if (timeRemaining > 0f) {
			yield return new WaitForSeconds (Time.fixedDeltaTime);
			
			timeRemaining -= Time.fixedDeltaTime;
			//GameObject.Find ("Timer").GetComponent<Text> ().text = timeRemaining.ToString ("n2");
			GameObject.Find ("Timer").GetComponent<Text> ().text = timeRemaining.ToString ("n0");
			progressBar.fillAmount -= reduceTimeRatio*Time.fixedDeltaTime;
			if(timeRemaining < 10)
				progressBar.color = Color.red;
			else if(timeRemaining < 30)
				progressBar.color = Color.yellow;
			StartCoroutine (UpdateTime ());
		} 
		else {
			
			timeRemaining = 0f;
			//GameObject.Find ("Timer").GetComponent<Text> ().text = timeRemaining.ToString ("n2");
			GameObject.Find ("Timer").GetComponent<Text> ().text = timeRemaining.ToString ("n0");
			progressBar.fillAmount = 0;
		}
	}
	
	void AddTime (float amount) {
//		if(amount > 0)
//		{
//			timerLifeBg.GetComponent<Animator>().Play("FeedTheAnimalTimerPlus",0,0);
//		}
//		else
//		{
//			timerLifeBg.GetComponent<Animator>().Play("FeedTheAnimalTimerMinus",0,0);
//		}
//		timeRemaining += amount;
//		progressBar.fillAmount += amount*reduceTimeRatio;
//		if(timeRemaining < 10)
//			progressBar.color = Color.red;
//		else if(timeRemaining < 30)
//			progressBar.color = Color.yellow;
//		else
//			progressBar.color = Color.green;

		GameObject.Find ("Timer").GetComponent<Timer>().AddTime(amount);
	}

	void StartGame()
	{
		InitializeGame ();
	}


	public void FLPauseGame( )
	{
		Debug.Log("AAAA");
		if(ButtonsScript.Instance.canPlay )
		{
			Debug.Log("AAAA2");
			CanvasInPenguins.Instance.PauseGame(0);
		}
	}

}
