﻿using UnityEngine;
using System.Collections;

public class AnimEvents2 : MonoBehaviour {

	bool soundPlayed;
	bool visible = false;


	void TurnOffGameLoadingHolder()
	{
		transform.parent.gameObject.SetActive(false);
	}

	void DisablePopup()
	{
		gameObject.SetActive(false);
	}

	void PlayPrepareForJump()
	{
		SoundManagerMiniGames.Instance.Play_PrepareForJump();
	}

	void PlayBlades()
	{
		if(!SoundManagerMiniGames.Instance.blades.isPlaying && !soundPlayed && visible)
		{
			SoundManagerMiniGames.Instance.Play_Blades();
			soundPlayed = true;
		}
	}

	void PlayRockFallDown()
	{
		if(visible)
			SoundManagerMiniGames.Instance.Play_RockFallDown();
	}

	void PlayOpenWoodenDoor()
	{
		if(visible)
			SoundManagerMiniGames.Instance.Play_WoodenDoorOpen();
	}

	void OnBecameInvisible()
	{
		visible = false;
	}

	void OnBecameVisible()
	{
		visible = true;
	}

	void PlayLogoSound()
	{
		GetComponent<AudioSource>().Play();
	}

	void EnableGestureBlocker()
	{
		Camera.main.transform.Find("GestureBlocker").gameObject.SetActive(true);
	}

	void DisableGestureBlocker()
	{
		Camera.main.transform.Find("GestureBlocker").gameObject.SetActive(false);
	}

	void JumpingAnimal_Splash()
	{
		Transform animal = GameObject.Find("PetHolder").transform.Find("AnimtionHolder/PetSideBody");
		if(Mathf.Abs((animal.position.x -1.3f) - transform.parent.position.x) <= 0.5f && transform.parent.GetComponent<JumpingAnimalPlatform>().activateEvent)
		{
			Camera.main.GetComponent<JumpThePenguinScript>().PetSplash();
			transform.parent.gameObject.SetActive(false);
		}
	}
}
