﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;

public class ScrollingScript : MonoBehaviour {

	private List<Transform> backgroundPart;
	public bool isHorizontal = true;
	
	// Use this for initialization
	void Start ()
	{
		backgroundPart = new List<Transform>();
		
		for (int i = 0; i < transform.childCount; i++)
		{
			Transform child = transform.GetChild(i);

			if (child.GetComponent<Renderer>() != null)
			{
				backgroundPart.Add(child);
			}
		}

		backgroundPart = backgroundPart.OrderBy(
			t => t.position.y
			).ToList();
	}
	
	// Update is called once per frame
	void Update ()
	{
		Transform firstChild = backgroundPart.FirstOrDefault();
		
		if (firstChild != null)
		{
			Transform lastChild = backgroundPart.LastOrDefault();
			float lastChildPosition = (isHorizontal) ? lastChild.transform.position.x : lastChild.transform.position.y;
			float cameraPosition = (isHorizontal) ? Camera.main.transform.position.x : Camera.main.transform.position.y;
			float firstChildPosition = (isHorizontal) ? firstChild.transform.position.x : firstChild.transform.position.y;

			if (lastChildPosition <= cameraPosition)
			{
				Vector3 lastPosition = lastChild.transform.position;
				Vector3 lastSize = (lastChild.GetComponent<Renderer>().bounds.max - lastChild.GetComponent<Renderer>().bounds.min);

				float firstChildX = (isHorizontal) ? lastPosition.x + lastSize.x - 0.1f : firstChild.position.x;
				float firstChildY = (isHorizontal) ? firstChild.position.y : lastPosition.y + lastSize.y;

				firstChild.position = new Vector3(firstChildX, firstChildY, firstChild.position.z);

				backgroundPart.Remove(firstChild);
				backgroundPart.Add(firstChild);
			}


			
			else if (firstChildPosition > cameraPosition)
			{
				Vector3 firstPosition = firstChild.transform.position;
				Vector3 firstSize = (firstChild.GetComponent<Renderer>().bounds.max - firstChild.GetComponent<Renderer>().bounds.min);
				
				float lastChildX = (isHorizontal) ? firstPosition.x - firstSize.x : lastChild.position.x;
				float lastChildY = (isHorizontal) ? lastChild.position.y : firstPosition.y - firstSize.y;
				
				lastChild.position = new Vector3(lastChildX, lastChildY, lastChild.position.z);
				
				backgroundPart.Remove(lastChild);
				backgroundPart.Insert(0, lastChild);
			}
		}
	}
}
