﻿using UnityEngine;
using System.Collections;

public class JumpingAnimalPlatform : MonoBehaviour {

	[HideInInspector] public bool activateEvent = false;
	public static bool firstPlank = false;
	bool startedBlinking = false;

	void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.gameObject.tag.Equals("Player"))
		{
			if(!firstPlank)
			{
				firstPlank = true;
			}
			else
			{
				activateEvent = true;
				Invoke("StartDisappearing",0.5f);
			}
		}
	}
	void OnCollisionExit2D(Collision2D collision)
	{
		activateEvent = false;
		if(!startedBlinking)
		{
			CancelInvoke("StartDisappearing");
		}
		//transform.Find("AnimationHolder").GetComponent<Animator>().Play("GroundIdle");
	}

	void StartDisappearing()
	{
		startedBlinking = true;
		transform.Find("AnimationHolder").GetComponent<Animator>().Play("GroundDisappear");
	}
}
