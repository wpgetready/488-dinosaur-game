﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;

public class JumpThePenguinScript : MonoBehaviour {

	public GameObject penguin;
	public GameObject[] roadElement;
	public GameObject coinElement;

	public int totalNextCoinPosition = -1;
	public int nextCoinPosition;
	public int coins = 0;
	public int penguinPosition = 0;
	public bool isOnTheRoad;
	
	float penguinX;
	float currentAnimFrame = 0;
	float totalAnimFrames;
	bool startMoving;
	float moveDistance;
	
	Vector3 roadElementPos;
	int[] repeatElementPairs;
	GameObject lastCreatedRoadElement;

	bool startResetAnimation = false;
	
	float maxHorizontalPosition;
	float minHorizontalPosition;

	int createdRoadElements = 0;
	public int roadElementIndex = 0;
	int totalCreatedElements = 0;
	
	List<float> roadPositions = new List<float> ();
	bool isDrowning = false;

	// Use this for initialization
	void Start () {
		CanvasInPenguins.Instance.CheckHighscore("JumpTheAnimal_Highscore");
		InitializeGame ();
		isOnTheRoad = true;
		Vector3 leftBound = Camera.main.ViewportToWorldPoint(Vector3.zero);
		Vector3 rightBound = Camera.main.ViewportToWorldPoint(Vector3.one);
		transform.Find("GameHolder/MoveButtonLeft").position = new Vector3(leftBound.x + 2.5f, transform.Find("GameHolder/MoveButtonLeft").position.y, transform.Find("GameHolder/MoveButtonLeft").position.z);
		transform.Find("GameHolder/MoveButtonRight").position = new Vector3(rightBound.x - 2.5f, transform.Find("GameHolder/MoveButtonRight").position.y, transform.Find("GameHolder/MoveButtonRight").position.z);

		GlobalVariables.OnPauseGame += FLPauseGame;
	}
	
	// Update is called once per frame
	void Update () {
		if (ButtonsScript.Instance.canPlay && MiniGameManager.Instance.canPlay)
		{
			if (startMoving)
			{
				//MoveScene ();
			}

			if (startResetAnimation)
			{
				CanvasInPenguins.Instance.GameOver();
				//ButtonsScript.Instance.ShowFinal ();
			}

			if (Input.GetMouseButtonDown (0))
			{
				if (ButtonsScript.Instance.mouseDownObject != null && ButtonsScript.Instance.mouseDownObject.name.Contains("MoveButton") && isOnTheRoad)
				{
					//StartMoving((ButtonsScript.Instance.mouseDownObject.name.Contains("Left")) ? 1.0f : 2.0f);
					isOnTheRoad = false;
					StartCoroutine(movePlayer(ButtonsScript.Instance.mouseDownObject.name.Contains("Left") ? 4.0f : 8.0f));
				}
			}

			#if UNITY_EDITOR
			if (Input.GetKeyDown(KeyCode.LeftArrow) && isOnTheRoad)
			{
				//StartMoving(1.0f);
				if(isOnTheRoad)
				{
					isOnTheRoad = false;
					StartCoroutine(movePlayer(4f));
				}
			}
			if (Input.GetKeyDown(KeyCode.RightArrow) && isOnTheRoad)
			{
				//StartMoving(2.0f);
				if(isOnTheRoad)
				{
					isOnTheRoad = false;
					StartCoroutine(movePlayer(8f));
				}
			}
			#endif
		}
	}

	IEnumerator movePlayer(float distance)
	{
		float targetPos = penguin.transform.position.x + distance;

		if (distance == 1.0f)
		{
			if (roadPositions.Exists (match => match == targetPos))
				penguin.transform.Find("PetHolder").GetComponent<Animator>().CrossFade("JumpOneSlot", 0f, -1, 0f);
			else
			{
				penguin.transform.Find("PetHolder").GetComponent<Animator>().CrossFade("JumpOneSlotMiss", 0f, -1, 0f);
				isDrowning = true;
			}
			SoundManagerMiniGames.Instance.Play_JumpJumping();
		}
		else
		{
			if (roadPositions.Exists (match => match == targetPos))
				penguin.transform.Find("PetHolder").GetComponent<Animator>().CrossFade("JumpTwoSlots", 0f, -1, 0f);
			else
			{
				penguin.transform.Find("PetHolder").GetComponent<Animator>().CrossFade("JumpTwoSlotsMiss", 0f, -1, 0f);
				isDrowning = true;
			}
			SoundManagerMiniGames.Instance.Play_JumpJumping();
		}
		while(penguin.transform.position.x != targetPos)
		{
			penguin.transform.position = Vector3.MoveTowards(penguin.transform.position, new Vector3(targetPos,penguin.transform.position.y,penguin.transform.position.z),Time.deltaTime*22);
			Camera.main.transform.position = new Vector3 (penguin.transform.position.x, Camera.main.transform.position.y, Camera.main.transform.position.z);
			yield return null;
			if (lastCreatedRoadElement.transform.position.x < maxHorizontalPosition*2f)
			{
				CreateRoad (1);
			}
		}
		if (isDrowning)
		{
			startResetAnimation = true;
			isDrowning = false;
			
			//SoundManager.Instance.PlaySound(SoundManager.Instance.penguinDrown);
			//penguin.transform.Find ("PetHolder/AnimtionHolder").gameObject.SetActive (false);
			Invoke("TurnOffPlayerWithDelay",1.5f);
			penguin.transform.Find ("PetHolder/ShipSplashHuge").gameObject.SetActive (true);
			SoundManagerMiniGames.Instance.Play_WaterSplash();
		}
		else
		{
			 
			ButtonsScript.Instance.AddScore (1);//((moveDistance == 1.0f) ? 1 : 2);
			isOnTheRoad = true;
			maxHorizontalPosition = Camera.main.transform.position.x + Camera.main.orthographicSize * Camera.main.aspect + 2.0f;
			minHorizontalPosition = Camera.main.transform.position.x - Camera.main.orthographicSize * Camera.main.aspect - 2.0f;
			
			if (lastCreatedRoadElement.transform.position.x < maxHorizontalPosition)
			{
				//CreateRoad (8);
				//CreateRoad (1);
			}
			
			GameObject[] roadElements = GameObject.FindGameObjectsWithTag ("RoadElement");
			foreach (GameObject roadElement in roadElements)
				if (roadElement.transform.position.x < minHorizontalPosition)
					Destroy (roadElement);
		}

	}

	void InitializeGame ()
	{
		nextCoinPosition = Random.Range (3, 6);
		repeatElementPairs = new int[2] {0, 0};
		roadElementPos = new Vector3 (-0.4f, roadElement[0].transform.position.y, roadElement[0].transform.position.z);
		CreateRoad (12);
		//InvokeRepeating("CreateRoadRepeating",1,0.5f);
		StartCoroutine (ButtonsScript.Instance.InitializeGame ());
	}

	void CreateRoadRepeating()
	{
		CreateRoad(1);
	}

	public void CreateRoad(int totalElements)
	{
		for (int i = 0; i < totalElements; i++)
		{
			CreateRoadElement(roadElement[roadElementIndex], roadElementPos);

//			if (createdRoadElements == nextCoinPosition)
//				CreateCoin (new Vector3(roadElementPos.x + 0.2f, -4.45f, 0.0f));
			
			float nextElementX = Random.Range (0.0f, 1.0f);
			nextElementX = (nextElementX <= 0.5f) ? 4.0f : 8.0f;
			
			//Never allow to have more than 6 road elements next to each, or more than 3 elements with double distance one after another
			repeatElementPairs[0] += (nextElementX == 4.0f) ? 1 : 2;
			repeatElementPairs[1] += (nextElementX == 4.0f) ? 0 : 1;
			if (repeatElementPairs[0] >= 6)
			{
				if (repeatElementPairs[1] == 3)
					nextElementX = 4.0f;
				if (repeatElementPairs[1] == 0)
					nextElementX = 8.0f;
				
				repeatElementPairs[0] = 0;
				repeatElementPairs[1] = 0;
			}
			
			//roadPositions.Add (totalCreatedElements);
			
			totalCreatedElements += (nextElementX == 4.0f) ? 1 : 2;
			if (createdRoadElements + 1 == nextCoinPosition)
				totalNextCoinPosition = totalCreatedElements;

			if (createdRoadElements != 0 && createdRoadElements % 50 == 0 && roadElementIndex < 3)
				roadElementIndex++;
			
			roadElementPos = new Vector3(roadElementPos.x + nextElementX, roadElement[roadElementIndex].transform.position.y, roadElementPos.z);
			roadPositions.Add(roadElementPos.x);
			createdRoadElements++;
		}
	}
	
	public void CreateRoadElement(GameObject elementType, Vector3 position)
	{
		GameObject element = Instantiate (elementType, position, Quaternion.identity) as GameObject;
		lastCreatedRoadElement = element;
	}

	public void StartMoving(float distance)
	{
		moveDistance = distance;
		startMoving = true;
	}

	public void MoveScene()
	{
		if (currentAnimFrame == 0f)
		{
			penguinPosition += (int)moveDistance;
			
			if (moveDistance == 1.0f)
			{
				if (roadPositions.Exists (match => match == penguinPosition))
					penguin.transform.Find("PetHolder").GetComponent<Animator>().CrossFade("JumpOneSlot", 0f, -1, 0f);
				else
				{
					penguin.transform.Find("PetHolder").GetComponent<Animator>().CrossFade("JumpOneSlotMiss", 0f, -1, 0f);
					isDrowning = true;
				}
				SoundManagerMiniGames.Instance.Play_JumpJumping();
				totalAnimFrames = 0.33f;
			}
			else
			{
				if (roadPositions.Exists (match => match == penguinPosition))
					penguin.transform.Find("PetHolder").GetComponent<Animator>().CrossFade("JumpTwoSlots", 0f, -1, 0f);
				else
				{
					penguin.transform.Find("PetHolder").GetComponent<Animator>().CrossFade("JumpTwoSlotsMiss", 0f, -1, 0f);
					isDrowning = true;
				}
				SoundManagerMiniGames.Instance.Play_JumpJumping();
				totalAnimFrames = 0.4f;//0.5f;
			}
			//penguin.transform.Find("PetHolder/AnimtionHolder/PetSideBody").GetComponent<JumpThePenguinPenguinScript>().animalIndex++;
			if(penguin.transform.Find("PetHolder/AnimtionHolder/PetSideBody").GetComponent<JumpThePenguinPenguinScript>().currentPlatform != null)
				penguin.transform.Find("PetHolder/AnimtionHolder/PetSideBody").GetComponent<JumpThePenguinPenguinScript>().currentPlatform.Find("AnimationHolder").GetComponent<Animator>().Play("ContinueDisappearing");
		}
		if (currentAnimFrame < totalAnimFrames - Time.deltaTime)
		{
			penguin.transform.Translate (new Vector3 (4.0f * moveDistance * (Time.deltaTime / totalAnimFrames), 0.0f, 0.0f));
			Camera.main.transform.position = new Vector3 (penguin.transform.position.x, Camera.main.transform.position.y, Camera.main.transform.position.z);

			maxHorizontalPosition = Camera.main.transform.position.x + Camera.main.orthographicSize * Camera.main.aspect + 2.0f;
			minHorizontalPosition = Camera.main.transform.position.x - Camera.main.orthographicSize * Camera.main.aspect - 2.0f;
			
			if (lastCreatedRoadElement.transform.position.x < maxHorizontalPosition)
				CreateRoad (1);
			
			GameObject[] roadElements = GameObject.FindGameObjectsWithTag ("RoadElement");
			foreach (GameObject roadElement in roadElements)
				if (roadElement.transform.position.x < minHorizontalPosition)
					Destroy (roadElement);
			
			currentAnimFrame += Time.deltaTime;
		}
		else
		{
			startMoving = false;
			//isOnTheRoad = true;
			currentAnimFrame = 0f;
			penguin.transform.position = new Vector3 (penguinX + 4.0f * moveDistance, penguin.transform.position.y, penguin.transform.position.z);
			Camera.main.transform.position = new Vector3 (penguin.transform.position.x, Camera.main.transform.position.y, Camera.main.transform.position.z);
			penguinX += 4.0f * moveDistance;
			
			if (isDrowning)
			{
				startResetAnimation = true;
				isDrowning = false;

				//SoundManager.Instance.PlaySound(SoundManager.Instance.penguinDrown);
				//penguin.transform.Find ("PetHolder/AnimtionHolder").gameObject.SetActive (false);
				Invoke("TurnOffPlayerWithDelay",2.5f);
				penguin.transform.Find ("PetHolder/ShipSplashHuge").gameObject.SetActive (true);
				SoundManagerMiniGames.Instance.Play_WaterSplash();
			}
			 
			ButtonsScript.Instance.AddScore( 1 );//((moveDistance == 1.0f) ? 1 : 2);
		}
	}

	void TurnOffPlayerWithDelay()
	{
		penguin.transform.Find ("PetHolder/AnimtionHolder").gameObject.SetActive (false);
	}

	public void PetSplash()
	{
		penguin.transform.Find("PetHolder").GetComponent<Animator>().CrossFade("JumpOneSlotMiss", 0f, -1, 0f);
		isDrowning = true;
		isOnTheRoad = false;
		Invoke("PetSplashGameOver",0.5f);
	}
	void PetSplashGameOver()
	{
		if (isDrowning)
		{
			startResetAnimation = true;
			isDrowning = false;
			
			//SoundManager.Instance.PlaySound(SoundManager.Instance.penguinDrown);
			penguin.transform.Find ("PetHolder/AnimtionHolder").gameObject.SetActive (false);
			penguin.transform.Find ("PetHolder/ShipSplashHuge").gameObject.SetActive (true);
			SoundManagerMiniGames.Instance.Play_WaterSplash();
		}
	}

//	public void CreateCoin (Vector3 position)
//	{
//		GameObject coin = Instantiate (coinElement, position, Quaternion.identity) as GameObject;
//		coin.transform.Find ("AnimationHolder").GetComponent<Animator> ().CrossFade ("CollectableIdle", 0f, -1, 0f);
//		nextCoinPosition += Random.Range (3 + roadElementIndex, 6 + roadElementIndex);
//	}
//
//	public IEnumerator CollectCoin (GameObject coinObject)
//	{
//		coins++;
//		coinObject.transform.Find ("AnimationHolder").GetComponent<Animator> ().CrossFade ("CollectableColected", 0f, -1, 0f);
//		coinObject.transform.Find ("AnimationHolder/ParticleCollected").gameObject.SetActive (true);
//		CanvasInPenguins.Instance.SetCoins(coins);
//		yield return new WaitForSeconds (1.5f);
//
//		Destroy (coinObject);
//	}

	public void PauseGame(int popupMenuIndex)
	{
		Time.timeScale = 0;
		CanvasInPenguins.Instance.PauseGame(popupMenuIndex);
	}

	public void ResumeGame(int popupMenuIndex)
	{
		CanvasInPenguins.Instance.ResumeGame(popupMenuIndex);
		StartCoroutine("CResumeGame");
	}

	IEnumerator CResumeGame()
	{
		yield return new WaitForSecondsRealtime(1.5f);
		Time.timeScale = 1;

	}

 

	public void FLPauseGame( )
	{
		if(ButtonsScript.Instance.canPlay )
		{
			Time.timeScale = 0;
			CanvasInPenguins.Instance.PauseGame(0);
		}
	}

 
}
