﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CanvasInPenguins2 : MonoBehaviour {

	static CanvasInPenguins instance;
	public static CanvasInPenguins Instance
	{
		get
		{
			if(instance == null)
				instance = GameObject.FindObjectOfType(typeof(CanvasInPenguins)) as CanvasInPenguins;
			
			return instance;
		}
	}
	enum orientation {
		
		Portrait = 0,
		Landscape = 1
	}
	int currentOrientation;
	Text scoreText;
	Text hiscoreText;
	Text coinsText;
	string prefsName = "";
	bool paused = false;
	bool canClick = true;

	void Awake()
	{
		scoreText = GameObject.Find("Score").GetComponent<Text>();
		hiscoreText = GameObject.Find("Highscore").GetComponent<Text>();
		coinsText = GameObject.Find("Coins").GetComponent<Text>();
	}

	void Start () 
	{
		currentOrientation = (Camera.main.aspect < 1.0f) ? (int)orientation.Portrait : (int)orientation.Landscape;
		MiniGameManager.Instance.StartMeasureTime();
	}

	void Update ()
	{
		if(Input.GetKeyUp(KeyCode.Escape) && MiniGameManager.Instance.canPlay)
		{
			if(!paused)
			{
				if(canClick)
				{
					if(MiniGameManager.Instance.canPlay)
						PauseGame(0);
				}
			}
			else
			{
				if(canClick)
				{
					ResumeGame(0);
				}
			}
		}
	}

	void UnpauseWithDelay()
	{
		paused = false;
	}

	void ResetCanClick()
	{
		canClick = true;
	}

	public void PauseGame (int popUpMenuIndex) {
		//TODO DODAJ ZVUK ZA BUTTON CLICK
		//SoundManager.Instance.Play_ButtonClick4();
		if(ButtonsScript.Instance != null)
			ButtonsScript.Instance.canPlay = false;
		//canPlay = false;
		//gamePaused = true;
		//owl.GetComponent<Rigidbody2D> ().isKinematic = true;
		
		int numOfPopUps = GameObject.Find ("Canvas").GetComponent<MenuManager_JumpingAnimal> ().disabledObjects.Length / 2;
		GameObject.Find ("Canvas").GetComponent<MenuManager_JumpingAnimal> ().ShowPopUpMenu (GameObject.Find ("Canvas").GetComponent<MenuManager_JumpingAnimal> ().disabledObjects [3 * currentOrientation + popUpMenuIndex]); 
		MiniGameManager.Instance.StopMeasureTime();
		if(Application.loadedLevelName.Equals("FeedTheAnimal") || Application.loadedLevelName.Equals("MemoryGame") || Application.loadedLevelName.Equals("WhackAMole"))
		{
			transform.Find("UI/TimerLifeBg/Timer").SendMessage("PauseTimer",SendMessageOptions.DontRequireReceiver);
		}
		if(Application.loadedLevelName.Equals("FruitGameScene"))
		{
			Camera.main.SendMessage("TurnOffCollider",SendMessageOptions.DontRequireReceiver);
		}
		paused = true;
		canClick = false;
		CancelInvoke("ResetCanClick");
		Invoke("ResetCanClick",1.25f);
	}
	
	public void ResumeGame (int popUpMenuIndex) {
		//TODO DODAJ ZVUK ZA BUTTON CLICK
		//SoundManager.Instance.Play_ButtonClick4();
		if(ButtonsScript.Instance != null)
			ButtonsScript.Instance.canPlay = true;
		//canPlay = true;
		//gamePaused = false;
		//owl.GetComponent<Rigidbody2D> ().isKinematic = false;
		
		int numOfPopUps = GameObject.Find ("Canvas").GetComponent<MenuManager_JumpingAnimal> ().disabledObjects.Length / 2;
		GameObject.Find ("Canvas").GetComponent<MenuManager_JumpingAnimal> ().ClosePopUpMenu (GameObject.Find ("Canvas").GetComponent<MenuManager_JumpingAnimal> ().disabledObjects [3 * currentOrientation + popUpMenuIndex]);
		MiniGameManager.Instance.StartMeasureTime();
		if(Application.loadedLevelName.Equals("FeedTheAnimal") || Application.loadedLevelName.Equals("MemoryGame") || Application.loadedLevelName.Equals("WhackAMole"))
		{
			transform.Find("UI/TimerLifeBg/Timer").SendMessage("UnpauseTimer",SendMessageOptions.DontRequireReceiver);
		}
		if(Application.loadedLevelName.Equals("FruitGameScene"))
		{
			Camera.main.SendMessage("TurnOnCollider",SendMessageOptions.DontRequireReceiver);
		}
		//Invoke("UnpauseWithDelay",1f);
		paused = false;
		canClick = false;
		CancelInvoke("ResetCanClick");
		Invoke("ResetCanClick",1.25f);
	}

	public void LoadLevel()
	{
		Time.timeScale = 1;

		SoundManagerMiniGames.Instance.Stop_GameplayMusicMiniGames1();

		Application.LoadLevel(Application.loadedLevelName);

	}

	public void GameOver () 
	{
//		MiniGameManager.Instance.doubleCoinsVideoCheck = 1;

		/*if(Screen.orientation == ScreenOrientation.Landscape)
			currentOrientation = 0;
		Debug.Log("current orientation: " + currentOrientation);*/

		SoundManagerMiniGames.Instance.Stop_GameplayMusicMiniGames1();

		SoundManagerMiniGames.Instance.Play_GameOver();
		MiniGameManager.Instance.FinalScore();

		ButtonsScript.Instance.canPlay = false;
		int numOfPopUps = GameObject.Find ("Canvas").GetComponent<MenuManager_JumpingAnimal> ().disabledObjects.Length / 2;
		GameObject.Find ("Canvas").GetComponent<MenuManager_JumpingAnimal> ().ShowPopUpMenu (GameObject.Find ("Canvas").GetComponent<MenuManager_JumpingAnimal> ().disabledObjects [2 + currentOrientation * 3]);
		MiniGameManager.Instance.IncreaseMoney();
		MiniGameManager.Instance.StopMeasureTime();
		MiniGameManager.Instance.canPlay = false;
	}

	public void SetScore(int value)
	{
		scoreText.text = value.ToString();
		SetHighscore(value);
	}

	public void SetCoins(int value)
	{
		coinsText.text = value.ToString();
		MiniGameManager.Instance.SetMoney(value);
	}

	public void SetHighscore(int score)
	{
		if(prefsName != "")
		{
			if (!PlayerPrefs.HasKey (prefsName) || (PlayerPrefs.HasKey (prefsName) && PlayerPrefs.GetInt (prefsName) < score))
			{
				PlayerPrefs.SetInt (prefsName, score);
				hiscoreText.text = score.ToString();
			}
		}
	}

	public void CheckHighscore(string prefsNameForMiniGame)
	{
		prefsName = prefsNameForMiniGame;
		if(PlayerPrefs.HasKey (prefsName))
		{
			hiscoreText.text = PlayerPrefs.GetInt(prefsName).ToString();
		}
	}
	
}
