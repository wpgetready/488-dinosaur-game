﻿using UnityEngine;
using System.Collections;

public class JumpThePenguinPenguinScript : MonoBehaviour {

	JumpThePenguinScript jumpThePenguinScript;
	[HideInInspector] public Transform currentPlatform;
	[HideInInspector] public Transform previousPlatform;
	bool firstPlank = true;
	[HideInInspector] public int roadIndex = 0;
	[HideInInspector] public int animalIndex = 0;
	[HideInInspector] public Vector3 criticalPosition;

	// Use this for initialization
	void Start () {
		jumpThePenguinScript = GameObject.Find ("Main Camera").GetComponent<JumpThePenguinScript> ();
		JumpingAnimalPlatform.firstPlank = false;
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.tag == "RoadElement" && !jumpThePenguinScript.isOnTheRoad)
		{
			//jumpThePenguinScript.isOnTheRoad = true;
			
			collision.transform.Find("AnimationHolder").GetComponent<Animator>().CrossFade("GroundDrop", 0f, -1, 0f);
			//if(currentPlatform != null)
				//currentPlatform.Find("AnimationHolder").GetComponent<Animator>().Play("ContinueDisappearing");

//			if(firstPlank)
//				firstPlank = false;
//			else
//			{
//				currentPlatform = collision.gameObject.transform;
//				roadIndex++;
//				CancelInvoke("StartDisappearing");
//				Invoke("StartDisappearing",1f);
//			}
			
			/*if (collision.gameObject.name.Contains("Plank"))
				SoundManager.Instance.PlaySound(SoundManager.Instance.jumpRoadElements[0]);
			else if (collision.gameObject.name.Contains ("Ball"))
				SoundManager.Instance.PlaySound(SoundManager.Instance.jumpRoadElements[1]);
			else if (collision.gameObject.name.Contains ("Inflatable"))
				SoundManager.Instance.PlaySound(SoundManager.Instance.jumpRoadElements[2]);
			else if (collision.gameObject.name.Contains ("Barrel"))
				SoundManager.Instance.PlaySound(SoundManager.Instance.jumpRoadElements[3]);*/
		}
	}

//	void StartDisappearing()
//	{
//		criticalPosition = currentPlatform.position;
//		currentPlatform.Find("AnimationHolder").GetComponent<Animator>().Play("GroundDisappear");
//	}

	void OnTriggerEnter2D (Collider2D collider) {

//		if (collider.gameObject.tag.Equals ("Collectable")) {
//			SoundManagerMiniGames.Instance.Play_CoinCollect();
//			StartCoroutine (jumpThePenguinScript.CollectCoin (collider.gameObject));
//		}
	}

	void OnCollisionExit2D (Collision2D collision)
	{
		//jumpThePenguinScript.isOnTheRoad = false;
	}
}
